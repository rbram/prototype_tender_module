<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Send_email extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->library('email');
    }
    /*TEXT*/
	function send_emails(){
        $this->email->to('richardramirez041392@gmail.com');
        $this->email->from('pangspotify041392@gmail.com','CodexWorld');
        $this->email->subject('email msg');
        $this->email->message('test message.');
        $this->email->send();
        echo "send";
        
    }
    function send_mailtype(){
        $htmlContent = '<h1>HTML email testing by CodeIgniter Email Library</h1>';
        $htmlContent .= '<p>You can use any HTML tags and content in this email.</p>';

        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        $this->email->to('richardramirez041392@gmail.com');
        $this->email->from('codexworld@gmail.com','CodexWorld');
        $this->email->subject('Test Email (HTML)');
        $this->email->message($htmlContent);
        $this->email->send();
    }
    function send_mailtype_attchment(){
        $htmlContent = '<h1>HTML email with attachment testing by CodeIgniter Email Library</h1>';
        $htmlContent .= '<p>You can attach the files in this email.</p>';

        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        $this->email->to('richardramirez041392@gmail.com');
        $this->email->from('codexworld@gmail.com','CodexWorld');
        $this->email->subject('Test Email (Attachment)');
        $this->email->message($htmlContent);
        $this->email->attach('files/attachment.pdf');
        $this->email->send();
    }
}
