<?php
class Admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_model','admin_model');
        $this->load->model('Database_model','database_model');
        if($this->session->username === NULL)
        {
            redirect(base_url());
        }
    }
    function index()
    {
        $this->dashboard();
    }
//  CSS LINKS
    function csslinks($arr_plugins = FALSE)
    {
        if($arr_plugins !== FALSE)
        {
            $arr_plugin_lists = array(
                'datatables'        => (in_array('datatables',$arr_plugins))        ? '<link rel="stylesheet" href="'.base_url("assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css").'">' : NULL,
                'morris'            => (in_array('morris',$arr_plugins))            ? '<link rel="stylesheet" href="'.base_url("assets/bower_components/morris.js/morris.css").'">' : NULL,
                'jvectormap'        => (in_array('jvectormap',$arr_plugins))        ? '<link rel="stylesheet" href="'.base_url("assets/bower_components/jvectormap/jquery-jvectormap.css").'">' : NULL,
                'datepicker'        => (in_array('datepicker',$arr_plugins))        ? '<link rel="stylesheet" href="'.base_url("assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css").'">' : NULL,
                'datepicker_range'  => (in_array('datepicker_range',$arr_plugins))  ? '<link rel="stylesheet" href="'.base_url("assets/bower_components/bootstrap-daterangepicker/daterangepicker.css").'">' : NULL,
                'wysihtml5'         => (in_array('wysihtml5',$arr_plugins))         ? '<link rel="stylesheet" href="'.base_url("assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css").'">' : NULL,
                'fancybox'          => (in_array('fancybox',$arr_plugins))          ? '<link rel="stylesheet" href="'.base_url("assets/css/jquery.fancybox.css").'">' : NULL,
                'toggle_switch'     => (in_array('toggle_switch',$arr_plugins))     ? '<link rel="stylesheet" href="'.base_url("assets/css/bootstrap-toggle.min.css").'">' : NULL,
                'datepicker'        => (in_array('datepicker',$arr_plugins))        ? '<link rel="stylesheet" href="'.base_url("assets/bower_components/jquery-ui/themes/base/all.css").'">' : NULL,
                'icheck'            => (in_array('icheck',$arr_plugins))            ? '<link rel="stylesheet" href="'.base_url("assets/plugins/iCheck/all.css").'">' : NULL
            );
        }
        $arr_plugin_merge = implode(' ',array_filter($arr_plugin_lists, function($var){return !is_null($var);} ));
        return $arr_plugin_merge;
    }
//  JAVASCRIPT LINKS
    function javalinks($arr_plugins = FALSE)
    {
        if($arr_plugins !== FALSE)
        {
            $arr_plugin_lists = array(
                'jquery_1_12_4'                 => (in_array('jquery_1_12_4',$arr_plugins))                 ? '<script src="'.base_url("assets/addons/datatable/jquery-1.12.4.js").'">"></script>' : NULL,
                'raphael'                       => (in_array('raphael',$arr_plugins))                       ? '<script src="'.base_url("assets/bower_components/raphael/raphael.min.js").'">"></script>' : NULL,
                'morris'                        => (in_array('morris',$arr_plugins))                        ? '<script src="'.base_url("assets/bower_components/morris.js/morris.min.js").'">"></script>' : NULL,
                'sparkline'                     => (in_array('sparkline',$arr_plugins))                     ? '<script src="'.base_url("assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js").'">"></script>' : NULL,
                'jvectormap'                    => (in_array('jvectormap',$arr_plugins))                    ? '<script src="'.base_url("assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js").'">"></script>' : NULL,
                'jvectormap_wold_mill'          => (in_array('jvectormap_wold_mill',$arr_plugins))          ? '<script src="'.base_url("assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js").'">"></script>' : NULL,
                'knob'                          => (in_array('knob',$arr_plugins))                          ? '<script src="'.base_url("assets/bower_components/jquery-knob/dist/jquery.knob.min.js").'">"></script>' : NULL,
                'moment'                        => (in_array('moment',$arr_plugins))                        ? '<script src="'.base_url("assets/bower_components/moment/min/moment.min.js").'">"></script>' : NULL,
                'datepicker'                    => (in_array('datepicker',$arr_plugins))                    ? '<script src="'.base_url("assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js").'">"></script>' : NULL,
                'datepicker_range'              => (in_array('datepicker_range',$arr_plugins))              ? '<script src="'.base_url("assets/bower_components/bootstrap-daterangepicker/daterangepicker.js").'">"></script>' : NULL,
                'wysihtml5'                     => (in_array('wysihtml5',$arr_plugins))                     ? '<script src="'.base_url("assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js").'">"></script>' : NULL,
                'wysihtml5_function'            => (in_array('wysihtml5_function',$arr_plugins))            ? '<script src="'.base_url("assets/js/wysihtml5_function.js").'">"></script>' : NULL,
                'datatables'                    => (in_array('datatables',$arr_plugins))                    ? '<script src="'.base_url("assets/bower_components/datatables.net/js/jquery.dataTables.min.js").'">"></script>' : NULL,
                'datatables_bootstrap'          => (in_array('datatables_bootstrap',$arr_plugins))          ? '<script src="'.base_url("assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js").'">"></script>' : NULL,
                'datatables_function'           => (in_array('datatables_function',$arr_plugins))           ? '<script src="'.base_url("assets/js/datatable_function.js").'">"></script>' : NULL,
                'icheck'                        => (in_array('icheck',$arr_plugins))                        ? '<script src="'.base_url("assets/plugins/iCheck/icheck.min.js").'">"></script>' : NULL,
                'icheck_function'               => (in_array('icheck_function',$arr_plugins))               ? '<script src="'.base_url("assets/js/icheck_function.js").'">"></script>' : NULL,
                'datatables_addon_button'       => (in_array('datatables_addon_button',$arr_plugins))       ? '<script src="'.base_url("assets/addons/datatable/dataTables.buttons.min.js").'">"></script>' : NULL,
                'datatables_addon_print'        => (in_array('datatables_addon_print',$arr_plugins))        ? '<script src="'.base_url("assets/addons/datatable/buttons.print.min.js").'">"></script>' : NULL,
                'datatables_addon_html5'        => (in_array('datatables_addon_html5',$arr_plugins))        ? '<script src="'.base_url("assets/addons/datatable/buttons.html5.min.js").'">"></script>' : NULL,
                'datatables_addon_vfs'          => (in_array('datatables_addon_vfs',$arr_plugins))          ? '<script src="'.base_url("assets/addons/datatable/vfs_fonts.js").'">"></script>' : NULL,
                'datatables_addon_pdf'          => (in_array('datatables_addon_pdf',$arr_plugins))          ? '<script src="'.base_url("assets/addons/datatable/pdfmake.min.js").'">"></script>' : NULL,
                'datatables_addon_jzip'         => (in_array('datatables_addon_jzip',$arr_plugins))         ? '<script src="'.base_url("assets/addons/datatable/jszip.min.js").'">"></script>' : NULL,
                'datatables_addon_flash'        => (in_array('datatables_addon_flash',$arr_plugins))        ? '<script src="'.base_url("assets/addons/datatable/buttons.flash.min.js").'">"></script>' : NULL,
                'supplier_list_function'        => (in_array('supplier_list_function',$arr_plugins))        ? '<script src="'.base_url("assets/js/supplier_list_function.js").'">"></script>' : NULL,
                'supplier_view_function'        => (in_array('supplier_view_function',$arr_plugins))        ? '<script src="'.base_url("assets/js/supplier_view_function.js").'">"></script>' : NULL,
                'fancybox'                      => (in_array('fancybox',$arr_plugins))                      ? '<script src="'.base_url("assets/js/jquery.fancybox.js").'">"></script>' : NULL,
                'fancybox_function'             => (in_array('fancybox_function',$arr_plugins))             ? '<script src="'.base_url("assets/js/fancybox_function.js").'">"></script>' : NULL,
                'form_quotation'                => (in_array('form_quotation',$arr_plugins))                ? '<script src="'.base_url("assets/js/form_quotation.js").'">"></script>' : NULL,
                'quotation_list_function'       => (in_array('quotation_list_function',$arr_plugins))       ? '<script src="'.base_url("assets/js/quotation_list_function.js").'">"></script>' : NULL,
                'currency_function'             => (in_array('currency_function',$arr_plugins))             ? '<script src="'.base_url("assets/js/currency_function.js").'">"></script>' : NULL,
                'currency_format_all'           => (in_array('currency_format_all',$arr_plugins))           ? '<script src="'.base_url("assets/js/jquery.formatCurrency.all.js").'">"></script>' : NULL,
                'currency_format'               => (in_array('currency_format',$arr_plugins))               ? '<script src="'.base_url("assets/js/jquery.formatCurrency-1.4.0.js").'">"></script>' : NULL,
                'datepicker_function'           => (in_array('datepicker_function',$arr_plugins))           ? '<script src="'.base_url("assets/js/datepicker_function.js").'">"></script>' : NULL,
                'datepicker'                    => (in_array('datepicker',$arr_plugins))                    ? '<script src="'.base_url("assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js").'">"></script>' : NULL,
                'bootbox'                       => (in_array('bootbox',$arr_plugins))                       ? '<script src="'.base_url("assets/js/bootbox.min.js").'">"></script>' : NULL,
                'form_new_project'              => (in_array('form_new_project',$arr_plugins))              ? '<script src="'.base_url("assets/js/form_new_project.js").'">"></script>' : NULL,
                'toggle_switch'                 => (in_array('toggle_switch',$arr_plugins))                 ? '<script src="'.base_url("assets/js/bootstrap-toggle.min.js").'">"></script>' : NULL,
                'toggle_switch_funtion'         => (in_array('toggle_switch_funtion',$arr_plugins))         ? '<script src="'.base_url("assets/js/toggle_switch_funtion.js").'">"></script>' : NULL,
                'form_supplier_application'     => (in_array('form_supplier_application',$arr_plugins))     ? '<script src="'.base_url("assets/js/form_supplier_application.js").'">"></script>' : NULL
            );
        }
        $arr_plugin_merge = implode(' ',array_filter($arr_plugin_lists, function($var){return !is_null($var);} ));
        return $arr_plugin_merge;
    }
//  ACTIVE PAGE TREEVIEW
    function treeview_active_page($arr_active = FALSE)
    {
//        TREEVIEW LISTS
        $arr_lists = array(
        'dashboard'         => (isset($arr_active['dashboard']))        ? 'active':'',
        'user_accounts'     => (isset($arr_active['user_accounts']))    ? 'active':'',
        'supplier_page'     => (isset($arr_active['supplier_page']))    ? 'active':'',
        'quotation_page'    => (isset($arr_active['quotation_page']))   ? 'active':'',
        'project_page'      => (isset($arr_active['project_page']))     ? 'active':'',
            'dashboard_dashboard_1'     => (isset($arr_active['dashboard_dashboard_1']))    ? 'active':'',
            'dashboard_dashboard_2'     => (isset($arr_active['dashboard_dashboard_2']))    ? 'active':'',
            'user_accounts_lists'       => (isset($arr_active['user_accounts_lists']))      ? 'active':'',
            'user_accounts_access'      => (isset($arr_active['user_accounts_access']))     ? 'active':'',
            'supplier_lists'            => (isset($arr_active['supplier_lists']))           ? 'active':'',
            'quotation_lists'           => (isset($arr_active['quotation_lists']))          ? 'active':'',
            'form_new_supplier'         => (isset($arr_active['form_new_supplier']))        ? 'active':'',
            'projects_lists'            => (isset($arr_active['projects_lists']))           ? 'active':'',
            'form_new_project'          => (isset($arr_active['form_new_project']))         ? 'active':'',
            'form_new_quotation'        => (isset($arr_active['form_new_quotation']))       ? 'active':''
        );
        return $arr_lists;
    }
    //SEND MAIL
    function send_mailtype($arr_data = FALSE)
    {
        $arr_info = array(
            'email_lists'   => (isset($arr_data['email_lists']))    ? $arr_data['email_lists']  : '',
            'email_subject' => (isset($arr_data['email_subject']))  ? $arr_data['email_subject']: 'Notification',
            'email_content' => (isset($arr_data['email_content']))  ? $arr_data['email_content']: '...'
            
        );
        $this->load->library('email');
        $htmlContent = $arr_info['email_content'];

        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        $this->email->to($arr_info['email_lists']);
        $this->email->from('pangspotify041392@gmail.com','Prototype');
        $this->email->subject($arr_info['email_subject']);
        $this->email->message($htmlContent);
        $this->email->send();
    }
//  DISPLAY
    function display_page($arr_view = FALSE)
    {
            $data_view = array(
                'header_view'     => (isset($arr_view['header_view']))       ? $arr_view['header_view']     :$this->load->view('Dashboard/Header','',TRUE),
                'top_navigation'  => (isset($arr_view['top_navigation']))    ? $arr_view['top_navigation']  :$this->load->view('Dashboard/Top_navigation','',TRUE),
                'left_sidebar'    => (isset($arr_view['left_sidebar']))      ? $arr_view['left_sidebar']    :$this->load->view('Dashboard/Left_sidebar','',TRUE),
                'main_content'    => (isset($arr_view['main_content']))      ? $arr_view['main_content']    :$this->load->view('Dashboard/Main_content','',TRUE),
                'right_sidebar'   => (isset($arr_view['right_sidebar']))     ? $arr_view['right_sidebar']   :$this->load->view('Dashboard/Right_sidebar','',TRUE),
                'footer_view'     => (isset($arr_view['footer_view']))       ? $arr_view['footer_view']     :$this->load->view('Dashboard/Footer','',TRUE)
            ); 
            
            foreach($data_view as $row){
            echo $row;  
            }
    }
//  MAIN RESTRICTION
    function dashboard()
    {
        if($this->session->username !== NULL)
        {
            $data['csslinks']   = $this->csslinks(array('morris','jvectormap','datepicker','datepicker_range','wysihtml5'));
            $data['javalinks']  = $this->javalinks(array('raphael','morris','sparkline','jvectormap','jvectormap_wold_mill','knob','moment','datepicker','datepicker_range','wysihtml5'));
//            ACTIVE LINKS FOR TREE VIEW SIDEBAR
            $data['arr_treeview_active'] = $this->treeview_active_page(
                array(
                'dashboard'             => 'active',
                'dashboard_dashboard_1' => 'active'
            ));
//            HEADER SECTION
            $arr_view = array(
                'header_view'=>$this->load->view('Dashboard/Header',$data,TRUE),
                'left_sidebar'=>$this->load->view('Dashboard/Left_sidebar',$data,TRUE),
                'footer_view'=>$this->load->view('Dashboard/Footer',$data,TRUE)
            );
            $this->display_page($arr_view);
        }
        else
        {
            redirect(base_url());
        }
    }
//    SUPPLIER INFORMATION for prototype
    function supplier_information($arrData)
    {
        
        $arr_supplier_data = array(
            'suppliers_info'                         => (isset($arrData['suppliers_info']))                         ? array_merge($arrData['suppliers_info'], array('table_name'=>'prototype_suppliers_info')) : NULL,
            'suppliers_insurance'                    => (isset($arrData['suppliers_insurance']))                    ? array_merge($arrData['suppliers_insurance'], array('table_name'=>'prototype_suppliers_insurance')) : NULL,
            'suppliers_quality_assurance_warranty'   => (isset($arrData['suppliers_quality_assurance_warranty']))   ? array_merge($arrData['suppliers_quality_assurance_warranty'], array('table_name'=>'	prototype_suppliers_quality_assurance_warranty')) : NULL,
            'suppliers_services'                     => (isset($arrData['suppliers_services']))                     ? array_merge($arrData['suppliers_services'], array('table_name'=>'prototype_suppliers_services')) : NULL,
            'suppliers_trade_references'             => (isset($arrData['suppliers_trade_references']))             ? array_merge($arrData['suppliers_trade_references'], array('table_name'=>'prototype_suppliers_trade_references')) : NULL,
            'suppliers_trading_terms'                => (isset($arrData['suppliers_trading_terms']))                ? array_merge($arrData['suppliers_trading_terms'], array('table_name'=>'prototype_suppliers_trading_terms')) : NULL
            );
        $arr_return_data = array(
            'suppliers_info'                         => ($arr_supplier_data['suppliers_info'] !== NULL)                           ? $this->database_model->db_select($arr_supplier_data['suppliers_info'])                         : NULL, 
            'suppliers_insurance'                    => ($arr_supplier_data['suppliers_insurance'] !== NULL)                      ? $this->database_model->db_select($arr_supplier_data['suppliers_insurance'])                    : NULL,
            'suppliers_quality_assurance_warranty'   => ($arr_supplier_data['suppliers_quality_assurance_warranty'] !== NULL)     ? $this->database_model->db_select($arr_supplier_data['suppliers_quality_assurance_warranty'])   : NULL,
            'suppliers_services'                     => ($arr_supplier_data['suppliers_services'] !== NULL)                       ? $this->database_model->db_select($arr_supplier_data['suppliers_services'])                     : NULL,
            'suppliers_trade_references'             => ($arr_supplier_data['suppliers_trade_references'] !== NULL)               ? $this->database_model->db_select($arr_supplier_data['suppliers_trade_references'])             : NULL,
            'suppliers_trading_terms'                => ($arr_supplier_data['suppliers_trading_terms'] !== NULL)                  ? $this->database_model->db_select($arr_supplier_data['suppliers_trading_terms'])                : NULL
        );
        return $arr_return_data;
        
    }
/*                                   ADMIN PAGES                                             */
//  USER ACCOUNTS
    function user_lists()
    {
        $data['csslinks']   = $this->csslinks(array('datatables'));
        $data['javalinks']  = $this->javalinks(array('datatables','datatables_bootstrap','datatables_function'));
//       ACTIVE LINKS FOR TREE VIEW SIDEBAR
        $data['arr_treeview_active'] = $this->treeview_active_page(
            array(
            'user_accounts'         => 'active',
            'user_accounts_lists'   => 'active'
        ));
//        LOAD PAGE VIEW
        $arr_view = array(
            'header_view'   => $this->load->view('Dashboard/Header',$data,TRUE),
            'left_sidebar'  => $this->load->view('Dashboard/Left_sidebar',$data,TRUE),
            'main_content'  => $this->load->view('Admin_sections/section_users','',TRUE),
            'footer_view'   => $this->load->view('Dashboard/Footer',$data,TRUE)
        );
        $this->display_page($arr_view);
    }
//    Supplier Application Form
    function forms_supplier_application()
    {
        $data['csslinks']   = $this->csslinks(array('datatables','date'));
        $data['javalinks']  = $this->javalinks(array('datatables','datatables_bootstrap','datatables_function','datatables_addon_button','datatables_addon_print','datatables_addon_html5','datatables_addon_vfs','datatables_addon_pdf','datatables_addon_jzip','datatables_addon_flash','jquery_1_12_4','supplier_list_function'));

        //       ACTIVE LINKS FOR TREE VIEW SIDEBAR
        $data['arr_treeview_active'] = $this->treeview_active_page(
            array(
            'supplier_page'     => 'active',
            'supplier_lists'    => 'active'
        ));
        // SHOW ALL SUPPLIERS
        $arrData = array(
            'table_name'    => 'prototype_suppliers_info',
            'order_by'      => 'company_name ASC',
            'condition'     => array('status NOT IN (3)')
        );
        $data['supplier_lists'] = $this->database_model->db_select($arrData)->result();
//        LOAD PAGE VIEW
        $arr_view = array(
            'header_view'   => $this->load->view('Dashboard/Header',$data,TRUE),
            'left_sidebar'  => $this->load->view('Dashboard/Left_sidebar',$data,TRUE),
            'main_content'  => $this->load->view('Admin_sections/section_form_supplier_application','',TRUE),
            'right_sidebar' => $this->load->view('Dashboard/Right_sidebar',$data,TRUE),
            'footer_view'   => $this->load->view('Dashboard/Footer',$data,TRUE)
        );
        $this->display_page($arr_view);
    }
//    FORM NEW SUPPLIER
    function form_new_supplier()
    {
        $data['csslinks']   = $this->csslinks(array('icheck'));
        $data['javalinks']  = $this->javalinks(array('icheck','icheck_function','form_supplier_application'));

        //       ACTIVE LINKS FOR TREE VIEW SIDEBAR
        $data['arr_treeview_active'] = $this->treeview_active_page(
            array(
            'supplier_page'   => 'active',
            'form_new_supplier' => 'active'
        ));
//        LOAD PAGE VIEW
        $arr_view = array(
            'header_view'   => $this->load->view('Dashboard/Header',$data,TRUE),
            'left_sidebar'  => $this->load->view('Dashboard/Left_sidebar',$data,TRUE),
            'main_content'  => $this->load->view('Admin_sections/form_new_supplier','',TRUE),
            'right_sidebar' => $this->load->view('Dashboard/Right_sidebar','',TRUE),
            'footer_view'   => $this->load->view('Dashboard/Footer',$data,TRUE)
        );
        $this->display_page($arr_view);
    }
//    FORM SUPPLIER VALIDATION LISTS
    function form_supplier_validation_lists($arr_input_name = FALSE)
    {
        $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
//        SUPPLIERS
        if($arr_input_name !== FALSE){
            (in_array('company_name',$arr_input_name))                                      ? $this->form_validation->set_rules('company_name', ' ', 'required') : '';
            (in_array('abn_acn',$arr_input_name))                                           ? $this->form_validation->set_rules('abn_acn', ' ', 'required') : '';
            (in_array('street_address',$arr_input_name))                                    ? $this->form_validation->set_rules('street_address', ' ', 'required') : '';
            (in_array('postal_address',$arr_input_name))                                    ? $this->form_validation->set_rules('postal_address', ' ', 'required') : '';
            (in_array('phone_number_sales',$arr_input_name))                                ? $this->form_validation->set_rules('phone_number_sales', ' ', 'required') : '';
            (in_array('phone_number_accounts',$arr_input_name))                             ? $this->form_validation->set_rules('phone_number_accounts', ' ', 'required') : '';
            (in_array('email_address_sales',$arr_input_name))                               ? $this->form_validation->set_rules('email_address_sales', ' ', 'required') : '';
            (in_array('email_address_accounts',$arr_input_name))                            ? $this->form_validation->set_rules('email_address_accounts', ' ', 'required') : '';
            (in_array('website',$arr_input_name))                                           ? $this->form_validation->set_rules('website', ' ', 'required') : '';
            (in_array('account_manager_name',$arr_input_name))                              ? $this->form_validation->set_rules('account_manager_name', ' ', 'required') : '';
            (in_array('years_in_operation',$arr_input_name))                                ? $this->form_validation->set_rules('years_in_operation', ' ', 'required') : '';
            (in_array('number_of_employees',$arr_input_name))                               ? $this->form_validation->set_rules('number_of_employees', ' ', 'required') : '';
            (in_array('category_of_services',$arr_input_name))                              ? $this->form_validation->set_rules('category_of_services', ' ', 'required') : '';
            (in_array('category_of_services_others',$arr_input_name))                       ? $this->form_validation->set_rules('category_of_services_others', ' ', 'required') : '';
            (in_array('details_of_service',$arr_input_name))                                ? $this->form_validation->set_rules('details_of_service', ' ', 'required') : '';
            (in_array('expertise_certification',$arr_input_name))                           ? $this->form_validation->set_rules('expertise_certification', ' ', 'required') : '';
            (in_array('delivery_stat',$arr_input_name))                                     ? $this->form_validation->set_rules('delivery_stat', ' ', 'required') : '';
            (in_array('delivery_stat_known',$arr_input_name))                               ? $this->form_validation->set_rules('delivery_stat_known', ' ', 'required') : '';
            (in_array('standard_quote_turnaround_time',$arr_input_name))                    ? $this->form_validation->set_rules('standard_quote_turnaround_time', ' ', 'required') : '';
            (in_array('standard_manufacturing_supply_lead_time',$arr_input_name))           ? $this->form_validation->set_rules('standard_manufacturing_supply_lead_time', ' ', 'required') : '';
            (in_array('specific_of_warranty_offered_on_goods_services',$arr_input_name))    ? $this->form_validation->set_rules('specific_of_warranty_offered_on_goods_services', ' ', 'required') : '';
            (in_array('quality_assusrance_procedure',$arr_input_name))                      ? $this->form_validation->set_rules('quality_assusrance_procedure', ' ', 'required') : '';
            (in_array('details_of_qa_procedure_policy',$arr_input_name))                    ? $this->form_validation->set_rules('details_of_qa_procedure_policy', ' ', 'required') : '';
            (in_array('details_of_qa_procedure_policy_attachments',$arr_input_name))        ? $this->form_validation->set_rules('details_of_qa_procedure_policy_attachments', ' ', 'required') : '';
            (in_array('public_liability_insurer',$arr_input_name))                          ? $this->form_validation->set_rules('public_liability_insurer', ' ', 'required') : '';
            (in_array('public_liability_number',$arr_input_name))                           ? $this->form_validation->set_rules('public_liability_number', ' ', 'required') : '';
            (in_array('public_liability_indemnity',$arr_input_name))                        ? $this->form_validation->set_rules('public_liability_indemnity', ' ', 'required') : '';
            (in_array('public_liability_excess',$arr_input_name))                           ? $this->form_validation->set_rules('public_liability_excess', ' ', 'required') : '';
            (in_array('public_liability_period_cover',$arr_input_name))                     ? $this->form_validation->set_rules('public_liability_period_cover', ' ', 'required') : '';
            (in_array('professional_indemnity_insurer',$arr_input_name))                    ? $this->form_validation->set_rules('professional_indemnity_insurer', ' ', 'required') : '';
            (in_array('professional_indemnity_number',$arr_input_name))                     ? $this->form_validation->set_rules('professional_indemnity_number', ' ', 'required') : '';
            (in_array('professional_indemnity_indemnity',$arr_input_name))                  ? $this->form_validation->set_rules('professional_indemnity_indemnity', ' ', 'required') : '';
            (in_array('professional_indemnity_excess',$arr_input_name))                     ? $this->form_validation->set_rules('professional_indemnity_excess', ' ', 'required') : '';
            (in_array('professional_indemnity_period_cover',$arr_input_name))               ? $this->form_validation->set_rules('professional_indemnity_period_cover', ' ', 'required') : '';
            (in_array('workers_compensation_insurer',$arr_input_name))                      ? $this->form_validation->set_rules('workers_compensation_insurer', ' ', 'required') : '';
            (in_array('workers_compensation_number',$arr_input_name))                       ? $this->form_validation->set_rules('workers_compensation_number', ' ', 'required') : '';
            (in_array('workers_compensation_indemnity',$arr_input_name))                    ? $this->form_validation->set_rules('workers_compensation_indemnity', ' ', 'required') : '';
            (in_array('workers_compensation_excess',$arr_input_name))                       ? $this->form_validation->set_rules('workers_compensation_excess', ' ', 'required') : '';
            (in_array('workers_compensation_period_cover',$arr_input_name))                 ? $this->form_validation->set_rules('workers_compensation_period_cover', ' ', 'required') : '';
            (in_array('marine_transit_insurer',$arr_input_name))                            ? $this->form_validation->set_rules('marine_transit_insurer', ' ', 'required') : '';
            (in_array('professional_indemnity_period_cover',$arr_input_name))               ? $this->form_validation->set_rules('professional_indemnity_period_cover', ' ', 'required') : '';
            (in_array('marine_transit_number',$arr_input_name))                             ? $this->form_validation->set_rules('marine_transit_number', ' ', 'required') : '';
            (in_array('marine_transit_indemnity',$arr_input_name))                          ? $this->form_validation->set_rules('marine_transit_indemnity', ' ', 'required') : '';
            (in_array('marine_transit_excess',$arr_input_name))                             ? $this->form_validation->set_rules('marine_transit_excess', ' ', 'required') : '';
            (in_array('marine_transit_period_cover',$arr_input_name))                       ? $this->form_validation->set_rules('marine_transit_period_cover', ' ', 'required') : '';
            (in_array('payment_terms',$arr_input_name))                                     ? $this->form_validation->set_rules('payment_terms', ' ', 'required') : '';
            (in_array('payment_terms_others',$arr_input_name))                              ? $this->form_validation->set_rules('payment_terms_others', ' ', 'required') : '';
            (in_array('credit_cards_charges',$arr_input_name))                              ? $this->form_validation->set_rules('credit_cards_charges', ' ', 'required') : '';
            (in_array('credit_cards_charges_others',$arr_input_name))                       ? $this->form_validation->set_rules('credit_cards_charges_others', ' ', 'required') : '';
            (in_array('bank_account_details_bsb',$arr_input_name))                          ? $this->form_validation->set_rules('bank_account_details_bsb', ' ', 'required') : '';
            (in_array('bank_account_details_acct',$arr_input_name))                         ? $this->form_validation->set_rules('bank_account_details_acct', ' ', 'required') : '';
            (in_array('bank_account_details_financial_institution',$arr_input_name))        ? $this->form_validation->set_rules('bank_account_details_financial_institution', ' ', 'required') : '';
            (in_array('public_liability_attachments',$arr_input_name))                      ? $this->form_validation->set_rules('public_liability_attachments', ' ', 'required') : '';
            (in_array('professional_indemnity_attachments',$arr_input_name))                ? $this->form_validation->set_rules('professional_indemnity_attachments', ' ', 'required') : '';
            (in_array('workers_compensation_attachments',$arr_input_name))                  ? $this->form_validation->set_rules('workers_compensation_attachments', ' ', 'required') : '';
            (in_array('marine_transit_attachments',$arr_input_name))                        ? $this->form_validation->set_rules('marine_transit_attachments', ' ', 'required') : '';
            (in_array('referee_one_company',$arr_input_name))                               ? $this->form_validation->set_rules('referee_one_company', ' ', 'required') : '';
            (in_array('referee_one_contact_name',$arr_input_name))                          ? $this->form_validation->set_rules('referee_one_contact_name', ' ', 'required') : '';
            (in_array('referee_one_phone',$arr_input_name))                                 ? $this->form_validation->set_rules('referee_one_phone', ' ', 'required') : '';
            (in_array('referee_one_email',$arr_input_name))                                 ? $this->form_validation->set_rules('referee_one_email', ' ', 'required') : '';
            (in_array('referee_two_company',$arr_input_name))                               ? $this->form_validation->set_rules('referee_two_company', ' ', 'required') : '';
            (in_array('referee_two_contact_name',$arr_input_name))                          ? $this->form_validation->set_rules('referee_two_contact_name', ' ', 'required') : '';
            (in_array('referee_two_phone',$arr_input_name))                                 ? $this->form_validation->set_rules('referee_two_phone', ' ', 'required') : '';
            (in_array('referee_two_email',$arr_input_name))                                 ? $this->form_validation->set_rules('referee_two_email', ' ', 'required') : '';
            (in_array('referee_three_company',$arr_input_name))                             ? $this->form_validation->set_rules('referee_three_company', ' ', 'required') : '';
            (in_array('referee_three_contact_name',$arr_input_name))                        ? $this->form_validation->set_rules('referee_three_contact_name', ' ', 'required') : '';
            (in_array('referee_three_phone',$arr_input_name))                               ? $this->form_validation->set_rules('referee_three_phone', ' ', 'required') : '';
            (in_array('referee_three_email',$arr_input_name))                               ? $this->form_validation->set_rules('referee_three_email', ' ', 'required') : '';
            (in_array('status',$arr_input_name))                                            ? $this->form_validation->set_rules('status', ' ', 'required') : '';
//        QUOTATIONS
            (in_array('freight_stat',$arr_input_name))                                      ? $this->form_validation->set_rules('freight_stat', ' ', 'required') : '';
            (in_array('freight_explanation',$arr_input_name))                               ? $this->form_validation->set_rules('freight_explanation', ' ', 'required') : '';
            (in_array('payment_method',$arr_input_name))                                    ? $this->form_validation->set_rules('payment_method', ' ', 'required') : '';
            (in_array('payment_terms',$arr_input_name))                                     ? $this->form_validation->set_rules('payment_terms', ' ', 'required') : '';
            (in_array('payment_details',$arr_input_name))                                   ? $this->form_validation->set_rules('payment_details', ' ', 'required') : '';
            (in_array('payment_attachment',$arr_input_name))                                ? $this->form_validation->set_rules('payment_attachment', ' ', 'required') : '';
            (in_array('contact_name',$arr_input_name))                                      ? $this->form_validation->set_rules('contact_name', ' ', 'required') : '';
//            TENDER INVITATION
            (in_array('sub_tasks',$arr_input_name))                                         ? $this->form_validation->set_rules('sub_tasks', ' ', 'required') : '';
            (in_array('expiration_date',$arr_input_name))                                   ? $this->form_validation->set_rules('expiration_date', ' ', 'required') : '';
            (in_array('project_id',$arr_input_name))                                        ? $this->form_validation->set_rules('project_id', ' ', 'required') : '';
        }
   
    }
//    FORM VALIDATION SUBMIT
    function form_supplier_validation()
    {
            $this->form_supplier_validation_lists(array('company_name'));
            if ($this->form_validation->run() == FALSE){
                $this->form_new_supplier();
            }
            else {
//                SUPPLIERS PERSONAL INFO
                $data_insert_suppliers_info = array(
                  'company_name'                                    => $this->input->post('company_name'),
                  'abn_acn'                                         => $this->input->post('abn_acn'),
                  'street_address'                                  => $this->input->post('street_address'),
                  'postal_address'                                  => $this->input->post('postal_address'),
                  'phone_number_sales'                              => $this->input->post('phone_number_sales'),
                  'phone_number_accounts'                           => $this->input->post('phone_number_accounts'),
                  'email_address_sales'                             => $this->input->post('email_address_sales'),
                  'email_address_accounts'                          => $this->input->post('email_address_accounts'),
                  'website'                                         => $this->input->post('website'),
                  'account_manager_name'                            => $this->input->post('account_manager_name'),
                  'years_in_operation'                              => $this->input->post('years_in_operation'),
                  'number_of_employees'                             => $this->input->post('number_of_employees'),
                  'status'                                          => $this->input->post('supplier_status')
                );
                $id = $this->database_model->prototype_insert_db('prototype_suppliers_info',$data_insert_suppliers_info);
//                IF SUPPLIERS ARE SUBMITED
                    if($this->input->post('supplier_status') == 0){
    //                USERR ACCOUNT
                        $password = str_replace(' ','',$this->input->post('company_name').$id);
                        $data_user_account = array(
                            'user_name'           => $this->input->post('company_name'),
                            'password'            => md5(sha1($password)),
                            'first_name'          => $this->input->post('company_name'),
                            'supplier_company_id' => $id
                        );
                        $this->database_model->prototype_insert_db('prototype_users',$data_user_account);
                        $this->send_mailtype(array(
                            'email_lists'   => $this->input->post('email_address_accounts'),
                            'email_subject' => 'Tender User Account Credentials',
                            'email_content' => '<p><b>Username:</b> '.$this->input->post('company_name').'</p>'.'<p><b>Password:</b> '.$password.'<p>'
                        ));
                        
                    }
//                SUPPLIERS SERVICES
                $category_of_services_merge = ($this->input->post('category_of_services') !== NULL) ? implode(',',array_filter($this->input->post('category_of_services'), function($var){return !is_null($var);} )) : '' ;
                $data_insert_suppliers_services = array(
                  'supplier_id'                             => $id,
                  'category_of_services'                    => $category_of_services_merge,
                  'category_of_services_others'             => $this->input->post('category_of_services_others'),
                  'details_of_service'                      => $this->input->post('details_of_service'),
                  'expertise_certification'                 => $this->input->post('expertise_certification'),
                  'delivery_stat'                           => $this->input->post('delivery_stat'),
                  'delivery_stat_known'                     => $this->input->post('delivery_stat_known'),
                  'standard_quote_turnaround_time'          => $this->input->post('standard_quote_turnaround_time'),
                  'standard_manufacturing_supply_lead_time' => $this->input->post('standard_manufacturing_supply_lead_time')
                );
                $this->database_model->prototype_insert_db('prototype_suppliers_services',$data_insert_suppliers_services);
//                SUPPLIERS QUALITY ASSURANCE
                
                $qa_procedure_attachment_name = $id.'_attachment_';
                $qa_procedure_attachment_name = $this->upload_files('./assets/attachments/quality assurance/', $qa_procedure_attachment_name, $_FILES['details_of_qa_procedure_policy_attachments'],'details_of_qa_procedure_policy_attachments');
                $qa_procedure_attachment_name = $qa_procedure_attachment_name['file_name'];
                
                $data_insert_suppliers_quality_assurance_warranty = array(
                  'supplier_id'                                     => $id,
                  'specific_of_warranty_offered_on_goods_services'  => $this->input->post('specific_of_warranty_offered_on_goods_services'),
                  'quality_assurance_procedure'                     => $this->input->post('quality_assurance_procedure'),
                  'details_of_qa_procedure_policy'                  => $this->input->post('details_of_qa_procedure_policy'),
                  'details_of_qa_procedure_policy_attachments'      => $qa_procedure_attachment_name
                );
                $this->database_model->prototype_insert_db('prototype_suppliers_quality_assurance_warranty',$data_insert_suppliers_quality_assurance_warranty);
//                SUPPLIERS INSURANCE
                $public_liability_attachment_name       = $id.'_public_liability';
                $professional_indemnity_attachment_name = $id.'_professional_indemnity';
                $workers_compensation_attachment_name   = $id.'_workers_compensation';
                $marine_transit_attachment_name         = $id.'_marine_transit';
//                PUBLIC LIABILITY ATTACHMENT
                $public_liability_attachment_name = $id.'_public_liability';
                $public_liability_attachment_name = $this->upload_files('./assets/attachments/public liability/', $public_liability_attachment_name, $_FILES['public_liability_attachment'],'public_liability_attachment');
                $public_liability_attachment_name = $public_liability_attachment_name['file_name'];
//                PUBLIC Professional INDEMNITY
                $professional_indemnity_attachment_name = $id.'_public_liability';
                $professional_indemnity_attachment_name = $this->upload_files('./assets/attachments/professional indemnity/', $professional_indemnity_attachment_name, $_FILES['professional_indemnity_attachment'],'professional_indemnity_attachment');
                $professional_indemnity_attachment_name = $professional_indemnity_attachment_name['file_name'];
//                PUBLIC Workers Compensation
                $workers_compensation_attachment_name = $id.'_public_liability';
                $workers_compensation_attachment_name = $this->upload_files('./assets/attachments/workers compensation/', $workers_compensation_attachment_name, $_FILES['workers_compensation_attachment'],'workers_compensation_attachment');
                $workers_compensation_attachment_name = $workers_compensation_attachment_name['file_name'];
//                PUBLIC MARINE
                $marine_transit_attachment_name = $id.'_public_liability';
                $marine_transit_attachment_name = $this->upload_files('./assets/attachments/public liability/', $marine_transit_attachment_name, $_FILES['marine_transit_attachment'],'marine_transit_attachment');
                $marine_transit_attachment_name = $marine_transit_attachment_name['file_name'];
                
                
                $data_insert_suppliers_insurance = array(
                    array(
                        'supplier_id'       => $id,
                        'insurance_type'    => 'Public Liability',
                        'insurer'           => $this->input->post('public_liability_insurer'),
                        'number'            => $this->input->post('public_liability_number'),
                        'indemnity'         => $this->input->post('public_liability_indemnity'),
                        'excess'            => $this->input->post('public_liability_excess'),
                        'period_cover'      => $this->input->post('public_liability_period_cover'),
                        'attachments'       => $public_liability_attachment_name
                    ),
                    array(
                        'supplier_id'       => $id,
                        'insurance_type'    => 'Professional Indemnity',
                        'insurer'           => $this->input->post('professional_indemnity_insurer'),
                        'number'            => $this->input->post('professional_indemnity_number'),
                        'indemnity'         => $this->input->post('professional_indemnity_indemnity'),
                        'excess'            => $this->input->post('professional_indemnity_excess'),
                        'period_cover'      => $this->input->post('professional_indemnity_period_cover'),
                        'attachments'       => $professional_indemnity_attachment_name
                    ),
                    array(
                        'supplier_id'       => $id,
                        'insurance_type'    => 'Workers Compensation',
                        'insurer'           => $this->input->post('workers_compensation_insurer'),
                        'number'            => $this->input->post('workers_compensation_number'),
                        'indemnity'         => $this->input->post('workers_compensation_indemnity'),
                        'excess'            => $this->input->post('workers_compensation_excess'),
                        'period_cover'      => $this->input->post('workers_compensation_period_cover'),
                        'attachments'       => $workers_compensation_attachment_name
                    ),
                    array(
                        'supplier_id'       => $id,
                        'insurance_type'    => 'Marine Transit',
                        'insurer'           => $this->input->post('marine_transit_insurer'),
                        'number'            => $this->input->post('marine_transit_number'),
                        'indemnity'         => $this->input->post('marine_transit_indemnity'),
                        'excess'            => $this->input->post('marine_transit_excess'),
                        'period_cover'      => $this->input->post('marine_transit_period_cover'),
                        'attachments'       => $marine_transit_attachment_name
                    )
                );
                $this->database_model->prototype_insert_bulk_db('prototype_suppliers_insurance',$data_insert_suppliers_insurance);
//                SUPPLIERS TRADING TERMS
                $trading_payment_terms = ($this->input->post('payment_terms') !== NULL) ? implode(',',array_filter($this->input->post('payment_terms'), function($var){return !is_null($var);} )) : NULL;
                $trading_credit_cards_charges = ($this->input->post('payment_terms') !== NULL) ? implode(',',array_filter($this->input->post('credit_cards_charges'), function($var){return !is_null($var);} )) : NULL;
                $data_insert_suppliers_trading_terms = array(
                  'supplier_id'                                 => $id,
                  'payment_terms'                               => $trading_payment_terms,
                  'payment_terms_others'                        => $this->input->post('payment_terms_others'),
                  'credit_cards_charges'                        => $trading_credit_cards_charges,
                  'credit_cards_charges_others'                 => $this->input->post('credit_cards_charges_others'),
                  'bank_account_details_bsb'                    => $this->input->post('bank_account_details_bsb'),
                  'bank_account_details_acct'                   => $this->input->post('bank_account_details_acct'),
                  'bank_account_details_financial_institution'  => $this->input->post('bank_account_details_financial_institution'),
                  'accounting_system_software'                  => $this->input->post('accounting_system_software')
                );
                $this->database_model->prototype_insert_db('prototype_suppliers_trading_terms',$data_insert_suppliers_trading_terms);
//                SUPPLIERS REFERENCES
                $data_insert_suppliers_trade_references = array(
                    array(
                          'supplier_id'     => $id,
                          'reference_no'    => 1,
                          'company'         => $this->input->post('referee_one_company'),
                          'contact_name'    => $this->input->post('referee_one_contact_name'),
                          'phone'           => $this->input->post('referee_one_phone'),
                          'email'           => $this->input->post('referee_one_email')
                    ),
                    array(
                          'supplier_id'     => $id,
                          'reference_no'    => 2,
                          'company'         => $this->input->post('referee_two_company'),
                          'contact_name'    => $this->input->post('referee_two_contact_name'),
                          'phone'           => $this->input->post('referee_two_phone'),
                          'email'           => $this->input->post('referee_two_email')
                    ),
                    array(
                          'supplier_id'     => $id,
                          'reference_no'    => 3,
                          'company'         => $this->input->post('referee_three_company'),
                          'contact_name'    => $this->input->post('referee_three_contact_name'),
                          'phone'           => $this->input->post('referee_three_phone'),
                          'email'           => $this->input->post('referee_three_email')
                    ),
                    
                );
                $this->database_model->prototype_insert_bulk_db('prototype_suppliers_trade_references',$data_insert_suppliers_trade_references);
                redirect(base_url('Admin/forms_supplier_application'));
            }
        }
    //    FORM VALIDATION SUBMIT
    function form_supplier_validation_update()
    {
        if(sha1(sha1($this->input->post('companyid'))) == $this->input->post('companycode')){
                $this->form_supplier_validation_lists(array('company_name'));
                if ($this->form_validation->run() == FALSE)
                {
                    $this->form_new_supplier();
                }
                else 
                {
    //                SUPPLIERS PERSONAL INFO
                    $id = $this->input->post('companyid');
                    $data_update_suppliers_info = array(
                      'company_name'                                    => $this->input->post('company_name'),
                      'abn_acn'                                         => $this->input->post('abn_acn'),
                      'street_address'                                  => $this->input->post('street_address'),
                      'postal_address'                                  => $this->input->post('postal_address'),
                      'phone_number_sales'                              => $this->input->post('phone_number_sales'),
                      'phone_number_accounts'                           => $this->input->post('phone_number_accounts'),
                      'email_address_sales'                             => $this->input->post('email_address_sales'),
                      'email_address_accounts'                          => $this->input->post('email_address_accounts'),
                      'website'                                         => $this->input->post('website'),
                      'account_manager_name'                            => $this->input->post('account_manager_name'),
                      'years_in_operation'                              => $this->input->post('years_in_operation'),
                      'number_of_employees'                             => $this->input->post('number_of_employees'),
                      'status'                                          => $this->input->post('supplier_status')
                    );
//                IF SUPPLIERS ARE SUBMITED
                    if($this->input->post('supplier_status') == 0){
    //                USERR ACCOUNT
                        $password = str_replace(' ','',$this->input->post('company_name').$id);
                        $data_user_account = array(
                            'user_name'           => $this->input->post('company_name'),
                            'password'            => md5(sha1($password)),
                            'first_name'          => $this->input->post('company_name'),
                            'supplier_company_id' => $id
                        );
                        $this->database_model->prototype_insert_db('prototype_users',$data_user_account);
                        $this->send_mailtype(array(
                            'email_lists'   => $this->input->post('email_address_accounts'),
                            'email_subject' => 'Tender User Account Credentials',
                            'email_content' => '<p><b>Username:</b> '.$this->input->post('company_name').'</p>'.'<p><b>Password:</b> '.$password.'<p>'
                        ));
                        
                    }
                    $data_update_suppliers_info_condition = array('autonum = '.$this->input->post('companyid'));
                    $this->database_model->prototype_edit('prototype_suppliers_info',$data_update_suppliers_info,$data_update_suppliers_info_condition);
    //                SUPPLIERS SERVICES
                    $category_of_services_merge = ($this->input->post('category_of_services') !== NULL) ? implode(',',array_filter($this->input->post('category_of_services'), function($var){return !is_null($var);} )) : '' ;
                    $data_update_suppliers_services = array(
                      'supplier_id'                             => $id,
                      'category_of_services'                    => $category_of_services_merge,
                      'category_of_services_others'             => $this->input->post('category_of_services_others'),
                      'details_of_service'                      => $this->input->post('details_of_service'),
                      'expertise_certification'                 => $this->input->post('expertise_certification'),
                      'delivery_stat'                           => $this->input->post('delivery_stat'),
                      'delivery_stat_known'                     => $this->input->post('delivery_stat_known'),
                      'standard_quote_turnaround_time'          => $this->input->post('standard_quote_turnaround_time'),
                      'standard_manufacturing_supply_lead_time' => $this->input->post('standard_manufacturing_supply_lead_time')
                    );
                    $data_update_suppliers_sub_info_condiotion = array('supplier_id = '.$id);
                    $this->database_model->prototype_edit('prototype_suppliers_services',$data_update_suppliers_services,$data_update_suppliers_sub_info_condiotion);
    //                SUPPLIERS QUALITY ASSURANCE

                    $qa_procedure_attachment_name = $id.'_attachment_';
                    $qa_procedure_attachment_name = $this->upload_files('./assets/attachments/quality assurance/', $qa_procedure_attachment_name, $_FILES['details_of_qa_procedure_policy_attachments'],'details_of_qa_procedure_policy_attachments');
                    $qa_procedure_attachment_name = ($qa_procedure_attachment_name['file_name'] != '') ? $qa_procedure_attachment_name['file_name']: $this->input->post('standard_manufacturing_supply_lead_time');

                    $data_update_suppliers_quality_assurance_warranty = array(
                      'supplier_id'                                     => $id,
                      'specific_of_warranty_offered_on_goods_services'  => $this->input->post('specific_of_warranty_offered_on_goods_services'),
                      'quality_assurance_procedure'                     => $this->input->post('quality_assurance_procedure'),
                      'details_of_qa_procedure_policy'                  => $this->input->post('details_of_qa_procedure_policy'),
                      'details_of_qa_procedure_policy_attachments'      => ($qa_procedure_attachment_name !='') ? $qa_procedure_attachment_name : $this->input->post('details_of_qa_procedure_policy_attachments_old_file')
                    );
                    $this->database_model->prototype_edit('prototype_suppliers_quality_assurance_warranty',$data_update_suppliers_quality_assurance_warranty,$data_update_suppliers_sub_info_condiotion);
    //                SUPPLIERS INSURANCE
                    $public_liability_attachment_name       = $id.'_public_liability';
                    $professional_indemnity_attachment_name = $id.'_professional_indemnity';
                    $workers_compensation_attachment_name   = $id.'_workers_compensation';
                    $marine_transit_attachment_name         = $id.'_marine_transit';
    //                PUBLIC LIABILITY ATTACHMENT
                    $public_liability_attachment_name = $id.'_public_liability';
                    $public_liability_attachment_name = $this->upload_files('./assets/attachments/public liability/', $public_liability_attachment_name, $_FILES['public_liability_attachment'],'public_liability_attachment');
                    $public_liability_attachment_name = $public_liability_attachment_name['file_name'];
    //                PUBLIC Professional INDEMNITY
                    $professional_indemnity_attachment_name = $id.'_public_liability';
                    $professional_indemnity_attachment_name = $this->upload_files('./assets/attachments/professional indemnity/', $professional_indemnity_attachment_name, $_FILES['professional_indemnity_attachment'],'professional_indemnity_attachment');
                    $professional_indemnity_attachment_name = $professional_indemnity_attachment_name['file_name'];
    //                PUBLIC Workers Compensation
                    $workers_compensation_attachment_name = $id.'_public_liability';
                    $workers_compensation_attachment_name = $this->upload_files('./assets/attachments/workers compensation/', $workers_compensation_attachment_name, $_FILES['workers_compensation_attachment'],'workers_compensation_attachment');
                    $workers_compensation_attachment_name = $workers_compensation_attachment_name['file_name'];
    //                PUBLIC MARINE
                    $marine_transit_attachment_name = $id.'_public_liability';
                    $marine_transit_attachment_name = $this->upload_files('./assets/attachments/public liability/', $marine_transit_attachment_name, $_FILES['marine_transit_attachment'],'marine_transit_attachment');
                    $marine_transit_attachment_name = $marine_transit_attachment_name['file_name'];


                    $data_insert_suppliers_insurance = array(
                        array(
                            'insurer'           => $this->input->post('public_liability_insurer'),
                            'number'            => $this->input->post('public_liability_number'),
                            'indemnity'         => $this->input->post('public_liability_indemnity'),
                            'excess'            => $this->input->post('public_liability_excess'),
                            'period_cover'      => $this->input->post('public_liability_period_cover'),
                            'attachments'       => ($public_liability_attachment_name !='') ? $public_liability_attachment_name:$this->input->post('public_liability_old_file')
                        ),
                        array(
                            'insurer'           => $this->input->post('professional_indemnity_insurer'),
                            'number'            => $this->input->post('professional_indemnity_number'),
                            'indemnity'         => $this->input->post('professional_indemnity_indemnity'),
                            'excess'            => $this->input->post('professional_indemnity_excess'),
                            'period_cover'      => $this->input->post('professional_indemnity_period_cover'),
                            'attachments'       => ($professional_indemnity_attachment_name !='') ? $professional_indemnity_attachment_name:$this->input->post('professional_indemnity_old_file')
                        ),
                        array(
                            'insurer'           => $this->input->post('workers_compensation_insurer'),
                            'number'            => $this->input->post('workers_compensation_number'),
                            'indemnity'         => $this->input->post('workers_compensation_indemnity'),
                            'excess'            => $this->input->post('workers_compensation_excess'),
                            'period_cover'      => $this->input->post('workers_compensation_period_cover'),
                            'attachments'       => ($workers_compensation_attachment_name !='') ? $workers_compensation_attachment_name:$this->input->post('workers_compensation_old_file')
                        ),
                        array(
                            'insurer'           => $this->input->post('marine_transit_insurer'),
                            'number'            => $this->input->post('marine_transit_number'),
                            'indemnity'         => $this->input->post('marine_transit_indemnity'),
                            'excess'            => $this->input->post('marine_transit_excess'),
                            'period_cover'      => $this->input->post('marine_transit_period_cover'),
                            'attachments'       => ($marine_transit_attachment_name !='') ? $marine_transit_attachment_name:$this->input->post('marine_transit_old_file')
                        )
                    );
                    //UPDATE INSURANCE
                    $arr_condition_insurance= array(
                        array('insurance_type = "Public Liability"','supplier_id = '.$id),
                        array('insurance_type = "Professional Indemnity"','supplier_id = '.$id),
                        array('insurance_type = "Workers Compensation"','supplier_id = '.$id),
                        array('insurance_type = "Marine Transit"','supplier_id = '.$id)
                    );
                    $insurance_arr_condition_count = 0;
                    foreach($data_insert_suppliers_insurance as $row){
                        $this->database_model->prototype_edit('prototype_suppliers_insurance',$row,$arr_condition_insurance[$insurance_arr_condition_count]);
                        $insurance_arr_condition_count++;
                    }
    //                SUPPLIERS TRADING TERMS
                    $trading_payment_terms = ($this->input->post('payment_terms') !== NULL) ? implode(',',array_filter($this->input->post('payment_terms'), function($var){return !is_null($var);} )) : NULL;
                    $trading_credit_cards_charges = ($this->input->post('payment_terms') !== NULL) ? implode(',',array_filter($this->input->post('credit_cards_charges'), function($var){return !is_null($var);} )) : NULL;
                    $data_insert_suppliers_trading_terms = array(
                      'supplier_id'                                 => $id,
                      'payment_terms'                               => $trading_payment_terms,
                      'payment_terms_others'                        => $this->input->post('payment_terms_others'),
                      'credit_cards_charges'                        => $trading_credit_cards_charges,
                      'credit_cards_charges_others'                 => $this->input->post('credit_cards_charges_others'),
                      'bank_account_details_bsb'                    => $this->input->post('bank_account_details_bsb'),
                      'bank_account_details_acct'                   => $this->input->post('bank_account_details_acct'),
                      'bank_account_details_financial_institution'  => $this->input->post('bank_account_details_financial_institution'),
                      'accounting_system_software'                  => $this->input->post('accounting_system_software')
                    );
                    $this->database_model->prototype_edit('prototype_suppliers_trading_terms',$data_insert_suppliers_trading_terms,$data_update_suppliers_sub_info_condiotion);
    //                SUPPLIERS REFERENCES
                    $data_insert_suppliers_trade_references = array(
                        array(
                              'supplier_id'     => $id,
                              'reference_no'    => 1,
                              'company'         => $this->input->post('referee_one_company'),
                              'contact_name'    => $this->input->post('referee_one_contact_name'),
                              'phone'           => $this->input->post('referee_one_phone'),
                              'email'           => $this->input->post('referee_one_email')
                        ),
                        array(
                              'supplier_id'     => $id,
                              'reference_no'    => 2,
                              'company'         => $this->input->post('referee_two_company'),
                              'contact_name'    => $this->input->post('referee_two_contact_name'),
                              'phone'           => $this->input->post('referee_two_phone'),
                              'email'           => $this->input->post('referee_two_email')
                        ),
                        array(
                              'supplier_id'     => $id,
                              'reference_no'    => 3,
                              'company'         => $this->input->post('referee_three_company'),
                              'contact_name'    => $this->input->post('referee_three_contact_name'),
                              'phone'           => $this->input->post('referee_three_phone'),
                              'email'           => $this->input->post('referee_three_email')
                        ),

                    );
                    $arr_condition_rederences= array(
                        array('reference_no = "1"','supplier_id = '.$id),
                        array('reference_no = "2"','supplier_id = '.$id),
                        array('reference_no = "3"','supplier_id = '.$id),
                        array('reference_no = "4"','supplier_id = '.$id)
                    );
                    $references_arr_condition_count = 0;
                    foreach($data_insert_suppliers_trade_references as $row){
                        $this->database_model->prototype_edit('prototype_suppliers_trade_references',$row,$arr_condition_rederences[$references_arr_condition_count]);
                        $references_arr_condition_count++;
                    }
                    redirect(base_url('Admin/view_supplier/'.$id));
                }
            }
        else
        {
            
        }
    }
    //    FORM NEW SUPPLIER
    function form_new_project()
    {
        $data['csslinks']   = $this->csslinks(array('icheck','wysihtml5','toggle_switch'));
        $data['javalinks']  = $this->javalinks(array('icheck','icheck_function','form_new_project','wysihtml5','wysihtml5_function','toggle_switch','toggle_switch_funtion'));

        //       ACTIVE LINKS FOR TREE VIEW SIDEBAR
        $data['arr_treeview_active'] = $this->treeview_active_page(
            array(
            'project_page'   => 'active',
            'form_new_project' => 'active'
        ));
//        LOAD PAGE VIEW
        $arr_view = array(
            'header_view'   => $this->load->view('Dashboard/Header',$data,TRUE),
            'left_sidebar'  => $this->load->view('Dashboard/Left_sidebar',$data,TRUE),
            'main_content'  => $this->load->view('Admin_sections/form_new_projects','',TRUE),
            'right_sidebar' => $this->load->view('Dashboard/Right_sidebar','',TRUE),
            'footer_view'   => $this->load->view('Dashboard/Footer',$data,TRUE)
        );
        $this->display_page($arr_view);
    }
//    FUNCTION UPLOAD FILE
    function upload_files($path, $title, $file,$input_name)
    {        
        if($file['name'] != '')
                {
                    $config['upload_path']          = $path;
                    $config['allowed_types']        = 'gif|jpg|jpeg|jpeg|png|pdf';
                    $config['file_name']            = $title;
//                    $config['max_size']             = 100;
//                    $config['max_width']            = 1024;
//                    $config['max_height']           = 768;
                    $this->load->library('upload', $config);
                    if ( ! $upload_data = $this->upload->do_upload($input_name))
                        {
                                echo "error";
                        }
                    else
                        {
                            $uploaded_filename = $this->upload->data();
                        }
                }
        else{
            $uploaded_filename['file_name'] = '';
        }
        return $uploaded_filename;
    }
//    SUPPLIER PROFILE
    function view_supplier($supplierid = FALSE)
    {
        if($supplierid == FALSE){ redirect(base_url('Admin/forms_supplier_application')); }
        $data['csslinks']   = $this->csslinks(array('datatables','fancybox','datepicker'));
        $data['javalinks']  = $this->javalinks(array('datatables','datatables_bootstrap','datatables_function','date_picker','datepicker_function','supplier_view_function','fancybox_function','fancybox','jquery_ui','bootbox'));
        //       ACTIVE LINKS FOR TREE VIEW SIDEBAR
        $data['arr_treeview_active'] = $this->treeview_active_page(
            array(
                'supplier_page'     => 'active',
                'supplier_lists'    => 'active'
        ));
//      CONDITION
        $supplier_information_condition = array('condition' => array('autonum = '.$supplierid));
        $supplier_sub_table_condition = array('condition' => array('supplier_id = '.$supplierid));
        $arr_supplier_full_info = array(
            'suppliers_info'                         => $supplier_information_condition,
            'suppliers_insurance'                    => $supplier_sub_table_condition,
            'suppliers_quality_assurance_warranty'   => $supplier_sub_table_condition,
            'suppliers_services'                     => $supplier_sub_table_condition,
            'suppliers_trade_references'             => $supplier_sub_table_condition,
            'suppliers_trading_terms'                => $supplier_sub_table_condition
        );
        $supplier_full_info = $this->supplier_information($arr_supplier_full_info);
        
//        SUPPLIER PERSONAL INFO
        $data['supplier_company_details']           = $supplier_full_info['suppliers_info']->row();
        $data['supplier_services']                  = $supplier_full_info['suppliers_services']->row();
        $data['supplier_services_category']         = explode(',',$data['supplier_services']->category_of_services);
        $data['supplier_quality_assurance']         = $supplier_full_info['suppliers_quality_assurance_warranty']->row();
        $data['supplier_insurance']                 = $supplier_full_info['suppliers_insurance']->result();
        $data['trading_terms']                      = $supplier_full_info['suppliers_trading_terms']->row();
        $data['trading_terms_payment_terms']        = explode(',',$data['trading_terms']->payment_terms);
        $data['trading_terms_credit_card_charges']  = explode(',',$data['trading_terms']->credit_cards_charges);
        $data['trading_references']                 = $supplier_full_info['suppliers_trade_references']->result();
//        SUPPLIER LISTS
        $arrData_supplier_lists = array(
            'table_name'    => 'prototype_suppliers_info',
            'column'        => 'autonum,company_name',
            'condition'     => array('status NOT IN (3)')
        );
        $data['supplier_lists'] = $this->database_model->db_select($arrData_supplier_lists)->result();
//        PROJECT LISTS
        $arrData_project_lists = array(
            'table_name'    => 'prototype_projects_sub',
            'condition'     => array('status = 0','project_id = 0')
        );
//        SUPPLIER TENDER INVITATIONS
        $arrData_supplier_tender_invitatoin = array(
            'table_name' => 'prototype_tender_invitation',
            'condition'  => array('supplier_id = '.$supplierid.'')
        );
        $data['tender_invitations'] = $this->database_model->db_select($arrData_supplier_tender_invitatoin)->result();
        
        foreach($data['tender_invitations'] as $key => $row){
            $data['tender_invitations'][$key]->sub_task_name = array();
            $project_sub_task_ids = explode(',',$row->project_sub_ids);
            foreach($project_sub_task_ids as $row2){
                $sub_task_arr = array(
                    'table_name'    => 'prototype_projects_sub',
                    'condition'     => array('autonum = '.$row2),
                    'columns'       => array('*','(SELECT prototype_projects.project_name FROM prototype_projects WHERE prototype_projects.autonum = prototype_projects_sub.project_id) as pr_name')
                );
                $sub_tasks_data = $this->database_model->db_select($sub_task_arr)->row();
             $data['tender_invitations'][$key]->sub_task_name[] = $sub_tasks_data->project_name;   
             $data['tender_invitations'][$key]->project_name[0] = $sub_tasks_data->pr_name;   
            }
        }
        $data['project_lists'] = $this->database_model->db_select($arrData_project_lists)->result();
        $arr_view = array(
            'header_view'   => $this->load->view('Dashboard/Header',$data,TRUE),
            'left_sidebar'  => $this->load->view('Dashboard/Left_sidebar',$data,TRUE),
            'main_content'  => $this->load->view('Supplier/supplier_view',$data,TRUE),
            'footer_view'   => $this->load->view('Dashboard/Footer',$data,TRUE)
        );
        $this->display_page($arr_view);
    }
//    FORM NEW SUPPLIER
    function form_edit_supplier($supplierid = FALSE)
    {
        $data['csslinks']   = $this->csslinks(array('icheck','fancybox'));
        $data['javalinks']  = $this->javalinks(array('icheck','icheck_function','form_supplier_application','fancybox','fancybox_function','bootbox'));

        //       ACTIVE LINKS FOR TREE VIEW SIDEBAR
        $data['arr_treeview_active'] = $this->treeview_active_page(
            array(
            'supplier_page'     => 'active',
            'form_new_supplier' => 'active'
        ));
        //      CONDITION
        $supplier_information_condition = array('condition' => array('autonum = '.$supplierid));
        $supplier_sub_table_condition = array('condition' => array('supplier_id = '.$supplierid));
        $arr_supplier_full_info = array(
            'suppliers_info'                         => $supplier_information_condition,
            'suppliers_insurance'                    => $supplier_sub_table_condition,
            'suppliers_quality_assurance_warranty'   => $supplier_sub_table_condition,
            'suppliers_services'                     => $supplier_sub_table_condition,
            'suppliers_trade_references'             => $supplier_sub_table_condition,
            'suppliers_trading_terms'                => $supplier_sub_table_condition
        );
        $supplier_full_info = $this->supplier_information($arr_supplier_full_info);
        
//        SUPPLIER PERSONAL INFO
        $data['supplier_company_details']           = $supplier_full_info['suppliers_info']->row();
        $data['supplier_services']                  = $supplier_full_info['suppliers_services']->row();
        $data['supplier_services_category']         = explode(',',$data['supplier_services']->category_of_services);
        $data['supplier_quality_assurance']         = $supplier_full_info['suppliers_quality_assurance_warranty']->row();
        $data['supplier_insurance']                 = $supplier_full_info['suppliers_insurance']->result();
        $data['trading_terms']                      = $supplier_full_info['suppliers_trading_terms']->row();
        $data['trading_terms_payment_terms']        = explode(',',$data['trading_terms']->payment_terms);
        $data['trading_terms_credit_card_charges']  = explode(',',$data['trading_terms']->credit_cards_charges);
        $data['trading_references']                 = $supplier_full_info['suppliers_trade_references']->result();
//        LOAD PAGE VIEW
        $arr_view = array(
            'header_view'   => $this->load->view('Dashboard/Header',$data,TRUE),
            'left_sidebar'  => $this->load->view('Dashboard/Left_sidebar',$data,TRUE),
            'main_content'  => $this->load->view('Admin_sections/form_edit_supplier','',TRUE),
            'footer_view'   => $this->load->view('Dashboard/Footer',$data,TRUE)
        );
        $this->display_page($arr_view);
    }
//    FROM NEW QUOTATION
    function form_new_quotation()
    {
        $data['csslinks']   = $this->csslinks(array('icheck'));
        $data['javalinks']  = $this->javalinks(array('icheck','icheck_function','currency_format_all','currency_format','currency_function','form_quotation'));

        //       ACTIVE LINKS FOR TREE VIEW SIDEBAR
        $data['arr_treeview_active'] = $this->treeview_active_page(
            array(
            'quotation_page'        => 'active',
            'form_new_quotation'    => 'active'
        ));
//        SUPPLIER DETAILS
        //      CONDITION
        $supplier_information_condition = array('condition' => array('autonum = '.$this->session->supplier_company_id));
        $arr_supplier_full_info = array(
            'suppliers_info'    => $supplier_information_condition
        );
        $supplier_details = $this->supplier_information($arr_supplier_full_info);
        $data['suppliers_info'] = $supplier_details['suppliers_info']->row();
        $arr_project_list = array(
            'table_name'    => 'prototype_projects',
            'condition'     => array('status = 0')
        );
        $data['project_lists'] = $this->database_model->db_select($arr_project_list)->result();
        
//        LOAD PAGE VIEW
        $arr_view = array(
            'header_view'   => $this->load->view('Dashboard/Header',$data,TRUE),
            'left_sidebar'  => $this->load->view('Dashboard/Left_sidebar',$data,TRUE),
            'main_content'  => $this->load->view('Admin_sections/form_new_quotation','',TRUE),
            'footer_view'   => $this->load->view('Dashboard/Footer',$data,TRUE)
        );
        $this->display_page($arr_view);
    }
//    FORM QUOTATION VALIDATION SUBMIT
    function form_quotation_validation()
    {
            $this->form_supplier_validation_lists(array('contact_name'));
            if ($this->form_validation->run() == FALSE){
                $this->form_new_quotation();
            }
            else {
//                QUOTATION INFO
                $payment_attachment = 'payment_attachment_'.$this->session->supplier_company_id;
                $payment_attachment = $this->upload_files('./assets/attachments/quality assurance/', $payment_attachment, $_FILES['payment_attachment'],'payment_attachment');
                $payment_attachment = $payment_attachment['file_name'];
                $data_insert_quotation_info = array(
                  'supplier_id'         => ($this->input->post('supplier_id') !== null) ? $this->input->post('supplier_id') : $this->session->supplier_company_id,
                  'freight_stat'        => $this->input->post('freight_stat'),
                  'contact_name'        => $this->input->post('contact_name'),
                  'contact_number'      => $this->input->post('contact_number'),
                  'contact_email'       => $this->input->post('contact_email'),
                  'freight_explanation' => $this->input->post('freight_explanation'),
                  'payment_method'      => $this->input->post('payment_method'),
                  'payment_terms'       => $this->input->post('payment_terms'),
                  'payment_details'     => $this->input->post('payment_details'),
                  'payment_attachment'  => $payment_attachment,
                  'status'              => $this->input->post('quotation_status'),
                  'creted_by_id'        => $this->session->autonum
                );
//                REMOVE PAYMENT ATTACHMENT IF NULL
                if($payment_attachment == ''){ unset($data_insert_quotation_info['payment_attachment']); }
//                    UPDATE QUOTATATION
                if(($this->input->post('quotation_id_enc') !== NULL) && ($this->input->post('quotation_id_enc') == sha1(sha1(sha1($this->input->post('quotation_id')))))){
                    $id = $this->input->post('quotation_id');
                    $this->database_model->prototype_delete(array(
                        'table_name'    => 'prototype_quotations_product_lists',
                        'condition'     => array('quotation_id = '.$id)
                    ));
                    $this->database_model->prototype_edit('prototype_quotations',$data_insert_quotation_info,array('condition'=>'autonum='.$id));
                }else
//                    NEW QUOTATION APPLICATION
                {
                    $id = $this->database_model->prototype_insert_db('prototype_quotations',$data_insert_quotation_info);
                }
//                IF SUBMITTED
                if($this->input->post('quotation_status') == 0)
                {
                    $quotation_submitted_arr = array(
                            'quotation_id'    => $id
                        );
                        $this->database_model->prototype_insert_db_manual('prototype_quotations_submitted',$quotation_submitted_arr);
                }

                                
                $quotation_products = array();
                $count = 0;
                foreach($this->input->post('product_name') as $row){
                    if($this->input->post('product_name')[$count] !='' && $this->input->post('description')[$count] != ''){
                        $quotation_products[] = array(
                            'quotation_id'  => $id,
                            'product_name'  => $this->input->post('product_name')[$count],
                            'description'   => $this->input->post('description')[$count],
                            'unit_price'    => preg_replace("/[^0-9.]/", "", $this->input->post('unit_price')[$count]),
                            'unit'          => $this->input->post('unit')[$count],
                            'gst'           => preg_replace("/[^0-9.]/", "", $this->input->post('gst')[$count]),
                            'qty'           => $this->input->post('qty')[$count]
                        );
                    }
                        $count++;
                }

                $this->database_model->prototype_insert_bulk_db('prototype_quotations_product_lists',$quotation_products);
                redirect(base_url('Admin/quotation_lists'));
            }
        }
//    QUOTATION EDIT
    function quotation_edit($quotation_id = FALSE)
    {
        $select_quotation_id_arr = array(
            'table_name'    => 'prototype_quotations',
            'condition'     => array('autonum = '.$quotation_id)
        );
        $quotation_info = $this->database_model->db_select($select_quotation_id_arr)->row();
        if($quotation_id == FALSE){ redirect(base_url('Admin/quotation_lists')); }
        $data['csslinks']   = $this->csslinks(array('icheck'));
        $data['javalinks']  = $this->javalinks(array('icheck','icheck_function','currency_format_all','currency_format','currency_function','form_quotation'));
//        SUPPLIER DETAILS
        //      CONDITION
        $supplier_information_condition = array('condition' => array('autonum = '.$quotation_info->supplier_id));
        $arr_supplier_full_info = array(
            'suppliers_info'    => $supplier_information_condition
        );
        $supplier_details = $this->supplier_information($arr_supplier_full_info);
        $data['suppliers_info'] = $supplier_details['suppliers_info']->row();
        $arr_project_list = array(
            'table_name'    => 'prototype_projects',
            'condition'     => array('status = 0')
        );
        $data['project_lists'] = $this->database_model->db_select($arr_project_list)->result();
//        QUOTATION DETAILS
        $quotations_arr = array(
            'table_name'    => 'prototype_quotations',
            'condition'     => array('autonum = '.$quotation_id)
        );
        $data['quotation_info'] = $this->database_model->db_select($quotations_arr)->row();
        $quotations_arr_products = array(
            'table_name'    => 'prototype_quotations_product_lists',
            'condition'     => array('quotation_id = '.$quotation_id)
        );
        $data['quotation_info_products'] = $this->database_model->db_select($quotations_arr_products)->result();
//        QUOTATION SUB TASKS
        $arr_quotation_sub_task = array(
            'table_name' => 'prototype_projects_sub',
            'condition'  => array('autonum IN ('.$data['quotation_info']->project_sub_ids.')')
        );
        $data['sub_tasks'] = $this->database_model->db_select($arr_quotation_sub_task)->result();
        //       ACTIVE LINKS FOR TREE VIEW SIDEBAR
        $data['arr_treeview_active'] = $this->treeview_active_page(
            array(
            'quotation_page'     => 'active'
        ));
     
//        LOAD PAGE VIEW
        $arr_view = array(
            'header_view'   => $this->load->view('Dashboard/Header',$data,TRUE),
            'left_sidebar'  => $this->load->view('Dashboard/Left_sidebar',$data,TRUE),
            'main_content'  => $this->load->view('Admin_sections/form_edit_quotation','',TRUE),
            'footer_view'   => $this->load->view('Dashboard/Footer',$data,TRUE)
        );
        $this->display_page($arr_view);
    }
//    QUOTATION LISTS
    function quotation_lists()
    {
        $data['csslinks']   = $this->csslinks(array('datatables','fancybox'));
        $data['javalinks']  = $this->javalinks(array('datatables','datatables_bootstrap','datatables_function','datatables_addon_button','datatables_addon_print','datatables_addon_html5','datatables_addon_vfs','datatables_addon_pdf','datatables_addon_jzip','datatables_addon_flash','jquery_1_12_4','quotation_list_function','fancybox_function','fancybox','bootbox'));

        //       ACTIVE LINKS FOR TREE VIEW SIDEBAR
        $data['arr_treeview_active'] = $this->treeview_active_page(
            array(
            'quotation_page'     => 'active',
            'quotation_lists'    => 'active'
        ));
        // SHOW ALL SUPPLIERS
        $arrData = array(
            'table_name'    => 'prototype_quotations',
            'columns'       => array(
                '*',
                '(SELECT prototype_suppliers_info.company_name FROM prototype_suppliers_info WHERE prototype_suppliers_info.autonum = prototype_quotations.supplier_id) as company_name',
                'DATE_FORMAT((SELECT prototype_tender_invitation.date_submitted FROM prototype_tender_invitation WHERE prototype_tender_invitation.autonum = prototype_quotations.tender_invitation_id), "%M  %e, %Y") as date_submitted',
                'DATE_FORMAT((SELECT prototype_tender_invitation.expiration_date FROM prototype_tender_invitation WHERE prototype_tender_invitation.autonum = prototype_quotations.tender_invitation_id), "%M  %e, %Y") as expiration_date',
                '(SELECT prototype_quotations_submitted.autonum FROM prototype_quotations_submitted WHERE prototype_quotations_submitted.quotation_id = prototype_quotations.autonum) as quotation_number',
                '(SELECT prototype_projects_sub.project_name FROM prototype_projects_sub WHERE prototype_projects_sub.autonum = (SELECT prototype_projects_sub.project_id FROM prototype_projects_sub WHERE prototype_projects_sub.autonum IN (prototype_quotations.project_sub_ids))) as project_name',
                '(SELECT GROUP_CONCAT(prototype_projects_sub.project_name) FROM prototype_projects_sub WHERE FIND_IN_SET(prototype_projects_sub.autonum, prototype_quotations.project_sub_ids) > 0) as project_sub_tasks',
                '(SELECT prototype_quotations_status.status_name FROM prototype_quotations_status WHERE prototype_quotations_status.autonum = prototype_quotations.status) as quotation_status',
            ),
            'order_by'      => 'company_name ASC',
            'condition'     => array('status NOT IN (3)')
        );
        $data['quotation_lists'] = $this->database_model->db_select($arrData)->result();
        
//        LOAD PAGE VIEW
        $arr_view = array(
            'header_view'   => $this->load->view('Dashboard/Header',$data,TRUE),
            'left_sidebar'  => $this->load->view('Dashboard/Left_sidebar',$data,TRUE),
            'main_content'  => $this->load->view('Admin_sections/quotation_list','',TRUE),
            'footer_view'   => $this->load->view('Dashboard/Footer',$data,TRUE)
        );
        $this->display_page($arr_view);
    }
//    TENDER PROJECT TASK LISTS
    function tender_project_list()
    {
        $project_id = $_POST['project_id'];
        $arr_task_list = array(
            'table_name'    => 'prototype_projects_sub',
            'condition'     => array($project_id.' IN (project_id,autonum)') 
        );
        $data['sub_tasks'] = $this->database_model->db_select($arr_task_list)->result();
        
        $this->load->view('Admin_sections/task_lists',$data);
    }
//    QUOTATION LISTS
    function project_lists()
    {
        $data['csslinks']   = $this->csslinks(array('datatables','fancybox'));
        $data['javalinks']  = $this->javalinks(array('datatables','datatables_bootstrap','datatables_function','datatables_addon_button','datatables_addon_print','datatables_addon_html5','datatables_addon_vfs','datatables_addon_pdf','datatables_addon_jzip','datatables_addon_flash','jquery_1_12_4','project_list_function','fancybox_function','fancybox','bootbox'));

        //       ACTIVE LINKS FOR TREE VIEW SIDEBAR
        $data['arr_treeview_active'] = $this->treeview_active_page(
            array(
            'project_page'     => 'active',
            'project_lists'    => 'active'
        ));
//        PROJECT LISTS
        $arr_lists = array(
            'table_name'    => 'prototype_projects_sub',
            'condition'     => array('status !=3')
        );
        $data['project_lists'] = $this->database_model->db_select($arr_lists)->result();
//        LOAD PAGE VIEW
        $arr_view = array(
            'header_view'   => $this->load->view('Dashboard/Header',$data,TRUE),
            'left_sidebar'  => $this->load->view('Dashboard/Left_sidebar',$data,TRUE),
            'main_content'  => $this->load->view('Admin_sections/project_list','',TRUE),
            'footer_view'   => $this->load->view('Dashboard/Footer',$data,TRUE)
        );
        $this->display_page($arr_view);
    }
    
/*                                      FUNCTIONS                                             */
    function remove_from_lists()
    {
        $id = implode(',',$_POST['arr_id']);
        $table = $_POST['table'];
        $this->database_model->prototype_edit($table,array('status'=>3),array('autonum IN ('.$id.')'));
    }
//    SEND TENDER INVITATION
    function send_invitation()
    {
            $this->form_supplier_validation_lists(array('tender_invitation_modal','expiration_date','sub_tasks'));
//        IMPLODE SUPPLIERS ID
            $implode_supplier_id = implode(',',$this->input->post('supplier_id'));
            $project_sub_ids = $this->input->post('sub_tasks');
            
            $arr_select_supplier_data = array(
                'table_name'    => 'prototype_suppliers_info',
                'condition'     => array('MD5(MD5(MD5(autonum))) IN ('.$implode_supplier_id.')')
            );
        
            $suppliers = $this->database_model->db_select($arr_select_supplier_data)->result();
        
        
            $expiration_date = date('Y-m-d',strtotime($this->input->post('expiration_date')));
            $arr_email = array();
            $arr_quotaion_data = array();
            foreach($suppliers as $row){
                $arr_email['email_lists'][] = $row->email_address_sales;
                $arr_email['email_lists'][] = $row->email_address_accounts;
                $arr_data_tender_invitation = array(
                    'project_sub_ids' => implode(',',$project_sub_ids),
                    'supplier_id' => $row->autonum,
                    'expiration_date' => $expiration_date,
                    'email_address' => $row->email_address_sales.' / '.$row->email_address_accounts,
                    'user_id' => $this->session->autonum,
                    'date_submitted' => date('Y-m-d H:i:s')
                );
                $id = $this->database_model->prototype_insert_db('prototype_tender_invitation',$arr_data_tender_invitation);
                foreach($project_sub_ids as $row2){
                    $arr_quotaion_data[] = array(
                        'supplier_id'           => $row->autonum,
                        'project_sub_ids'       => $row2,
                        'tender_invitation_id'  => $id,
                        'status'                => 2,
                        'creted_by_id'          => $this->session->autonum
                    );
                }
            }
            $this->database_model->prototype_insert_bulk_db('prototype_quotations',$arr_quotaion_data);
            $this->send_mailtype($arr_email);
            redirect(base_url($this->input->post('after_submit')));
            
     }
//    SEND TENDER INVITATION
    function send_user_account()
    {
            foreach($suppliers as $row){
                $arr_email['email_lists'][] = $row->email_address_sales;
                $arr_email['email_lists'][] = $row->email_address_accounts;
                $arr_data_tender_invitation = array(
                    'project_sub_ids' => $project_sub_ids,
                    'supplier_id' => $row->autonum,
                    'expiration_date' => $expiration_date,
                    'email_address' => $row->email_address_sales.' / '.$row->email_address_accounts,
                    'user_id' => $this->session->autonum,
                    'date_submitted' => date('Y-m-d H:i:s')
                );
                $id = $this->database_model->prototype_insert_db('prototype_tender_invitation',$arr_data_tender_invitation);
                
                $arr_quotaion_data[] = array(
                    'supplier_id'           => $row->autonum,
                    'project_sub_ids'       => $project_sub_ids,
                    'tender_invitation_id'  => $id,
                    'status'                => 2,
                    'creted_by_id'          => $this->session->autonum
                );
            }
            $this->database_model->prototype_insert_bulk_db('prototype_quotations',$arr_quotaion_data);
            $this->send_mailtype($arr_email);
            redirect(base_url($this->input->post('after_submit')));
            
}
// VIEW QUOTE
    function view_quote(){
        $quotation_id = $_POST['quotation_id'];
        $arr_data = array(
            'table_name'    => 'prototype_quotations',
            'condition'     => array('autonum = '.$quotation_id),
            'columns'       => array('*',
                                    '(SELECT prototype_suppliers_info.company_name FROM prototype_suppliers_info WHERE prototype_suppliers_info.autonum = prototype_quotations.supplier_id) as company_name',
                'DATE_FORMAT((SELECT prototype_quotations_submitted.submitted_date FROM prototype_quotations_submitted WHERE prototype_quotations_submitted.quotation_id = prototype_quotations.autonum), "%M  %e, %Y") as date_submitted',
                'DATE_FORMAT((SELECT prototype_tender_invitation.expiration_date FROM prototype_tender_invitation WHERE prototype_tender_invitation.autonum = prototype_quotations.tender_invitation_id), "%M  %e, %Y") as expiration_date',
                '(SELECT prototype_quotations_submitted.autonum FROM prototype_quotations_submitted WHERE prototype_quotations_submitted.quotation_id = prototype_quotations.autonum) as quotation_number',
                '(SELECT prototype_projects.project_name FROM prototype_projects WHERE prototype_projects.autonum = prototype_quotations.project_id) as project_name',
                                    )
        );
        $data['quotation_info'] = $this->database_model->db_select($arr_data)->row();
        $sub_task_arr = array(
            'table_name'    => 'prototype_projects_sub',
            'condition'     => array('autonum IN ('.$data['quotation_info']->project_sub_ids.')')
        );
        $data['sub_tasks'] = $this->database_model->db_select($sub_task_arr)->result();
        $arr_data_product_lists = array(
            'table_name'    => 'prototype_quotations_product_lists',
            'condition'     => array('quotation_id = '.$quotation_id)
        );
        $data['quotation_product_lists'] = $this->database_model->db_select($arr_data_product_lists)->result();
        $this->load->view('Admin_sections/quotation_view',$data);
    }
}

