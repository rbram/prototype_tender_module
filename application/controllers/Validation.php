<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Validation extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('Validation_model','validation_model');
    }
    
    public function form_supplier()
	{
        // basic required field
        $this->load->library('form_validation');
        $this->form_validation->set_rules('company_name', '', 'required');
//        $this->form_validation->set_rules('abn_acn', '', 'required');
//        $this->form_validation->set_rules('street_address', '', 'required');
//        $this->form_validation->set_rules('postal_address', '', 'required');
//        $this->form_validation->set_rules('phone_number_sales', '', 'required');
//        $this->form_validation->set_rules('phone_number_accounts', '', 'required');
//        $this->form_validation->set_rules('email_address_sales', '', 'required');
//        $this->form_validation->set_rules('email_address_accounts', '', 'required');
//        $this->form_validation->set_rules('website', '', 'required');
//        $this->form_validation->set_rules('account_manager_name', '', 'required');
//        $this->form_validation->set_rules('years_in_operation', '', 'required');
//        $this->form_validation->set_rules('number_of_employees', '', 'required');
//        $this->form_validation->set_rules('category_of_services', '', 'required');
//        $this->form_validation->set_rules('category_of_services_others', '', 'required');
//        $this->form_validation->set_rules('details_of_service', '', 'required');
//        $this->form_validation->set_rules('expertise_certification', '', 'required');
//        $this->form_validation->set_rules('delivery_stat', '', 'required');
//        $this->form_validation->set_rules('delivery_stat_known', '', 'required');
//        $this->form_validation->set_rules('standard_quote_turnaround_time', '', 'required');
//        $this->form_validation->set_rules('standard_manufacturing_supply_lead_time', '', 'required');
//        $this->form_validation->set_rules('specific_of_warranty_offered_on_goods_services', '', 'required');
//        $this->form_validation->set_rules('quality_assusrance_procedure', '', 'required');
//        $this->form_validation->set_rules('details_of_qa_procedure_policy', '', 'required');
//        $this->form_validation->set_rules('details_of_qa_procedure_policy_attachments', '', 'required');
//        $this->form_validation->set_rules('public_liability_insurer', '', 'required');
//        $this->form_validation->set_rules('public_liability_number', '', 'required');
//        $this->form_validation->set_rules('public_liability_indemnity', '', 'required');
//        $this->form_validation->set_rules('public_liability_excess', '', 'required');
//        $this->form_validation->set_rules('public_liability_period_cover', '', 'required');
//        $this->form_validation->set_rules('professional_indemnity_insurer', '', 'required');
//        $this->form_validation->set_rules('professional_indemnity_number', '', 'required');
//        $this->form_validation->set_rules('professional_indemnity_indemnity', '', 'required');
//        $this->form_validation->set_rules('professional_indemnity_excess', '', 'required');
//        $this->form_validation->set_rules('professional_indemnity_period_cover', '', 'required');
//        $this->form_validation->set_rules('workers_compensation_insurer', '', 'required');
//        $this->form_validation->set_rules('workers_compensation_number', '', 'required');
//        $this->form_validation->set_rules('workers_compensation_indemnity', '', 'required');
//        $this->form_validation->set_rules('workers_compensation_excess', '', 'required');
//        $this->form_validation->set_rules('workers_compensation_period_cover', '', 'required');
//        $this->form_validation->set_rules('marine_transit_insurer', '', 'required');
//        $this->form_validation->set_rules('professional_indemnity_period_cover', '', 'required');
//        $this->form_validation->set_rules('marine_transit_number', '', 'required');
//        $this->form_validation->set_rules('marine_transit_indemnity', '', 'required');
//        $this->form_validation->set_rules('marine_transit_excess', '', 'required');
//        $this->form_validation->set_rules('marine_transit_period_cover', '', 'required');
//        $this->form_validation->set_rules('payment_terms', '', 'required');
//        $this->form_validation->set_rules('payment_terms_others', '', 'required');
//        $this->form_validation->set_rules('credit_cards_charges', '', 'required');
//        $this->form_validation->set_rules('credit_cards_charges_others', '', 'required');
//        $this->form_validation->set_rules('bank_account_details_bsb', '', 'required');
//        $this->form_validation->set_rules('bank_account_details_acct', '', 'required');
//        $this->form_validation->set_rules('bank_account_details_financial_institution', '', 'required');
//        $this->form_validation->set_rules('public_liability_attachments', '', 'required');
//        $this->form_validation->set_rules('professional_indemnity_attachments', '', 'required');
//        $this->form_validation->set_rules('workers_compensation_attachments', '', 'required');
//        $this->form_validation->set_rules('marine_transit_attachments', '', 'required');
//        $this->form_validation->set_rules('status', '', 'required');
        if ($this->form_validation->run() == FALSE){
            $this->load->view('Admin/form_new_supplier');
        }
        else {
            var_dump($data);
         $this->Validation_model->save($data);           
        }
	}
}
