<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('Login_model','login_model');
    }
    
    public function index()
	{
        $this->login();
	}
    public function login()
	{
        $data['title'] = "Log in form";
		$this->load->view('Login_view' , $data);
	}
    
//    LOG IN VALIDATION
    function login_validation(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username','Username', 'required');
        $this->form_validation->set_rules('password','Password', 'required');
        if($this->form_validation->run()){
            //TRUE
            $username = $this->input->post('username');
            $password = md5(sha1($this->input->post('password')));
//            $password = $this->input->post('password');
            $user_account = $this->login_model->login_authentication($username,$password);
            if($user_account)
            {
                $user_account = $user_account->row();
                $session_data = array(
                    'autonum'               => $user_account->autonum,
                    'username'              => $user_account->user_name,
                    'fname'                 => $user_account->first_name,
                    'mname'                 => $user_account->middle_name,
                    'lname'                 => $user_account->last_name,
                    'sname'                 => $user_account->suffix_name,
                    'tname'                 => $user_account->title_name,
                    'gender'                => $user_account->gender,
                    'position'              => $user_account->position,
                    'supplier_company_id'   => $user_account->supplier_company_id,
                    'supplier_company_name' => $user_account->supplier_company_name
                );
                $this->session->set_userdata($session_data);
                redirect(base_url('Admin/dashboard'));
            }
            else
            {
                $this->session->set_flashdata('error','Invalid Username and Password');
                redirect(base_url());
            }
        }
        else{
            //FALSE
            $this->login();
        }
    }
//    LOG OUT
    function logout(){
        $this->session->unset_userdata('username');
        redirect(base_url());
    }
}
