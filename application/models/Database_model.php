<?php
class Database_model extends CI_Model{  
    function __construct()
    {
        parent::__construct();
    }
    
    function prototype_insert_db($table=FALSE,$data=FALSE)
    {
        $this->db->insert($table, $data); 
        $id = $this->db->insert_id();
        return $id;
    }
    
    function prototype_insert_bulk_db($table=FALSE,$data=FALSE)
    {
        $this->db->insert_batch($table, $data); 
    }
    
    function prototype_insert_db_manual($table,$arrData)
    {
        $arr_columns = array();
        foreach($arrData as $key=> $value)
        {
            $arr_columns[] = $key;
        }
        $columns = implode(',',$arr_columns);
        $values = implode(',',$arrData);
        $query = $this->db->query('INSERT INTO '.$table.' ('.$columns.') VALUES ('.$values.')');
        return $query;
    }
    
    function prototype_edit($table=FALSE,$data=FALSE,$condition=FALSE)
    {
        $condition = ($condition !== FALSE) ? implode(' AND ',$condition):'';
        $this->db->update($table, $data, $condition);
    }
    
    function db_select($arrData)
    {
        $table      = $arrData['table_name'];
        $columns    = (isset($arrData['columns']))      ? implode(',',$arrData['columns']) : '*';
        $condition  = (isset($arrData['condition']))    ? 'WHERE '.implode(' AND ',$arrData['condition']) : '';
        $query = $this->db->query('SELECT '.$columns.' FROM '.$table.' '.$condition.'');
        return $query;
    }
    
    function prototype_delete($arrData){
        $table      = $arrData['table_name'];
        $condition  = (isset($arrData['condition']))    ? 'WHERE '.implode('AND ',$arrData['condition']) : '';
        $query = $this->db->query('DELETE FROM '.$table.' '.$condition.'');
        return $query;
    }
    
}



