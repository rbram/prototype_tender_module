<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Prototype Tender| Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/font-awesome/css/font-awesome.min.css')?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bower_components/Ionicons/css/ionicons.min.css')?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/custom-styles.css')?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/iCheck/square/blue.css')?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
 
<div class="wrapper clearfix">
          <section class="login-form-container">
            <h1 class="login-form-container__login-logo">Pro z</h1>
            <form action="<?php echo base_url('Login/login_validation') ?>" method="post">
              <div class="bg-red login-form-container__login-msg login-form-container__login-msg--error-msg">
                <span>Username and Password are Required!</span>
              </div>
              <div class="form-group">
                  <div class="form-group">
                    <label for="exampleInputEmail1">User Name</label>
                    <input type="text" class="form-control" name="username">
                    <span class="text-danger"><?php echo form_error('username'); ?></span>
                  </div>

                  <div class="form-group">
                      <label for="exampleInputPassword1">Password</label>
                      <input type="password" class="form-control" name="password">
                      <span class="text-danger"><?php echo form_error('password'); ?></span>
                  </div>

                  <div class="login-form-container__login-links">
                    <a href="#" class="login-form-container__login-forgot">Cant Login?</a>
                    <button type="submit" class="login-form-container__login-btn btn " name="insert">Log in</button>
                  </div>
                  <span class="text-danger"><?php echo $this->session->flashdata('error'); ?></span>
                  <div class="login-form-container__login-divider">
                      <span class="login-form-container__login-divider-message">or</span>
                  </div>

                  <button class="btn btn-block login-form-container__signup-btn">
                    Sign-up
                  </button>
                  
              </div><!-- /.form-group -->
            </form>
          </section><!-- /.login-form-container-->
          <section class="login-info-container"></section>
      </div><!-- /.wrapper -->
<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/bower_components/jquery/dist/jquery.min.js')?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<!-- iCheck -->
<script src="<?php echo base_url('assets/plugins/iCheck/icheck.min.js')?>"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>
