<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
     
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="<?php echo $this->session->supplier_company_name ?>" disabled>
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat" disabled><i class="fa fa-align-center"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview <?php echo $arr_treeview_active['dashboard'] ?>">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php echo $arr_treeview_active['dashboard_dashboard_1'] ?>"><a href="<?php echo base_url('Admin/'); ?>"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul>
        </li>
        <li class="treeview <?php echo $arr_treeview_active['user_accounts'] ?>">
          <a href="#">
            <i class="fa fa-users"></i> <span>Accounts</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php echo $arr_treeview_active['user_accounts_lists'] ?>"><a href="<?php echo base_url('Admin/user_lists'); ?>"><i class="fa fa-circle-o"></i> Lists</a></li>
            <li class="<?php echo $arr_treeview_active['user_accounts_access'] ?>"><a href="<?php echo base_url('Admin/user_access'); ?>"><i class="fa fa-circle-o"></i> Access</a></li>
          </ul>
        </li>
        <li class="treeview <?php echo $arr_treeview_active['supplier_page'] ?>">
          <a href="#">
            <i class="fa fa-drivers-license"></i> <span>Suppliers</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php echo $arr_treeview_active['supplier_lists'] ?>"><a href="<?php echo base_url('Admin/forms_supplier_application'); ?>"><i class="fa fa-circle-o"></i>Lists</a></li>
            <li class="<?php echo $arr_treeview_active['form_new_supplier'] ?>"><a href="<?php echo base_url('Admin/form_new_supplier'); ?>"><i class="fa fa-circle-o"></i> Add&nbsp;New</a></li>
          </ul>
        </li>
        <li class="treeview <?php echo $arr_treeview_active['quotation_page'] ?>">
          <a href="#">
            <i class="fa fa-list-ol"></i> <span>Quotations</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php echo $arr_treeview_active['quotation_lists'] ?>"><a href="<?php echo base_url('Admin/quotation_lists'); ?>"><i class="fa fa-circle-o"></i>Lists</a></li>
            <li class="<?php echo $arr_treeview_active['form_new_quotation'] ?>"><a href="<?php echo base_url('Admin/form_new_quotation'); ?>"><i class="fa fa-circle-o"></i> Add&nbsp;New</a></li>
          </ul>
        </li>
          
        <li class="treeview <?php echo $arr_treeview_active['project_page'] ?>">
          <a href="#">
            <i class="fa fa-th"></i> <span>Projects</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php echo $arr_treeview_active['projects_lists'] ?>"><a href="<?php echo base_url('Admin/project_lists'); ?>"><i class="fa fa-circle-o"></i>Lists</a></li>
            <li class="<?php echo $arr_treeview_active['form_new_project'] ?>"><a href="<?php echo base_url('Admin/form_new_project'); ?>"><i class="fa fa-circle-o"></i> Add&nbsp;New</a></li>
          </ul>
        </li>
        
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

<div class="content-wrapper">