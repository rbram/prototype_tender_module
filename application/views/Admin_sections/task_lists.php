<hr/>
<div class="row">
        <div class="col-md-12">
            <label>Task Lists:</label>
            <span class="text-danger hidden-text" id="notification_task_lists">Please select atleast one project. <?php echo form_error('sub_tasks'); ?></span>
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <?php 
                $count = 0;
                $arr_task_data = array();
                foreach($sub_tasks as $row) 
                {
                    $active = ($count == 0) ? 'active':'';
                    $arr_task_data[] = '<div class="tab-pane '.$active.'" id="task_'.$row->autonum.'">'
                                        .'<label>Task Overview:</label><br/>'
                                        .$count.'task overview<br/>'
                                        .'<label>Task Details & Requirements:</label><br/>'
                                        .'task details and requrements<br/>'
                                        .'<label>Freight & Delivery Instructions:</label><br/>'
                                        .'delivery instructions<br/>'
                                        .'<label>Other Information important to the quote:</label><br/>'
                                        .'other Informations<br/>'
                                        .'</div>';
                ?>
                    <li class="<?php echo $active; ?>"><a href="#task_<?php echo $row->autonum ?>" data-toggle="tab" aria-expanded="<?php echo ($count == 0) ? 'true':'false' ?>" style="text-transform:capitalize;"><input type="checkbox" style="vertical-align:center;" class="tender_sub_task_checkbox" name="sub_tasks[]" value="<?php echo $row->autonum; ?>">&nbsp;&nbsp;<?php echo $row->project_name; ?></a></li>
                <?php
                    $count++;
                } 
                ?>
            </ul>
            <div class="tab-content">
              <?php foreach($arr_task_data as $row){ echo $row; } ?>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->

        <!-- /.col -->
      </div>