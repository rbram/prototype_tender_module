
<form action="<?php echo base_url('Admin/form_supplier_validation') ?>" method="post" enctype="multipart/form-data">

<div class="content">
    <div class="form_error">
      <?php echo validation_errors(); ?>
    </div>
    <section class="content-header">
        <h1>&nbsp;</h1>  
        <ol class="breadcrumb">
            <li><a href="#">New&nbsp;Supplier&nbsp;Application&nbsp;Form</a></li>
        </ol>
    </section>
    
    <div class="box box-primary">
        <div class="box-header with-border">
          <h1 class="box-title">NEW&nbsp;SUPPLIER&nbsp;APPLICATION&nbsp;FROM</h1>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
              <div class="col-sm-12">
                  <form role="form">
                    <h3 class="text-center">Company&nbsp;Details</h3><hr/>
                  <div class="box-body">
                      <div class="form-group">
                          <label class="">Full&nbsp;Company&nbsp;Name:</label>
                          <input type="text" name="company_name" class="form-control" id="" placeholder="..." value="<?php echo set_value('company_name'); ?>" name="company_name">
                          <span class="text-danger"><?php echo form_error('company_name'); ?></span>
                        </div>
                        <div class="form-group">
                            <label for="" class="">ABN&nbsp;and/or&nbsp;ACN:</label>
                            <input type="text" class="form-control" id="" placeholder="..." value="<?php echo set_value('abn_acn'); ?>" name="abn_acn">
                            <span class="text-danger"><?php echo form_error('abn_acn'); ?></span>
                        </div>
                        <div class="form-group">
                            <label for="" class="">Street&nbsp;Address:</label>
                            <input type="text" class="form-control" id="" placeholder="..." value="<?php echo set_value('street_address'); ?>" name="street_address">
                            <span class="text-danger"><?php echo form_error('street_address'); ?></span>
                        </div>
                        <div class="form-group">
                            <label for="" class="">Postal&nbsp;Address:</label><small><br/>*If&nbsp;different&nbsp;to&nbsp;street&nbsp;address</small>
                            <input type="text" class="form-control" id="" placeholder="..." value="<?php echo set_value('postal_address'); ?>" name="postal_address">
                            <span class="text-danger"><?php echo form_error('postal_address'); ?></span>
                        </div>
                        <div class="form-group">
                            <label for="" class="">Phone&nbsp;Numbers:</label>
                                <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="For Sales/Orders:" value="<?php echo set_value('phone_number_sales'); ?>" name="phone_number_sales">
                                    <span class="text-danger"><?php echo form_error('phone_number_sales'); ?></span>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="For Accounts" value="<?php echo set_value('phone_number_accounts'); ?>" name="phone_number_accounts">
                                    <span class="text-danger"><?php echo form_error('phone_number_accounts'); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="">Email&nbsp;Addresses:</label>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="For Sales/Orders:" name="email_address_sales">
                                    <span class="text-danger"><?php echo form_error('email_address_sales'); ?></span>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="For Accounts" name="email_address_accounts">
                                    <span class="text-danger"><?php echo form_error('email_address_accounts'); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="">Website:</label>
                            <input type="text" class="form-control" id="" placeholder="..." name="website">
                            <span class="text-danger"><?php echo form_error('website'); ?></span>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="">Account&nbsp;Manager&nbsp;Name:</label>
                            <input type="text" class="form-control" id="" placeholder="..." name="account_manager_name">
                            <span class="text-danger"><?php echo form_error('account_manager_name'); ?></span>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="">Years&nbsp;in&nbsp;Operation:</label>
                            <input type="text" class="form-control" id="" placeholder="..." name="years_in_operation">
                            <span class="text-danger"><?php echo form_error('years_in_operation'); ?></span>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="">Number&nbsp;of&nbsp;Employees:</label>
                            <input type="text" class="form-control" id="" placeholder="..." name="number_of_employees">
                            <span class="text-danger"><?php echo form_error('number_of_employees'); ?></span>
                        </div>
                      <hr/>
                      <h3 class="text-center">Supplier&nbsp;Services</h3>
                      <div class="col-sm-6 col-xs-12 no-gutter">
                      <div class="box box-primary">
                        <div class="box-header">
                          <h3 class="box-title">Category&nbsp;of&nbsp;services:<br/><small>*please tick all service offering that apply</small></h3>
                        </div>
                        <div class="box-body">
                            <div class="col-sm-6 col-xs-12">
                                <ul class="nav nav-stacked">
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" value="Timber / Joinert">&nbsp;&nbsp;Timber&nbsp;/&nbsp;Joinery</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" value="Powder / Coating">&nbsp;&nbsp;Poweder&nbsp;/&nbsp;Coating</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" value="Upholstery and Framing">&nbsp;&nbsp;Upholstery&nbsp;and&nbsp;Framing</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" value="Wholesaler">&nbsp;&nbsp;Wholesaler</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" value="Shop fitting">&nbsp;&nbsp;Shop&nbsp;fitting</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" value="Tiler">&nbsp;&nbsp;Tiler</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" value="Electroplating">&nbsp;&nbsp;Electroplating</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" value="CAD">&nbsp;&nbsp;CAD</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" value="Compact Laminate">&nbsp;&nbsp;Compact&nbsp;Laminate</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" value="Labour">&nbsp;&nbsp;Labour</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" value="Freight">&nbsp;&nbsp;Freight</li>
                                </ul>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <ul class="nav nav-stacked">
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" value="Metal Fabrication">&nbsp;&nbsp;Metal&nbsp;Fabrication</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" value="Upholstery">&nbsp;&nbsp;Upholstery</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" value="Furniture">&nbsp;&nbsp;Furniture</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" value="Painting Staining and 2pac">&nbsp;&nbsp;Painting&nbsp;Staining&nbsp;and&nbsp;2pac</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" value="Stone Mason">&nbsp;&nbsp;Stone&nbsp;Mason</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" value="Fabric Supplier">&nbsp;&nbsp;Fabric&nbsp;Supplier</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" value="Electrical">&nbsp;&nbsp;Electrical</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" value="Water proofing">&nbsp;&nbsp;Waterproofing</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" value="Installation and Rigging">&nbsp;&nbsp;Installation&nbsp;and&nbsp;Rigging</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" value="Warehousing Storage">&nbsp;&nbsp;Warehousing&nbsp;Storage</li>
                                    <li><input type="text" class="form-control" placeholder="Others please specify." name="category_of_services_others" /></li>
                                </ul>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                        </div>
                      </div>
                      </div>
                      <div class="col-sm-6 col-xs-12 no-gutter-right">
                            <div class="form-group">
                                <label for="" class="">Detail&nbsp;of&nbsp;Service:</label>
                                <br/>
                                <small>*Provide&nbsp;any&nbsp;further&nbsp;details&nbsp;regarding&nbsp;materials&nbsp;or&nbsp;services&nbsp;offered.</small>
                                <textarea class="form-control" id="" placeholder="..." name="details_of_service"></textarea>
                                <span class="text-danger"><?php echo form_error('details_of_service'); ?></span>
                            </div>
                          <div class="form-group">
                                <label for="inputPassword3" class="">Expertise&nbsp;/&nbsp;Certification:</label>
                                <textarea class="form-control" id="" placeholder="..." name="expertise_certification"></textarea>
                              <span class="text-danger"><?php echo form_error('expertise_certification'); ?></span>
                            </div>
                          <div class="form-group">
                                <label for="inputPassword3" class="">Delivery</label>
                                <br/>
                                <ul class="nav nav-stacked">
                                    <li><input type="checkbox" class="flat-red" name="delivery_stat" value="1"><label>&nbsp;&nbsp;Yes</label></li>
                                    <li><input type="text" class="form-control" placeholder="Please specify. if known" name="delivery_stat_known" /></li>
                                    <span class="text-danger"><?php echo form_error('delivery_stat_known'); ?></span>
                                </ul>
                            </div>
                          <div class="form-group">
                                <label for="inputPassword3" class="">Standard&nbsp;Quote&nbsp;turnaround/&nbsp;time:</label>
                                <input class="form-control" id="" placeholder="..." name="standard_quote_turnaround_time"/>
                              <span class="text-danger"><?php echo form_error('standard_quote_turnaround_time'); ?></span>
                            </div>
                          <div class="form-group">
                                <label for="inputPassword3" class="">Standard&nbsp;Manufacturing/&nbsp;supply&nbsp;lead&nbsp;time:</label>
                                <input class="form-control" id=""  placeholder="..." name="standard_manufacturing_supply_lead_time"/>
                              <span class="text-danger"><?php echo form_error('standard_manufacturing_supply_lead_time'); ?></span>
                            </div>
                      </div>
                      <br/>
                        <h3 class="text-center">Quality&nbsp;Assurance&nbsp;/&nbsp;Warranty</h3>
                      <div class="col-sm-12  no-gutter">
                            <div class="form-group">
                                <label for="" class="">Specifics&nbsp;of&nbsp;warranty&nbsp;offered&nbsp;on good/services:</label>
                                <br/>
                                <small>*Length of time and other particulars. Attach copy of any warranty document.</small>
                                <textarea class="form-control" id="" placeholder="..." name="specific_of_warranty_offered_on_goods_services"></textarea>
                                <span class="text-danger"><?php echo form_error('specific_of_warranty_offered_on_goods_services'); ?></span>
                            </div>
                            <div class="form-group">
                              <label for="inputPassword3" class="">Quality&nbsp;Assurance&nbsp;procedure:</label>
                                <div class="col-sm-12">
                                    <input type="radio" class="flat-red quality_assurance_radio_btn" name="quality_assurance_procedure" value="yes">&nbsp;&nbsp;Yes&nbsp;&nbsp;
                                    <input type="radio" class="flat-red quality_assurance_radio_btn" name="quality_assurance_procedure" value="no">&nbsp;&nbsp;No
                                </div>
                            </div>
                      </div>
                      <div class="col-sm-12" style="margin-top:15px;">
                          <div class="box box-primary hidden-box" id="box_category_of_services">
                            <div class="box-header">
                              
                            </div>
                            <div class="box-body">
                                <label for="inputPassword3" class="">Details&nbsp;of&nbsp;QA&nbsp;procedure&nbsp;/&nbsp;policy:</label>
                                <br/>
                                <small>Outline&nbsp;details&nbsp;of&nbsp;QA&nbsp;procedure.&nbsp;Annex&nbsp;a&nbsp;page&nbsp;if insufficient space. Attach a copy of any procedure document.</small>
                                <div class="col-sm-12">
                                    <textarea class="form-control" id="" placeholder="..." name="details_of_qa_procedure_policy"></textarea>
                                    <span class="text-danger"><?php echo form_error('details_of_qa_procedure_policy'); ?></span>
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputFile">Attach&nbsp;file:</label>
                                  <input type="file" id="exampleInputFile" name="details_of_qa_procedure_policy_attachments">
                                    <span class="text-danger"><?php echo form_error('standard_manufacturing_supply_lead_time'); ?></span>
                                </div>
                            </div>
                            <!-- /.box-body -->
    <!--
                            <div class="box-footer">

                            </div>
    -->
                            </div>
                        <h3 class="text-center">Insurance</h3>
                        <div class="form-group">
                            <label for="" class="">Public Liability:</label>
                            <br/>
                            <small>*Please Attach Certificate of Currency</small>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="Name of Insurer:" name="public_liability_insurer">
                                    <span class="text-danger"><?php echo form_error('public_liability_insurer'); ?></span>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="Policy Number:" name="public_liability_number">
                                    <span class="text-danger"><?php echo form_error('public_liability_number'); ?></span>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="Limit of Idemnity:" name="public_liability_indemnity">
                                    <span class="text-danger"><?php echo form_error('public_liability_indemnity'); ?></span>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="Excess:" name="public_liability_excess">
                                    <span class="text-danger"><?php echo form_error('public_liability_excess'); ?></span>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="Period of Cover:" name="public_liability_period_cover">
                                    <span class="text-danger"><?php echo form_error('public_liability_period_cover'); ?></span>  
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                      <label for="exampleInputFile">Attach&nbsp;file:</label>
                                      <input type="file" id="exampleInputFile" name="public_liability_attachment">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="">Professional Indemnity:</label>
                            <br/>
                            <small>*Please Attach Certificate of Currency</small>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="Name of Insurer:" name="professional_indemnity_insurer">
                                    <span class="text-danger"><?php echo form_error('professional_indemnity_insurer'); ?></span>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="Policy Number:" name="professional_indemnity_number">
                                    <span class="text-danger"><?php echo form_error('professional_indemnity_number'); ?></span>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="Limit of Idemnity:" name="professional_indemnity_indemnity">
                                    <span class="text-danger"><?php echo form_error('professional_indemnity_indemnity'); ?></span>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="Excess:" name="professional_indemnity_excess">
                                    <span class="text-danger"><?php echo form_error('professional_indemnity_excess'); ?></span>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="Period of Cover:" name="professional_indemnity_period_cover">
                                    <span class="text-danger"><?php echo form_error('professional_indemnity_period_cover'); ?></span>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                      <label for="exampleInputFile">Attach&nbsp;file:</label>
                                      <input type="file" id="exampleInputFile" name="professional_indemnity_attachment">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="">Worker's Compensation:</label>
                            <br/>
                            <small>*Please Attach Certificate of Currency</small>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="Name of Insurer:" name="workers_compensation_insurer">
                                    <span class="text-danger"><?php echo form_error('workers_compensation_insurer'); ?></span>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="Policy Number:" name="workers_compensation_number">
                                    <span class="text-danger"><?php echo form_error('workers_compensation_number'); ?></span>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="Limit of Idemnity:" name="workers_compensation_indemnity">
                                    <span class="text-danger"><?php echo form_error('workers_compensation_indemnity'); ?></span>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="Excess:" name="workers_compensation_excess">
                                    <span class="text-danger"><?php echo form_error('workers_compensation_excess'); ?></span>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="Period of Cover:" name="workers_compensation_period_cover">
                                    <span class="text-danger"><?php echo form_error('workers_compensation_period_cover'); ?></span>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                      <label for="exampleInputFile">Attach&nbsp;file:</label>
                                      <input type="file" id="exampleInputFile" name="workers_compensation_attachment">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="">Marine Transit:</label>
                            <br/>
                            <small>*Please Attach Certificate of Currency</small>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="Name of Insurer:" name="marine_transit_insurer">
                                    <span class="text-danger"><?php echo form_error('marine_transit_insurer'); ?></span>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="Policy Number:" name="marine_transit_number">
                                    <span class="text-danger"><?php echo form_error('marine_transit_number'); ?></span>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="Limit of Idemnity:" name="marine_transit_indemnity">
                                    <span class="text-danger"><?php echo form_error('marine_transit_indemnity'); ?></span>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="Excess:" name="marine_transit_excess">
                                    <span class="text-danger"><?php echo form_error('marine_transit_excess'); ?></span>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="Period of Cover:" name="marine_transit_period_cover">
                                    <span class="text-danger"><?php echo form_error('marine_transit_period_cover'); ?></span>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                      <label for="exampleInputFile">Attach&nbsp;file:</label>
                                      <input type="file" id="exampleInputFile" name="marine_transit_attachment">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h3 class="text-center">Trading&nbsp;Terms</h3>
                        <div class="col-sm-12">
                            <div class="col-sm-6 col-xs-12">
                                <label>Payment Terms:</label>
                                <div class="col-sm-12">
                                    <ul class="nav nav-stacked">
                                        <li><input type="checkbox" class="flat-red" value="30 days" name="payment_terms[]"><label>&nbsp;&nbsp;30 days<small>(End of Month)</small></label></li>
                                        <li><input type="checkbox" class="flat-red" value="60 days" name="payment_terms[]"><label>&nbsp;&nbsp;60 days<small>(End of Month)</small></label></li>
                                        <li><input type="checkbox" class="flat-red" value="90 days" name="payment_terms[]"><label>&nbsp;&nbsp;90 days<small>(End of Month)</small></label></li>
                                        <li><input class="form-control" placeholder="Other..." name="payment_terms_others"></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <label>Credit Cards and Charges:</label>
                                <div class="col-sm-12">
                                    <ul class="nav nav-stacked">
                                        <li><input type="checkbox" class="flat-red" name="credit_cards_charges[]" value="VISA"><label>&nbsp;&nbsp;VISA</label></li>
                                        <li><input type="checkbox" class="flat-red" name="credit_cards_charges[]" value="Amex"><label>&nbsp;&nbsp;Amex</label></li>
                                        <li><input type="checkbox" class="flat-red" name="credit_cards_charges[]" value="Mastercard"><label>&nbsp;&nbsp;Mastercard</label></li>
                                        <li><input class="form-control" placeholder="Charges..." name="credit_cards_charges_others"></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                            <div class="form-group">
                                <label for="" class="">Bank Account Details:</label>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" id="" placeholder="BSB:" name="bank_account_details_bsb">
                                        <span class="text-danger"><?php echo form_error('bank_account_details_bsb'); ?></span>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" id="" placeholder="Acct:" name="bank_account_details_acct">
                                        <span class="text-danger"><?php echo form_error('bank_account_details_acct'); ?></span>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="" placeholder="Financial Institution:" name="bank_account_details_financial_institution">
                                        <span class="text-danger"><?php echo form_error('bank_account_details_financial_institution'); ?></span>
                                    </div>
                                </div>
                            </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Accounting System / Software</label>(<small>e.g xero</small>):
                                <input type="text" class="form-control" id="" placeholder="Financial Institution:" name="accounting_system_software">
                                <span class="text-danger"><?php echo form_error('accounting_system_software'); ?></span>
                            </div>
                        </div>
                          <div class="col-sm-12">
                            <h3 class="text-center">Trade References</h3>
                              <center>
                              <small>*Provide 3 referees that will provide a reference regarding quality and service</small>
                              </center>
                              <br/>
                              <div class="col-sm-4 col-xs-12">
                                <label>Referee 1</label>
                                <input type="text" class="form-control" id="" placeholder="Company:" name="referee_one_company"><br/>
                                <span class="text-danger"><?php echo form_error('referee_one_company'); ?></span>
                                <input type="text" class="form-control" id="" placeholder="Contact Name:" name="referee_one_contact_name"><br/>
                                <span class="text-danger"><?php echo form_error('referee_one_contact_name'); ?></span>
                                <input type="text" class="form-control" id="" placeholder="Phone:" name="referee_one_phone"><br/>
                                <span class="text-danger"><?php echo form_error('referee_one_phone'); ?></span>
                                <input type="text" class="form-control" id="" placeholder="Email:" name="referee_one_email"><br/>
                                <span class="text-danger"><?php echo form_error('referee_one_email'); ?></span>
                              </div>
                              <div class="col-sm-4 col-xs-12">
                                <label>Referee 2</label>
                                <input type="text" class="form-control" id="" placeholder="Company:" name="referee_two_company"><br/>
                                <span class="text-danger"><?php echo form_error('referee_two_company'); ?></span>
                                <input type="text" class="form-control" id="" placeholder="Contact Name:" name="referee_two_contact_name"><br/>
                                <span class="text-danger"><?php echo form_error('referee_two_contact_name'); ?></span>
                                <input type="text" class="form-control" id="" placeholder="Phone:" name="referee_two_phone"><br/>
                                <span class="text-danger"><?php echo form_error('referee_two_phone'); ?></span>
                                <input type="text" class="form-control" id="" placeholder="Email:" name="referee_two_email"><br/>
                                <span class="text-danger"><?php echo form_error('referee_two_email'); ?></span>
                              </div>
                              <div class="col-sm-4 col-xs-12">
                                <label>Referee 3</label>
                                <input type="text" class="form-control" id="" placeholder="Company:" name="referee_three_company"><br/>
                                <span class="text-danger"><?php echo form_error('referee_three_company'); ?></span>
                                <input type="text" class="form-control" id="" placeholder="Contact Name:" name="referee_three_contact_name"><br/>
                                <span class="text-danger"><?php echo form_error('referee_three_contact_name'); ?></span>
                                <input type="text" class="form-control" id="" placeholder="Phone:" name="referee_three_phone"><br/>
                                <span class="text-danger"><?php echo form_error('referee_three_phone'); ?></span>
                                <input type="text" class="form-control" id="" placeholder="Email:" name="referee_three_email"><br/>
                                <span class="text-danger"><?php echo form_error('referee_three_email'); ?></span>
                              </div>
                        </div>
                            
                      </div>
                  </div>
                  <!-- /.box-body -->

                  <div class="box-footer">
                    <button type="button" class="btn btn-primary form_new_supplier_btn" value="0"  data-toggle="modal" data-target="#confirm-delete" data-vl="Submit">Submit</button>&nbsp;<button type="button" class="btn btn-warning form_new_supplier_btn" value="1" data-toggle="modal" data-target="#confirm-delete" data-vl="Save as draft">Save as draft</button>
                  </div>
                      
                      
                <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3>Supplier Application From</h3>
                            </div>
                            <div class="modal-body">
                                Are you sure you want to submit this form?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                <button type="submit" id="form_supplier_submit_btn" name="supplier_status" value="" class="btn btn-primary">Yes</button>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
              
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
         
        </div>
      </div>
</div>
</form>