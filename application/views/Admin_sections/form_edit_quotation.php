
<form action="<?php echo base_url('Admin/form_quotation_validation') ?>" method="post" enctype="multipart/form-data">
<input type="hidden" name="quotation_id" value="<?php echo $quotation_info->autonum ?>"/>
<input type="hidden" name="quotation_id_enc" value="<?php echo sha1(sha1(sha1($quotation_info->autonum))); ?>"/>
<input type="hidden" name="supplier_id" value="<?php echo $suppliers_info->autonum; ?>"/>

<div class="content">
    <div class="form_error">
      <?php echo validation_errors(); ?>
    </div>
    <section class="content-header">
        <h1>&nbsp;</h1>  
        <ol class="breadcrumb">
            <li><a href="#">Update Quotation</a></li>
        </ol>
    </section>
    
    <div class="box box-primary">
        <div class="box-header with-border">
          <h1 class="box-title">Supplier Quote Form</h1>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
              <div class="col-sm-12">
                  <form role="form">
                  <div class="box-body">
                    <h3 class="">Supplier Information</h3><hr/>
                      <div class="col-md-6 col-xs-12">
                          <div class="box box-solid">
                            <!-- /.box-header -->
                            <div class="box-body">
                              <label class="">Company&nbsp;Name:</label>
                                <p><?php echo $suppliers_info->company_name; ?></p>
                              <label class="">Phone Number:</label>
                                <p><?php echo $suppliers_info->phone_number_sales; ?>&nbsp;<small>sales</small>&nbsp;/&nbsp;<?php echo $suppliers_info->phone_number_accounts; ?>&nbsp;<small>accounts</small></p>
                              <label class="">Email:</label>
                                <p><small>sales:</small>&nbsp;<?php echo $suppliers_info->email_address_sales; ?>&nbsp;/&nbsp;<small>accounts:</small>&nbsp;<?php echo $suppliers_info->email_address_accounts; ?>&nbsp;</p>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /.box -->
                        </div>
                      
                      <div class="col-md-6 col-xs-12">
                          <div class="box box-solid">
                            <!-- /.box-header -->
                            <div class="box-body">
                              <label for="" class="">Street&nbsp;Address:</label>
                                <p><?php echo $suppliers_info->street_address; ?></p>
                              <label for="" class="">Postal&nbsp;Address:</label>
                                <p><?php echo $suppliers_info->postal_address; ?></p>
                              <label class="">Years in operation:</label>
                                <p><?php echo $suppliers_info->years_in_operation; ?> years</p>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /.box -->
                        </div>
                        
                      <hr/>
                      <div class="row">
                          
                    <div class="col-sm-12 col-xs-12">
                    <h3 >Project Information</h3>
                          <hr/>
                         <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <?php 
                                $count = 0;
                                $arr_task_data = array();
                                foreach($sub_tasks as $row) 
                                {
                                    $active = ($count == 0) ? 'active':'';
                                    $arr_task_data[] = '<div class="tab-pane '.$active.'" id="task_'.$row->autonum.'">'
                                                        .'<label>Task Overview:</label><br/>'
                                                        .$count.'task overview<br/>'
                                                        .'<label>Task Details & Requirements:</label><br/>'
                                                        .'task details and requrements<br/>'
                                                        .'<label>Freight & Delivery Instructions:</label><br/>'
                                                        .'delivery instructions<br/>'
                                                        .'<label>Other Information important to the quote:</label><br/>'
                                                        .'other Informations<br/>'
                                                        .'</div>';
                                ?>
                                    <li class="<?php echo $active; ?>"><a href="#task_<?php echo $row->autonum ?>" data-toggle="tab" aria-expanded="<?php echo ($count == 0) ? 'true':'false' ?>" style="text-transform:capitalize;">&nbsp;&nbsp;<?php echo $row->project_name; ?></a></li>
                                <?php
                                    $count++;
                                } 
                                ?>
                            </ul>
                            <div class="tab-content">
                              <?php foreach($arr_task_data as $row){ echo $row; } ?>
                              <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                          </div>
                     </div>
                    <div class="col-sm-12">
                        <h3 >Quote Information</h3>
                    <hr/>
                    </div>
                     <div class="col-sm-4 col-xs-12">
                      <label>Contact Name:</label>
                                <input type="text" class="form-control" name="contact_name" value="<?php echo $quotation_info->contact_name ?>" />
                                <span class="text-danger"><?php echo form_error('contact_name'); ?></span>
                     </div>
                     <div class="col-sm-4 col-xs-12">
                         <label>Contact Number:</label>
                                <input type="text" class="form-control" name="contact_number" value="<?php echo $quotation_info->contact_number ?>" />
                                <span class="text-danger"><?php echo form_error('contact_number'); ?></span>
                     </div>
                     <div class="col-sm-4 col-xs-12">
                         <label>Contact Email:</label>
                                <input type="text" class="form-control" name="contact_email" value="<?php echo $quotation_info->contact_email ?>" />
                                <span class="text-danger"><?php echo form_error('contact_email'); ?></span>
                     </div>
                      </div>
                      
                      <table id="tbl_product_quotation" class="table table-bordered" style="margin-top:15px;">
                        <thead>
                            <tr>
                                <th>Product Name or Code</th>
                                <th style="width:30%;">Description</th>
                                <th>Unit Price (AUD) per Qty</th>
                                <th>Qty</th>
                                <th>Unit</th>
                                <th>Sub Total</th>
                                <th>GST (AUD)</th>
                                <th>Total Incl. GST (AUD)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total_sub_total = 0;
                            $total_gst = 0;
                            $total_incld_gst = 0;
                            if(count($quotation_info_products) == 0){
                                ?>
                                <tr class="tr_quotation_products">
                                    <td><input type="text" name="product_name[]" class="form-control" value=""></td>
                                    <td class="product_description"><textarea name="description[]" class="form-control"></textarea></td>
                                    <td><input type="text" step="any" name="unit_price[]" class="form-control text-right inpt_unit_price" value=""></td>
                                    <td><input type="number" step="any" name="qty[]" class="form-control text-center inpt_qty" value=""></td>
                                    <td><input type="text" name="unit[]" class="form-control text-center inpt_unit" placeholder="eg. box, pcs , set , hours" value=""></td>
                                    <td class="product_amt text-right"></td>
                                    <td><input type="text" step="any" name="gst[]" class="form-control text-right inpt_gst" value="" placeholder=""></td>
                                    <td class="total_incl_gst text-right"></td>
                                </tr>
                                <?php
                            }
                            foreach($quotation_info_products as $row){
                                ?>
                            <tr class="tr_quotation_products">
                                <td><input type="text" name="product_name[]" class="form-control" value="<?php echo $row->product_name; ?>"></td>
                                <td class="product_description"><textarea name="description[]" class="form-control"><?php echo $row->description; ?></textarea></td>
                                <td><input type="text" step="any" name="unit_price[]" class="form-control text-right inpt_unit_price" value="$<?php echo Number_format($row->unit_price,2); ?>"></td>
                                <td><input type="text" step="any" name="qty[]" class="form-control text-center inpt_qty" value="<?php echo $row->qty; ?>"></td>
                                <td><input type="text" name="unit[]" class="form-control text-center inpt_unit" placeholder="eg. box, pcs , set , hours" value="<?php echo $row->unit; ?>"></td>
                                <td class="product_amt text-right">$<?php echo Number_format($row->unit_price*$row->qty,2); ?></td>
                                <td><input type="text" step="any" name="gst[]" class="form-control text-right inpt_gst" value="<?php echo ($row->gst != 0) ? '$'.Number_format($row->gst,2):''; ?>" placeholder="<?php echo Number_format(($row->unit_price*$row->qty)*.10,2); ?>"></td>
                                <td class="total_incl_gst text-right">$<?php echo Number_format($row->gst+($row->unit_price*$row->qty),2); ?></td>
                            </tr>
                            <?php
                                $total_sub_total+=$row->unit_price*$row->qty;
                                $total_gst+=$row->gst;
                                $total_incld_gst+=($row->unit_price*$row->qty)*.10+($row->unit_price*$row->qty);
                            }
                            ?>
                        </tbody>
                          <tfoot>
                              <tr><td colspan="8">&nbsp;</td></tr>
                            <tr>
                                <th colspan="2" class="text-right">Total:</th>  
                                <td colspan="3"></td>  
                                <td class="text-right" id="total_sub_total">$<?php echo Number_format($total_sub_total,2); ?></td>  
                                <td class="text-right" id="total_gst">$<?php echo Number_format($total_gst,2); ?></td>  
                                <td class="text-right" id="total_gst_incld">$<?php echo Number_format($total_incld_gst,2); ?></td>  
                            </tr>
                          </tfoot>
                      </table>
                      <hr/>
                      <div class="col-sm-1 pull-right">
                        <button type="button" class="btn btn-block btn-success btn-sm" id="btn_add_products">Add Row</button>
                      </div>
                      <div class="col-sm-12 no-gutter">
                        <label>Freight:</label>
                            <div class="col-sm-12">
                                <input type="radio" class="flat-red quality_assurance_radio_btn" name="freight_stat" value="1" <?php echo ('1' == $quotation_info->freight_stat) ? "checked":'' ?>>&nbsp;&nbsp;Yes&nbsp;&nbsp;
                                <input type="radio" class="flat-red quality_assurance_radio_btn" name="freight_stat" value="0" <?php echo ('0' == $quotation_info->freight_stat) ? "checked":'' ?>>&nbsp;&nbsp;No
                            </div>
                          <span class="text-danger"><?php echo form_error('freight_stat'); ?></span>
                          <div class="col-sm-12">
                          <small>Please add freight to the above quote. If there is not freight, please explain why. </small>
                              <textarea class="form-control" name="freight_explanation"><?php echo $quotation_info->freight_explanation; ?></textarea>
                              <span class="text-danger"><?php echo form_error('freight_explanation'); ?></span>
                          </div>
                      </div>
                      <div class="col-sm-12 no-gutter" style="margin-top:15px;">
                        <hr/>
                          <h3>
                            <label>Payment Information</label>
                          </h3>
                          <small>Should you quote be processed as an order, please advise your payment methods, policy and legal information. </small><br/>
                          <hr/>
                          <label>Payment Method:</label>
                                <select class="form-control" name="payment_method">
                                    <option value="" disabled <?php echo ($quotation_info->payment_method == '') ? "selected":'' ?> ><strong>Select Payment:</strong></option>
                                    <option value="Cash" <?php echo ('Cash' == $quotation_info->payment_method) ? "selected":'' ?>>Cash</option>
                                    <option value="Credit Card" <?php echo ('Credit Card' == $quotation_info->payment_method) ? "selected":'' ?>>Credit Card</option>
                                    <option value="On Account" <?php echo ('On Account' == $quotation_info->payment_method) ? "selected":'' ?>>On Account</option>
                                    <option value="Any Method" <?php echo ('Any Method' == $quotation_info->payment_method) ? "selected":'' ?>>Any Method</option>
                                </select>
                          <span class="text-danger"><?php echo form_error('payment_method'); ?></span>
                            <label>Payment Terms:</label>
                                <textarea class="form-control" name="payment_terms"><?php echo $quotation_info->payment_terms ?></textarea>
                                <span class="text-danger"><?php echo form_error('payment_terms'); ?></span>
                            <label>Payment Details:</label>
                                <textarea class="form-control" name="payment_details"><?php echo $quotation_info->payment_details ?></textarea>
                                <span class="text-danger"><?php echo form_error('payment_details'); ?></span>
                            <label>Terms and Condition of Sales:&nbsp;</label><small>Attach file (pdf only)
</small>
                                <?php if($quotation_info->payment_attachment !=''){
                                        ?>
                                            <a href="<?php echo base_url('assets/attachments/quality assurance/'.$quotation_info->payment_attachment); ?>" target="_blank">&nbsp;<i class="fa fa-expand"></i>&nbsp;View</a>
                                    <?php    } ?>
                                <input type="file" class="form-control" name="payment_attachment">
                                <span class="text-danger"><?php echo form_error('payment_attachment'); ?></span>
                        
                      </div>
                      
                      
                  </div>
                  <!-- /.box-body -->

                  <div class="box-footer">
                    <button type="button" class="btn btn-primary form_add_quotation_btn" value="0"  data-toggle="modal" data-target="#confirm-delete" data-vl="Send">Send Quotation</button>&nbsp;<button type="button" class="btn btn-warning form_add_quotation_btn" value="1" data-toggle="modal" data-target="#confirm-delete" data-vl="Save as draft">Save as draft</button>
                  </div>
                      
                      
                <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3>Quotation Application From</h3>
                            </div>
                            <div class="modal-body">
                                Are you sure you want to submit this form?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                <button type="submit" id="form_quotation_submit_btn" name="quotation_status" value="" class="btn btn-primary">Yes</button>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
              
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
         
        </div>
      </div>
</div>
</form>