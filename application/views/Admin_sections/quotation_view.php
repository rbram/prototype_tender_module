<div class="box box-primary">
<div class="box-header with-border">
  <h3 class="box-title">Quote Number: <?php echo str_pad($quotation_info->quotation_number, 4, '0', STR_PAD_LEFT); ?>&nbsp;<small><?php echo $quotation_info->project_name; ?></small></h3>

  <div class="box-tools pull-right">
    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
    </button>
    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
  </div>
</div>
<!-- /.box-header -->
<div class="box-body">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> <span id="lbl_company_name"><?php echo $quotation_info->company_name; ?></span>
            <small class="pull-right"><strong>Quote #:</strong>&nbsp;&nbsp;<?php echo str_pad($quotation_info->quotation_number, 4, '0', STR_PAD_LEFT); ?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
    <div class="row">
        <h4 class="text-center">Project Information</h4><hr/>
        <div class="col-sm-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <?php 
                $count = 0;
                $arr_task_data = array();
                foreach($sub_tasks as $row) 
                {
                    $active = ($count == 0) ? 'active':'';
                    $arr_task_data[] = '<div class="tab-pane '.$active.'" id="task_'.$row->autonum.'">'
                                        .'<label>Task Overview:</label><br/>'
                                        .$count.'task overview<br/>'
                                        .'<label>Task Details & Requirements:</label><br/>'
                                        .'task details and requrements<br/>'
                                        .'<label>Freight & Delivery Instructions:</label><br/>'
                                        .'delivery instructions<br/>'
                                        .'<label>Other Information important to the quote:</label><br/>'
                                        .'other Informations<br/>'
                                        .'</div>';
                ?>
                    <li class="<?php echo $active; ?>"><a href="#task_<?php echo $row->autonum ?>" data-toggle="tab" aria-expanded="<?php echo ($count == 0) ? 'true':'false' ?>" style="text-transform:capitalize;">&nbsp;&nbsp;<?php echo $row->project_name; ?></a></li>
                <?php
                    $count++;
                } 
                ?>
            </ul>
            <div class="tab-content">
              <?php foreach($arr_task_data as $row){ echo $row; } ?>
              <!-- /.tab-pane -->
            </div>
            </div>
            <!-- /.tab-content -->
          </div>
    </div>
      <!-- info row -->
      <div class="row invoice-info ">
          <h4 class="text-center">Supplier Information and Quotation</h4><hr/>
        <div class="col-sm-6 invoice-col">
          <strong>Contact Name:</strong>
            <?php echo $quotation_info->contact_name;?><br/>
          <strong>Contact Number:</strong>
            <?php echo $quotation_info->contact_number;?><br/>
          <strong>Contact Email:</strong>
            <?php echo $quotation_info->contact_email;?>
        </div>
        <!-- /.col -->
        
        <!-- /.col -->
        <div class="col-sm-6 invoice-col">
          <b>Date Submitted:</b> <?php echo $quotation_info->date_submitted ?><br>
          <b>Quotation Due:</b> <?php echo $quotation_info->expiration_date ?><br>
          <b>Account:</b> 968-34567
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    <hr/>
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped table-hover">
            <thead>
            <tr>
              <th>Product Name / Code</th>
              <th>Description</th>
              <th>Qty</th>
              <th>Unit</th>
              <th>Unit Price(AUD)</th>
              <th>Sub Total(AUD)</th>
              <th>GST(AUD)</th>
              <th>Total Incld. GST(AUD)</th>
            </tr>
            </thead>
            <tbody>
            <?php
                $total_unit_price = 0;
                $total_gst = 0;
                $total_unit_gst = 0;
                $total_sub_total = 0;
                foreach($quotation_product_lists as $row){
                    $sub_total = $row->unit_price*$row->qty;
                    ?>
                    <tr>
                        <td><?php echo $row->product_name; ?></td>
                        <td><?php echo $row->description; ?></td>
                        <td class="text-center"><?php echo $row->qty; ?></td>
                        <td class="text-center"><?php echo $row->unit; ?></td>
                        <td class="text-right">$<?php echo number_format($row->unit_price,2); ?></td>
                        <td class="text-right">$<?php echo number_format($sub_total,2); ?></td>
                        <td class="text-right">$<?php echo number_format($row->gst,2); ?></td>
                        <td class="text-right">$<?php echo number_format($row->unit_price+$row->gst,2); ?></td>
                    </tr>
                    <?php
                    $total_unit_price+=$row->unit_price;
                    $total_gst+=$row->gst;
                    $total_sub_total += $sub_total;
                    $total_unit_gst+=($sub_total+$row->gst);
                    
                }
            ?>
                <tr>
                    <td colspan="8"></td>
                </tr>
                <tr>
                    <td colspan="5" class="text-right"><strong>Total:</strong></td>
                    <td class="text-right">$<?php echo number_format($total_sub_total,2); ?></td>
                    <td class="text-right">$<?php echo number_format($total_gst,2); ?></td>
                    <td class="text-right">$<?php echo number_format($total_unit_gst,2); ?></td>
                </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    <hr/>
      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Payment:</p>
          
            <strong>Method:</strong>
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            <?php echo $quotation_info->payment_method;?>
          </p>
            <strong>Terms:</strong>
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            <?php echo $quotation_info->payment_terms;?>
          </p>
            <strong>Details:</strong>
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            <?php echo $quotation_info->payment_details;?>
          </p>
            <strong>Terms And Condition of Sales:</strong>
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            <a href="<?php echo base_url('assets/attachments/quality assurance/'.$quotation_info->payment_attachment); ?>" target="_blank">&nbsp;<i class="fa fa-expand"></i>&nbsp;View</a>
          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead">Freight</p>
            <strong>Status:</strong>
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                <?php echo ($quotation_info->freight_stat == 1) ? 'Yes, ':'No, ' ?>
              <?php echo $quotation_info->freight_explanation;?>
          </p>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          
            <?php if(in_array($quotation_info->status,array(1,2)))
            {
            ?>
            <a href="<?php echo base_url('Admin/quotation_edit/'.$quotation_info->autonum) ?>" class="btn btn-info pull-right" style="margin-right: 5px;"><i class="fa fa-edit" ></i> Edit</a>
            <?php
            }else{
            ?>
              <button type="button" class="btn btn-success pull-right btn_quote_update" value="4" style="margin-right: 5px;"><i class="fa fa-trophy" ></i> Award
              </button>
              <button type="button" class="btn btn-danger pull-right btn_quote_update" value="5" style="margin-right: 5px;">
                <i class="fa fa-close"></i> Reject
              </button>
            <?php
            } ?>
        </div>
      </div>
</div>
<!-- /.box-body -->
<div class="box-footer">
  
</div>
<!-- /.box-footer-->
</div>

