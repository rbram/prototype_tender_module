
<form action="<?php echo base_url('Admin/form_supplier_validation_update') ?>" method="post" enctype="multipart/form-data">
<input type="hidden" value="<?php echo sha1(sha1($supplier_company_details->autonum)) ?>" name="companycode" />
<input type="hidden" value="<?php echo $supplier_company_details->autonum ?>" name="companyid" />
<div class="content">
    <div class="form_error">
      <?php echo validation_errors(); ?>
    </div>
    <section class="content-header">
        <h1>&nbsp;<a href="<?php echo base_url('Admin/view_supplier/'.$supplier_company_details->autonum) ?>" class="btn btn-danger pull-right">Cancel</a></h1>
        <br/>
    </section>
    
    <div class="box box-warning">
        <div class="box-header with-border">
          <h1 class="box-title">Edit Supplier Information</h1>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
              <div class="col-sm-12">
                  <form role="form">
                    <h3 class="text-center">Company&nbsp;Details</h3><hr/>
                  <div class="box-body">
                      <div class="form-group">
                          <label class="">Full&nbsp;Company&nbsp;Name:</label>
                          <input type="text" name="company_name" class="form-control" id="" placeholder="..." value="<?php echo $supplier_company_details->company_name; ?>" name="company_name">
                          <span class="text-danger"><?php echo form_error('company_name'); ?></span>
                        </div>
                        <div class="form-group">
                            <label for="" class="">ABN&nbsp;and/or&nbsp;ACN:</label>
                            <input type="text" class="form-control" id="" placeholder="..." value="<?php echo $supplier_company_details->abn_acn; ?>" name="abn_acn">
                            <span class="text-danger"><?php echo form_error('abn_acn'); ?></span>
                        </div>
                        <div class="form-group">
                            <label for="" class="">Street&nbsp;Address:</label>
                            <input type="text" class="form-control" id="" placeholder="..." value="<?php echo $supplier_company_details->street_address; ?>" name="street_address">
                            <span class="text-danger"><?php echo form_error('street_address'); ?></span>
                        </div>
                        <div class="form-group">
                            <label for="" class="">Postal&nbsp;Address:</label><small><br/>*If&nbsp;different&nbsp;to&nbsp;street&nbsp;address</small>
                            <input type="text" class="form-control" id="" placeholder="..." value="<?php echo $supplier_company_details->postal_address; ?>" name="postal_address">
                            <span class="text-danger"><?php echo form_error('postal_address'); ?></span>
                        </div>
                        <div class="form-group">
                            <label for="" class="">Phone&nbsp;Numbers:</label>
                                <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="For Sales/Orders:" value="<?php echo $supplier_company_details->phone_number_sales; ?>" name="phone_number_sales">
                                    <span class="text-danger"><?php echo form_error('phone_number_sales'); ?></span>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="For Accounts" value="<?php echo $supplier_company_details->phone_number_accounts; ?>" name="phone_number_accounts">
                                    <span class="text-danger"><?php echo form_error('phone_number_accounts'); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="">Email&nbsp;Addresses:</label>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="For Sales/Orders:" name="email_address_sales" value="<?php echo $supplier_company_details->email_address_sales; ?>">
                                    <span class="text-danger"><?php echo form_error('email_address_sales'); ?></span>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" id="" placeholder="For Accounts" name="email_address_accounts" value="<?php echo $supplier_company_details->email_address_accounts; ?>">
                                    <span class="text-danger"><?php echo form_error('email_address_accounts'); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="">Website:</label>
                            <input type="text" class="form-control" id="" placeholder="..." name="website" value="<?php echo $supplier_company_details->website; ?>">
                            <span class="text-danger"><?php echo form_error('website'); ?></span>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="">Account&nbsp;Manager&nbsp;Name:</label>
                            <input type="text" class="form-control" id="" placeholder="..." name="account_manager_name" value="<?php echo $supplier_company_details->account_manager_name; ?>">
                            <span class="text-danger"><?php echo form_error('account_manager_name'); ?></span>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="">Years&nbsp;in&nbsp;Operation:</label>
                            <input type="text" class="form-control" id="" placeholder="..." name="years_in_operation" value="<?php echo $supplier_company_details->years_in_operation ?>">
                            <span class="text-danger"><?php echo form_error('years_in_operation'); ?></span>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="">Number&nbsp;of&nbsp;Employees:</label>
                            <input type="text" class="form-control" id="" placeholder="..." name="number_of_employees" value="<?php echo $supplier_company_details->number_of_employees; ?>">
                            <span class="text-danger"><?php echo form_error('number_of_employees'); ?></span>
                        </div>
                      <hr/>
                      <h3 class="text-center">Supplier&nbsp;Services</h3>
                      <div class="col-sm-6 col-xs-12 no-gutter">
                      <div class="box box-primary">
                        <div class="box-header">
                          <h3 class="box-title">Category&nbsp;of&nbsp;services:<br/><small>*please tick all service offering that apply</small></h3>
                        </div>
                        <div class="box-body">
                            <div class="col-sm-6 col-xs-12">
                                <ul class="nav nav-stacked">
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" <?php  echo (in_array('Timber / Joinert',$supplier_services_category)) ?  "checked='checked'":''; ?> value="Timber / Joinert">&nbsp;&nbsp;Timber&nbsp;/&nbsp;Joinery</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" <?php  echo (in_array('Powder / Coating',$supplier_services_category)) ?  "checked='checked'":''; ?> value="Powder / Coating">&nbsp;&nbsp;Poweder&nbsp;/&nbsp;Coating</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" <?php  echo (in_array('Upholstery and Framing',$supplier_services_category)) ?  "checked='checked'":''; ?> value="Upholstery and Framing">&nbsp;&nbsp;Upholstery&nbsp;and&nbsp;Framing</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" <?php  echo (in_array('Wholesaler',$supplier_services_category)) ?  "checked='checked'":''; ?> value="Wholesaler">&nbsp;&nbsp;Wholesaler</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" <?php  echo (in_array('Shop fitting',$supplier_services_category)) ?  "checked='checked'":''; ?> value="Shop fitting">&nbsp;&nbsp;Shop&nbsp;fitting</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" <?php  echo (in_array('Tiler',$supplier_services_category)) ?  "checked='checked'":''; ?> value="Tiler">&nbsp;&nbsp;Tiler</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" <?php  echo (in_array('Electroplating',$supplier_services_category)) ?  "checked='checked'":''; ?> value="Electroplating">&nbsp;&nbsp;Electroplating</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" <?php  echo (in_array('CAD',$supplier_services_category)) ?  "checked='checked'":''; ?> value="CAD">&nbsp;&nbsp;CAD</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" <?php  echo (in_array('Compact Laminate',$supplier_services_category)) ?  "checked='checked'":''; ?> value="Compact Laminate">&nbsp;&nbsp;Compact&nbsp;Laminate</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" <?php  echo (in_array('Labour',$supplier_services_category)) ?  "checked='checked'":''; ?> value="Labour">&nbsp;&nbsp;Labour</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" <?php  echo (in_array('Freight',$supplier_services_category)) ?  "checked='checked'":''; ?> value="Freight">&nbsp;&nbsp;Freight</li>
                                </ul>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <ul class="nav nav-stacked">
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" <?php  echo (in_array('Metal Fabrication',$supplier_services_category)) ?  "checked='checked'":''; ?> value="Metal Fabrication">&nbsp;&nbsp;Metal&nbsp;Fabrication</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" <?php  echo (in_array('Upholstery',$supplier_services_category)) ?  "checked='checked'":''; ?> value="Upholstery">&nbsp;&nbsp;Upholstery</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" <?php  echo (in_array('Furniture',$supplier_services_category)) ?  "checked='checked'":''; ?> value="Furniture">&nbsp;&nbsp;Furniture</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" <?php  echo (in_array('Painting Staining and 2pac',$supplier_services_category)) ?  "checked='checked'":''; ?> value="Painting Staining and 2pac">&nbsp;&nbsp;Painting&nbsp;Staining&nbsp;and&nbsp;2pac</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" <?php  echo (in_array('Stone Mason',$supplier_services_category)) ?  "checked='checked'":''; ?> value="Stone Mason">&nbsp;&nbsp;Stone&nbsp;Mason</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" <?php  echo (in_array('Fabric Supplier',$supplier_services_category)) ?  "checked='checked'":''; ?> value="Fabric Supplier">&nbsp;&nbsp;Fabric&nbsp;Supplier</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" <?php  echo (in_array('Electrical',$supplier_services_category)) ?  "checked='checked'":''; ?> value="Electrical">&nbsp;&nbsp;Electrical</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" <?php  echo (in_array('Water proofing',$supplier_services_category)) ?  "checked='checked'":''; ?> value="Water proofing">&nbsp;&nbsp;Waterproofing</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" <?php  echo (in_array('Installation and Rigging',$supplier_services_category)) ?  "checked='checked'":''; ?> value="Installation and Rigging">&nbsp;&nbsp;Installation&nbsp;and&nbsp;Rigging</li>
                                    <li><input type="checkbox" class="flat-red" name="category_of_services[]" <?php  echo (in_array('Warehousing Storage',$supplier_services_category)) ?  "checked='checked'":''; ?> value="Warehousing Storage">&nbsp;&nbsp;Warehousing&nbsp;Storage</li>
                                    <li><input type="text" class="form-control" placeholder="Others please specify." name="category_of_services_others" value="<?php echo $supplier_services->	category_of_services_others; ?>" /></li>
                                </ul>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                        </div>
                      </div>
                      </div>
                      <div class="col-sm-6 col-xs-12 no-gutter-right">
                            <div class="form-group">
                                <label for="" class="">Detail&nbsp;of&nbsp;Service:</label>
                                <br/>
                                <small>*Provide&nbsp;any&nbsp;further&nbsp;details&nbsp;regarding&nbsp;materials&nbsp;or&nbsp;services&nbsp;offered.</small>
                                <textarea class="form-control" id="" placeholder="..." name="details_of_service" ><?php echo $supplier_services->details_of_service; ?></textarea>
                                <span class="text-danger"><?php echo form_error('details_of_service'); ?></span>
                            </div>
                          <div class="form-group">
                                <label for="inputPassword3" class="">Expertise&nbsp;/&nbsp;Certification:</label>
                                <textarea class="form-control" id="" placeholder="..." name="expertise_certification"><?php echo $supplier_services->expertise_certification; ?></textarea>
                              <span class="text-danger"><?php echo form_error('expertise_certification'); ?></span>
                            </div>
                          <div class="form-group">
                                <label for="inputPassword3" class="">Delivery</label>
                                <br/>
                                <ul class="nav nav-stacked">
                                    <li><input type="checkbox" class="flat-red" name="delivery_stat" value="1" <?php  echo ($supplier_services->delivery_stat == 1) ?  "checked='checked'":''; ?>><label>&nbsp;&nbsp;Yes</label></li>
                                    <li><input type="text" class="form-control" placeholder="Please specify. if known" name="delivery_stat_known" value="<?php echo $supplier_services->delivery_stat_known; ?>" /></li>
                                    <span class="text-danger"><?php echo form_error('delivery_stat_known'); ?></span>
                                </ul>
                            </div>
                          <div class="form-group">
                                <label for="inputPassword3" class="">Standard&nbsp;Quote&nbsp;turnaround/&nbsp;time:</label>
                                <input class="form-control" id="" placeholder="..." name="standard_quote_turnaround_time" value="<?php echo $supplier_services->standard_quote_turnaround_time; ?>"/>
                              <span class="text-danger"><?php echo form_error('standard_quote_turnaround_time'); ?></span>
                            </div>
                          <div class="form-group">
                                <label for="inputPassword3" class="">Standard&nbsp;Manufacturing/&nbsp;supply&nbsp;lead&nbsp;time:</label>
                                <input class="form-control" id=""  placeholder="..." name="standard_manufacturing_supply_lead_time" value="<?php echo $supplier_services->standard_manufacturing_supply_lead_time; ?>"/>
                              <span class="text-danger"><?php echo form_error('standard_manufacturing_supply_lead_time'); ?></span>
                            </div>
                      </div>
                      <br/>
                        <h3 class="text-center">Quality&nbsp;Assurance&nbsp;/&nbsp;Warranty</h3>
                      <div class="col-sm-12  no-gutter">
                            <div class="form-group">
                                <label for="" class="">Specifics&nbsp;of&nbsp;warranty&nbsp;offered&nbsp;on good/services:</label>
                                <br/>
                                <small>*Length of time and other particulars. Attach copy of any warranty document.</small>
                                <textarea class="form-control" id="" placeholder="..." name="specific_of_warranty_offered_on_goods_services"><?php echo $supplier_quality_assurance->specific_of_warranty_offered_on_goods_services; ?></textarea>
                                <span class="text-danger"><?php echo form_error('specific_of_warranty_offered_on_goods_services'); ?></span>
                            </div>
                            <div class="form-group">
                              <label for="inputPassword3" class="">Quality&nbsp;Assurance&nbsp;procedure:</label>
                                <div class="col-sm-12">
                                    <input type="radio" class="flat-red quality_assurance_radio_btn" name="quality_assurance_procedure" value="yes" <?php  echo ($supplier_quality_assurance->quality_assurance_procedure == 'yes') ?  "checked='checked'":''; ?>>&nbsp;&nbsp;Yes&nbsp;&nbsp;
                                    <input type="radio" class="flat-red quality_assurance_radio_btn" name="quality_assurance_procedure" value="no">&nbsp;&nbsp;No
                                </div>
                            </div>
                      </div>
                      <div class="col-sm-12" style="margin-top:15px;">
                          <div class="box box-primary hidden-box" id="box_category_of_services" <?php  echo ($supplier_quality_assurance->quality_assurance_procedure == 'yes') ?  "style='display:block;'":''; ?>>
                            <div class="box-header">
                              
                            </div>
                            <div class="box-body">
                                <label for="inputPassword3" class="">Details&nbsp;of&nbsp;QA&nbsp;procedure&nbsp;/&nbsp;policy:</label>
                                <br/>
                                <small>Outline&nbsp;details&nbsp;of&nbsp;QA&nbsp;procedure.&nbsp;Annex&nbsp;a&nbsp;page&nbsp;if insufficient space. Attach a copy of any procedure document.</small>
                                <div class="col-sm-12">
                                    <textarea class="form-control" id="" placeholder="..." name="details_of_qa_procedure_policy"><?php echo $supplier_quality_assurance->details_of_qa_procedure_policy; ?></textarea>
                                    <span class="text-danger"><?php echo form_error('details_of_qa_procedure_policy'); ?></span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Attach&nbsp;file: </label><?php echo ($supplier_quality_assurance->details_of_qa_procedure_policy_attachments != '') ? '<a href="'.base_url('assets/attachments/quality assurance/'.$supplier_quality_assurance->details_of_qa_procedure_policy_attachments).'" class="fancybox"><i class="fa fa-expand"></i>&nbsp;View</a>':''; ?>
                                    <span class="text-danger"><?php echo form_error('standard_manufacturing_supply_lead_time'); ?></span>
                                  <input type="file" id="exampleInputFile" name="details_of_qa_procedure_policy_attachments" value="">
                                  <input type="hidden" name="details_of_qa_procedure_policy_attachments_old_file" value="<?php echo $supplier_quality_assurance->details_of_qa_procedure_policy_attachments; ?>">
                                </div>
                            </div>
                            <!-- /.box-body -->
    <!--
                            <div class="box-footer">

                            </div>
    -->
                            </div>
                        <h3 class="text-center">Insurance</h3>
                            <?php
                            $insurace_category_arr = array(
                                'public_liability' ,
                                'professional_indemnity',
                                'workers_compensation',
                                'marine_transit'
                            );
                            $count = 0;
                            foreach($supplier_insurance as $row){    
                            ?>
                                <div class="form-group">
                                    <label for="" class=""><?php echo $row->insurance_type; ?>:</label>
                                    <br/>
                                    <small>*Please Attach Certificate of Currency</small>
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" id="" placeholder="Name of Insurer:" name="<?php echo $insurace_category_arr[$count]; ?>_insurer" value="<?php echo $row->insurer; ?>">
                                            <span class="text-danger"><?php echo form_error($row->insurance_type.'_insurer'); ?></span>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" id="" placeholder="Policy Number:" name="<?php echo $insurace_category_arr[$count]; ?>_number" value="<?php echo $row->number; ?>">
                                            <span class="text-danger"><?php echo form_error($row->insurance_type.'_number'); ?></span>
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" id="" placeholder="Limit of Idemnity:" name="<?php echo $insurace_category_arr[$count]; ?>_indemnity" value="<?php echo $row->indemnity; ?>">
                                            <span class="text-danger"><?php echo form_error($row->insurance_type.'_indemnity'); ?></span>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" id="" placeholder="Excess:" name="<?php echo $insurace_category_arr[$count]; ?>_excess" value="<?php echo $row->excess; ?>">
                                            <span class="text-danger"><?php echo form_error($row->insurance_type.'_excess'); ?></span>
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" id="" placeholder="Period of Cover:" name="<?php echo $insurace_category_arr[$count]; ?>_period_cover" value="<?php echo $row->period_cover; ?>">
                                            <span class="text-danger"><?php echo form_error($row->insurance_type.'_period_cover'); ?></span>  
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                              <label for="exampleInputFile">Attach&nbsp;file:&nbsp;</label><?php echo ($row->attachments != '') ? '<a href="'.base_url('assets/attachments/quality assurance/'.$row->attachments).'" class="fancybox"><i class="fa fa-expand"></i>&nbsp;View</a>':''; ?>
                                              <input type="file" id="exampleInputFile" name="<?php echo $insurace_category_arr[$count]; ?>_attachment">
                                              <input type="hidden" name="<?php echo $insurace_category_arr[$count] ?>_old_file" value="<?php echo $row->attachments; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                                $count++;
                            }
                            ?>
                        <h3 class="text-center">Trading&nbsp;Terms</h3>
                        <div class="col-sm-12">
                            <div class="col-sm-6 col-xs-12">
                                <label>Payment Terms:</label>
                                <div class="col-sm-12">
                                    <ul class="nav nav-stacked">
                                        <li><input type="checkbox" class="flat-red" value="30 days" name="payment_terms[]" <?php  echo (in_array('30 days',$trading_terms_payment_terms)) ?  "checked='checked'":''; ?>><label>&nbsp;&nbsp;30 days<small>(End of Month)</small></label></li>
                                        <li><input type="checkbox" class="flat-red" value="60 days" name="payment_terms[]" <?php  echo (in_array('60 days',$trading_terms_payment_terms)) ?  "checked='checked'":''; ?>><label>&nbsp;&nbsp;60 days<small>(End of Month)</small></label></li>
                                        <li><input type="checkbox" class="flat-red" value="90 days" name="payment_terms[]" <?php  echo (in_array('90 days',$trading_terms_payment_terms)) ?  "checked='checked'":''; ?>><label>&nbsp;&nbsp;90 days<small>(End of Month)</small></label></li>
                                        <li><input class="form-control" placeholder="Other..." name="payment_terms_others" value="<?php echo $trading_terms->payment_terms_others ?>"></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <label>Credit Cards and Charges:</label>
                                <div class="col-sm-12">
                                    <ul class="nav nav-stacked">
                                        <li><input type="checkbox" class="flat-red" name="credit_cards_charges[]" value="VISA" <?php  echo (in_array('VISA',$trading_terms_credit_card_charges)) ?  "checked='checked'":''; ?>><label>&nbsp;&nbsp;VISA</label></li>
                                        <li><input type="checkbox" class="flat-red" name="credit_cards_charges[]" value="Amex" <?php  echo (in_array('Amex',$trading_terms_credit_card_charges)) ?  "checked='checked'":''; ?>><label>&nbsp;&nbsp;Amex</label></li>
                                        <li><input type="checkbox" class="flat-red" name="credit_cards_charges[]" value="Mastercard" <?php  echo (in_array('Mastercard',$trading_terms_credit_card_charges)) ?  "checked='checked'":''; ?>><label>&nbsp;&nbsp;Mastercard</label></li>
                                        <li><input class="form-control" placeholder="Charges..." name="credit_cards_charges_others" value="<?php echo $trading_terms->credit_cards_charges_others ?>"></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                            <div class="form-group">
                                <label for="" class="">Bank Account Details:</label>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" id="" placeholder="BSB:" name="bank_account_details_bsb" value="<?php echo $trading_terms->bank_account_details_bsb ?>">
                                        <span class="text-danger"><?php echo form_error('bank_account_details_bsb'); ?></span>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" id="" placeholder="Acct:" name="bank_account_details_acct" value="<?php echo $trading_terms->bank_account_details_acct ?>">
                                        <span class="text-danger"><?php echo form_error('bank_account_details_acct'); ?></span>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="" placeholder="Financial Institution:" name="bank_account_details_financial_institution" value="<?php echo $trading_terms->bank_account_details_financial_institution ?>">
                                        <span class="text-danger"><?php echo form_error('bank_account_details_financial_institution'); ?></span>
                                    </div>
                                </div>
                            </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Accounting System / Software</label>(<small>e.g xero</small>):
                                <input type="text" class="form-control" id="" placeholder="Financial Institution:" name="accounting_system_software" value="<?php echo $trading_terms->accounting_system_software ?>">
                                <span class="text-danger"><?php echo form_error('accounting_system_software'); ?></span>
                            </div>
                        </div>
                          <div class="col-sm-12">
                            <h3 class="text-center">Trade References</h3>
                              <center>
                              <small>*Provide 3 referees that will provide a reference regarding quality and service</small>
                              </center>
                              <br/>
                                <?php 
                                $references_arr = array('one','two','three');
                                $references_count = 0;
                                foreach($trading_references as $row){
                                ?>
                                <div class="col-sm-4 col-xs-12">
                                <label>Referee <?php echo $row->reference_no; ?></label>
                                <input type="text" class="form-control" id="" placeholder="Company:" name="referee_<?php echo $references_arr[$references_count]; ?>_company" value="<?php echo $row->company; ?>"><br/>
                                <span class="text-danger"><?php echo form_error('referee_one_company'); ?></span>
                                <input type="text" class="form-control" id="" placeholder="Contact Name:" name="referee_<?php echo $references_arr[$references_count]; ?>_contact_name" value="<?php echo $row->contact_name; ?>"><br/>
                                <span class="text-danger"><?php echo form_error('referee_one_contact_name'); ?></span>
                                <input type="text" class="form-control" id="" placeholder="Phone:" name="referee_<?php echo $references_arr[$references_count]; ?>_phone" value="<?php echo $row->phone; ?>"><br/>
                                <span class="text-danger"><?php echo form_error('referee_one_phone'); ?></span>
                                <input type="text" class="form-control" id="" placeholder="Email:" name="referee_<?php echo $references_arr[$references_count]; ?>_email" value="<?php echo $row->email; ?>"><br/>
                                <span class="text-danger"><?php echo form_error('referee_'.$references_arr[$references_count].'_email'); ?></span>
                              </div>  
                                <?php
                                $references_count++;
                                } ?>
                        </div>
                            
                      </div>
                  </div>
                  <!-- /.box-body -->

                  <div class="box-footer">
                    <button type="button" class="btn btn-primary form_new_supplier_btn" value="0"  data-toggle="modal" data-target="#confirm-delete" data-vl="Submit">Submit</button>&nbsp;<button type="button" class="btn btn-warning form_new_supplier_btn" value="1" data-toggle="modal" data-target="#confirm-delete" data-vl="Save as draft">Save as draft</button>
                  </div>
      
                <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3>Supplier Application From</h3>
                            </div>
                            <div class="modal-body">
                                Are you sure you want to submit this form?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                <button type="submit" id="form_supplier_submit_btn" name="supplier_status" value="" class="btn btn-primary">Yes</button>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
              
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
         
        </div>
      </div>
</div>
</form>