
<div class="content">
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total</span>
              <span class="info-box-number total_supplier_count">1000</span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
                  <span class="progress-description">
                    Quotations
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa  fa-map-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total</span>
              <span class="info-box-number">41,410</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    Projects
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-send-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total</span>
              <span class="info-box-number">41,410</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                   Tender Invitations
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="fa fa-clipboard"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total</span>
              <span class="info-box-number">41,410</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    New&nbsp;Quotations
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
<!--    ACCOUNT OPTION-->
    <!--USERS TABLE-->
    <div class="col-sm-12 no-gutter">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Project List</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                <form method="post" id="form_quotation_lists">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th colspan="7">
                        <div class="mailbox-controls">
                            <!-- Check all button -->
                            <button type="button" class="btn btn-default btn-sm quotation_check_all" value="0"><i class="fa fa-square-o"></i>
                            </button>
                            <button type="button" class="btn btn-default btn-sm remove_btn" data-toggle="modal" data-target="#modal-danger"><i class="fa fa-trash-o"></i></button>
                            <!-- /.pull-right -->
                          </div>
                        </th>    
                    </tr>
                    <tr>
                      <th></th>
                      <th>Job Code</th>
                      <th>Customer Name</th>
                      <th>Prototype Sales Person</th>
                      <th>Prototype Project Manager</th>
                      <th>Contact Number</th>
                      <th>Contact Email</th>
                      <th>Installation IN Date</th>
                      <th>Installation OUT Date</th>
                      <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($project_lists as $row){ ?> 
                       <tr>
                            <td>
                                <center>
                                    <input type="checkbox" class="quotation_check_box" name="checkbox_project_id" value="<?php echo $row->autonum; ?>">
                                </center>
                            </td>
                            <td class="view_quotation text-blue" data-quotationid="<?php echo $row->autonum; ?>"><?php echo str_pad($row->job_code, 4, '0', STR_PAD_LEFT); ?></td>
                            <td><?php echo $row->customer_name; ?></td>
                            <td><?php echo $row->prototype_sales_person; ?></td>
                            <td><?php echo $row->prototype_project_manager; ?></td>
                            <td><?php echo $row->prototype_contact_number; ?></td>
                            <td><?php echo $row->prototype_contact_email; ?></td>
                            <td><?php echo $row->installation_in_date; ?></td>
                            <td><?php echo $row->installation_out_date; ?></td>
                            <td><?php echo $row->status; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                   
                    </table>
                </form>
                </div>
            </div>
        <!-- /.box-body -->
            <div class="box-footer">

            </div>
        </div>
    </div>
    <div class="row ">
        <div id="view_quote" class="col-sm-12">
        </div>
    </div>
    
    <div class="modal modal-danger fade" id="modal-danger">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Warning</h4>
              </div>
              <div class="modal-body">
                <p>Are you sure you want to remove checked item(s)?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left close_modal" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-outline confirm_delete_btn">Yes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
    
</div>