
<form action="<?php echo base_url('Admin/form_project_validation') ?>" method="post" enctype="multipart/form-data">

<div class="content">
    <div class="form_error">
      <?php echo validation_errors(); ?>
    </div>
    <section class="content-header">
        <h1>&nbsp;</h1>  
        <ol class="breadcrumb">
            <li><a href="#">New&nbsp;Projects&nbsp;Form</a></li>
        </ol>
    </section>
    
    <div class="box box-primary">
        <div class="box-header with-border">
          <h1 class="box-title">NEW&nbsp;PROJECT&nbsp;FROM</h1>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
              <div class="col-sm-12">
                  <div class="box-body">
                    <h3 >Order Information</h3>
                      <div class="row" style="margin-bottom:10px;">
                          <div class="col-sm-2 col-xs-12">
                              <label class="">Customer ID:</label>
                              <input type="text" name="cutomer_id" class="form-control" id="" placeholder="..." value="<?php echo set_value('cutomer_id'); ?>">
                              <span class="text-danger"><?php echo form_error('cutomer_id'); ?></span>
                          </div>
                          <div class="col-sm-10 col-xs-12 no-gutter-left">
                              <label class="">Company Name:</label>
                              <input type="text" class="form-control" id="" placeholder="..." value="<?php echo set_value('customer_name'); ?>" name="customer_name">
                              <span class="text-danger"><?php echo form_error('customer_name'); ?></span>
                          </div>
                      </div>
                      <div class="row" style="margin-bottom:10px;">
                          <div class="col-sm-8 col-xs-12">
                              <label class="">Street:</label>
                              <input type="text" class="form-control" id="" placeholder="..." value="<?php echo set_value('stree_line_one'); ?>" name="stree_line_one">
                              <span class="text-danger"><?php echo form_error('stree_line_one'); ?></span>
                          </div>
                          <div class="col-sm-1 col-xs-12 no-gutter-left">
                              <label class="">Suburb:</label>
                              <input type="text" class="form-control" id="" placeholder="..." value="<?php echo set_value('suburb'); ?>" name="suburb">
                              <span class="text-danger"><?php echo form_error('suburb'); ?></span>
                          </div>
                          <div class="col-sm-1 col-xs-12 no-gutter-left">
                              <label class="">State:</label>
                              <input type="text" class="form-control" id="" placeholder="..." value="<?php echo set_value('suburb'); ?>" name="suburb">
                              <span class="text-danger"><?php echo form_error('suburb'); ?></span>
                          </div>
                          <div class="col-sm-1 col-xs-12 no-gutter-left">
                              <label class="">P/Code:</label>
                              <input type="text" class="form-control" id="" placeholder="..." value="<?php echo set_value('post_code'); ?>" name="post_code">
                              <span class="text-danger"><?php echo form_error('post_code'); ?></span>
                          </div>
                          <div class="col-sm-1 col-xs-12 no-gutter-left">
                              <label class="">Country:</label>
                              <input type="text" class="form-control" id="" placeholder="..." value="<?php echo set_value('post_code'); ?>" name="post_code">
                              <span class="text-danger"><?php echo form_error('post_code'); ?></span>
                          </div>
                      </div>
                      
                      <div class="row" style="margin-bottom:10px;">
                          <div class="col-sm-4 col-xs-12">
                              <label class="">Contact Person:</label>
                              <input type="text" name="customer_contact_name" class="form-control" id="" placeholder="..." value="<?php echo set_value('customer_contact_name'); ?>">
                              <span class="text-danger"><?php echo form_error('customer_contact_name'); ?></span>
                          </div>
                          <div class="col-sm-4 col-xs-12 no-gutter-left">
                              <label class="">Email:</label>
                              <input type="text" class="form-control" id="" placeholder="..." value="<?php echo set_value('customer_email'); ?>" name="customer_email">
                              <span class="text-danger"><?php echo form_error('customer_email'); ?></span>
                          </div>
                          <div class="col-sm-1 col-xs-12 no-gutter-left">
                              <label class="">Phone number:</label>
                              <input type="text" class="form-control" id="" placeholder="..." value="<?php echo set_value('customer_phone_number'); ?>" name="customer_phone_number">
                              <span class="text-danger"><?php echo form_error('customer_phone_number'); ?></span>
                          </div>
                          <div class="col-sm-1 col-xs-12 no-gutter-left">
                              <label class="">Mobile number:</label>
                              <input type="text" class="form-control" id="" placeholder="..." value="<?php echo set_value('customer_mobile_number'); ?>" name="customer_mobile_number">
                              <span class="text-danger"><?php echo form_error('customer_mobile_number'); ?></span>
                          </div>
                          <div class="col-sm-1 col-xs-12 no-gutter-left">
                              <label class="">Fax:</label>
                              <input type="text" class="form-control" id="" placeholder="..." value="<?php echo set_value('customer_fax'); ?>" name="customer_fax">
                              <span class="text-danger"><?php echo form_error('customer_fax'); ?></span>
                          </div>
                          <div class="col-sm-1 col-xs-12 no-gutter-left">
                              <label class="">Contact ID:</label>
                              <input type="text" class="form-control" id="" placeholder="..." value="<?php echo set_value('customer_contact_id'); ?>" name="customer_contact_id">
                              <span class="text-danger"><?php echo form_error('customer_contact_id'); ?></span>
                          </div>
                      </div>
                      
                      <hr/>
                      <h3 >Project Details</h3>
                      <div class="row" style="margin-bottom:10px;">
                          <div class="col-sm-1 col-xs-12">
                              <label class="">Job Code:</label>
                              <input type="text" class="form-control" id="" placeholder="..." value="<?php echo set_value('customer_mobile_number'); ?>" name="customer_mobile_number">
                              <span class="text-danger"><?php echo form_error('customer_mobile_number'); ?></span>
                          </div>
                          <div class="col-sm-8 col-xs-12 no-gutter-left">
                              <label class="">Delivery Address:</label>
                              <input type="text" class="form-control" id="" placeholder="..." value="<?php echo set_value('customer_mobile_number'); ?>" name="customer_mobile_number">
                              <span class="text-danger"><?php echo form_error('customer_mobile_number'); ?></span>
                          </div>
                          <div class="col-sm-1 col-xs-12 no-gutter-left">
                              <label class="">Suburb:</label>
                              <input type="text" class="form-control" id="" placeholder="..." value="<?php echo set_value('suburb'); ?>" name="suburb">
                              <span class="text-danger"><?php echo form_error('suburb'); ?></span>
                          </div>
                          <div class="col-sm-1 col-xs-12 no-gutter-left">
                              <label class="">State:</label>
                              <input type="text" class="form-control" id="" placeholder="..." value="<?php echo set_value('suburb'); ?>" name="suburb">
                              <span class="text-danger"><?php echo form_error('suburb'); ?></span>
                          </div>
                          <div class="col-sm-1 col-xs-12 no-gutter-left">
                              <label class="">P/Code:</label>
                              <input type="text" class="form-control" id="" placeholder="..." value="<?php echo set_value('post_code'); ?>" name="post_code">
                              <span class="text-danger"><?php echo form_error('post_code'); ?></span>
                          </div>
                      </div>
                      
                      <div class="row" style="margin-bottom:10px;">
                          <div class="col-sm-2 col-xs-12">
                              <label class="">Sales Person:</label>
                              <input type="text" name="customer_contact_name" class="form-control" id="" placeholder="..." value="<?php echo set_value('customer_contact_name'); ?>">
                              <span class="text-danger"><?php echo form_error('customer_contact_name'); ?></span>
                          </div>
                          <div class="col-sm-2 col-xs-12 no-gutter-left">
                              <label class="">Project Manager:</label>
                              <input type="text" class="form-control" id="" placeholder="..." value="<?php echo set_value('customer_email'); ?>" name="customer_email">
                              <span class="text-danger"><?php echo form_error('customer_email'); ?></span>
                          </div>
                          <div class="col-sm-2 col-xs-12">
                              <label class="">Contact Number:</label>
                              <input type="text" class="form-control" id="" placeholder="..." value="<?php echo set_value('customer_phone_number'); ?>" name="customer_phone_number">
                              <span class="text-danger"><?php echo form_error('customer_phone_number'); ?></span>
                          </div>
                          <div class="col-sm-2 col-xs-12 no-gutter-left">
                              <label class="">Email:</label>
                              <input type="text" class="form-control" id="" placeholder="..." value="<?php echo set_value('customer_mobile_number'); ?>" name="customer_mobile_number">
                              <span class="text-danger"><?php echo form_error('customer_mobile_number'); ?></span>
                          </div>
                          <div class="col-sm-2 col-xs-12 no-gutter-left">
                              <label class="">Installation IN Date:</label>
                              <input type="text" class="form-control" id="" placeholder="..." value="<?php echo set_value('customer_fax'); ?>" name="customer_fax">
                              <span class="text-danger"><?php echo form_error('customer_fax'); ?></span>
                          </div>
                          <div class="col-sm-2 col-xs-12 no-gutter-left">
                              <label class="">Installation OUT Date:</label>
                              <input type="text" class="form-control" id="" placeholder="..." value="<?php echo set_value('customer_contact_id'); ?>" name="customer_contact_id">
                              <span class="text-danger"><?php echo form_error('customer_contact_id'); ?></span>
                          </div>
                      </div>
                      
                      <hr/>
                      <h3 >Task Details</h3>
                      <div class="row" style="margin-bottom:10px;">
                          <div class="col-sm-12 col-xs-12">
                              <label class="">Task Overview (Title):</label>
                              <input type="text" name="customer_contact_name" class="form-control" id="" placeholder="..." value="<?php echo set_value('customer_contact_name'); ?>">
                              <span class="text-danger"><?php echo form_error('customer_contact_name'); ?></span>
                          </div>
                          <div class="row col-sm-12" style="margin-top:15px;">
                              <div class="col-sm-4 col-xs-12">
                                  <div class="box box-info">
                                    <div class="box-header with-border">
                                      <label class="box-title">Details and Requirements</label>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                      <div class="form-group">
                                            <textarea class="form-control compose-textarea" style="max-height: 300px" placeholder="..."></textarea>
                                      </div>
                                    </div>
                                    <!-- /.box-body -->
                                  </div>
                                  <span class="text-danger"><?php echo form_error('customer_email'); ?></span>
                              </div>
                              <div class="col-sm-4 col-xs-12">
                                  <div class="box box-info">
                                    <div class="box-header with-border">
                                      <label class="box-title">Freight and Delivery Instructions</label>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                      <div class="form-group">
                                            <textarea class="form-control compose-textarea" style="max-height: 300px" placeholder="..."></textarea>
                                      </div>
                                    </div>
                                    <!-- /.box-body -->
                                  </div>
                                  <span class="text-danger"><?php echo form_error('customer_email'); ?></span>
                              </div>
                              <div class="col-sm-4 col-xs-12">
                                  <div class="box box-info">
                                    <div class="box-header with-border">
                                      <label class="box-title">Other Information important to the Quote</label>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                      <div class="form-group">
                                            <textarea class="form-control compose-textarea" style="max-height: 300px" placeholder="..."></textarea>
                                      </div>
                                    </div>
                                    <!-- /.box-body -->
                                  </div>
                                  <span class="text-danger"><?php echo form_error('customer_email'); ?></span>
                              </div>
                          </div>
                      </div>
                      
                      <h3 >Dow you want to add sub task?&nbsp;</h3>
                      <input type="checkbox" data-toggle="toggle" data-on="Yes" data-off="No" data-width="100" data-size="small">
                      <div class="row col-sm-12" style="margin-top:10px;">
                          <div class="box box-primary">
                              <div class="box-header with-border">
                                  <label class="box-title">Sub Tasks</label>
                                </div>
                              <div class="box-body">
                                  <div class="box-group" id="accordion">
                                    <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs" id="sub_task_nav_tabs">
                                          <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">New Task</a></li>
                                        </ul>
                                        <div class="tab-content" id="sub_task_tab_content">
                                          <div class="tab-pane active" id="tab_1">
                                              <div class="row" style="margin-top:10px;">
                                                <div class="col-sm-1 col-xs-12">
                                                  <label>Customer ID:</label>
                                                  <input type="text" class="form-control" placeholder="..." />
                                                </div>
                                                <div class="col-sm-2 no-gutter-left col-xs-12">
                                                  <label>Customer Name:</label>
                                                  <input type="text" class="form-control" placeholder="..." />
                                                </div>
                                                <div class="col-sm-1 no-gutter-left col-xs-12">
                                                  <label>Job Code:</label>
                                                  <input type="text" class="form-control" placeholder="..." />
                                                </div>
                                                <div class="col-sm-5 no-gutter-left col-xs-12">
                                                  <label>Delivery Address:<small class="text-aqua"></small></label>
                                                  <div class="input-group">
                                                        <span class="input-group-addon">
                                                          <input type="checkbox">
                                                        </span>
                                                    <input type="text" class="form-control" placeholder="check the box if Address is same as the main project" placeholder="...">
                                                  </div>
                                                </div>
                                                <div class="col-sm-1 no-gutter-left col-xs-12">
                                                  <label>Suburb:</label>
                                                  <input type="text" class="form-control" placeholder="..." />
                                                </div>
                                                <div class="col-sm-1 no-gutter-left col-xs-12">
                                                  <label>State:</label>
                                                  <input type="text" class="form-control" placeholder="..." />
                                                </div>
                                                <div class="col-sm-1 no-gutter-left col-xs-12">
                                                  <label>Post Code:</label>
                                                  <input type="text" class="form-control" placeholder="..." />
                                                </div>
                                              </div>
                                              
                                              <div class="row" style="margin-top:10px;">
                                                <div class="col-sm-4 col-xs-12">
                                                  <label>Sales Person:</label>
                                                  <input type="text" class="form-control" placeholder="..." />
                                                </div>
                                                <div class="col-sm-4 col-xs-12 no-gutter-left">
                                                  <label>Project Manager:</label>
                                                  <input type="text" class="form-control" placeholder="..." />
                                                </div>
                                                <div class="col-sm-1 no-gutter-left col-xs-12">
                                                  <label>Contact Number:</label>
                                                  <input type="text" class="form-control" placeholder="..." />
                                                </div>
                                                <div class="col-sm-2 no-gutter-left col-xs-12">
                                                  <label>Email:</label>
                                                  <input type="text" class="form-control" placeholder="..." />
                                                </div>
                                                <div class="col-sm-1 no-gutter-left col-xs-12">
                                                  <label>Delivery Date:<small class="text-aqua"></small></label>
                                                  <input type="text" class="form-control" placeholder="..." />
                                                </div>
                                              </div>
                                            <hr/>
                                              <h4>Task Details</h4>
                                                  <div class="row" style="margin-bottom:10px;">
                                                      <div class="col-sm-12 col-xs-12">
                                                          <label class="">Task Overview (Title):</label>
                                                          <input type="text" name="customer_contact_name" class="form-control" id="" placeholder="..." value="<?php echo set_value('customer_contact_name'); ?>">
                                                          <span class="text-danger"><?php echo form_error('customer_contact_name'); ?></span>
                                                      </div>
                                                      <div class="row col-sm-12" style="margin-top:15px;">
                                                          <div class="col-sm-4 col-xs-12">
                                                              <div class="box box-info">
                                                                <div class="box-header with-border">
                                                                  <label class="box-title">Details and Requirements</label>
                                                                </div>
                                                                <!-- /.box-header -->
                                                                <div class="box-body">
                                                                  <div class="form-group tmp_text_area" id="text_area_details_">
                                                                        <textarea class="form-control compose-textarea" style="max-height: 300px" placeholder="..."></textarea>
                                                                  </div>
                                                                </div>
                                                                <!-- /.box-body -->
                                                              </div>
                                                              <span class="text-danger"><?php echo form_error('customer_email'); ?></span>
                                                          </div>
                                                          <div class="col-sm-4 col-xs-12">
                                                              <div class="box box-info">
                                                                <div class="box-header with-border">
                                                                  <label class="box-title">Freight and Delivery Instructions</label>
                                                                </div>
                                                                <!-- /.box-header -->
                                                                <div class="box-body">
                                                                  <div class="form-group tmp_text_area" id="text_area_instructions_">
                                                                        <textarea class="form-control compose-textarea" style="max-height: 300px" placeholder="..."></textarea>
                                                                  </div>
                                                                </div>
                                                                <!-- /.box-body -->
                                                              </div>
                                                              <span class="text-danger"><?php echo form_error('customer_email'); ?></span>
                                                          </div>
                                                          <div class="col-sm-4 col-xs-12">
                                                              <div class="box box-info">
                                                                <div class="box-header with-border">
                                                                  <label class="box-title">Other Information important to the Quote</label>
                                                                </div>
                                                                <!-- /.box-header -->
                                                                <div class="box-body">
                                                                  <div class="form-group tmp_text_area" id="text_area_other_">
                                                                        <textarea class="form-control compose-textarea" style="max-height: 300px" placeholder="..."></textarea>
                                                                  </div>
                                                                </div>
                                                                <!-- /.box-body -->
                                                              </div>
                                                              <span class="text-danger"><?php echo form_error('customer_email'); ?></span>
                                                          </div>
                                                      </div>
                                                  </div>
                                          </div>
                                          <!-- /.tab-pane -->
                                          
                                        </div>
                                        <!-- /.tab-content -->
                                      </div>
                                    
                                    
                                  </div>
                              </div>
                              <div class="box-footer">
                                <button type="button" class="btn btn-primary pull-right btn_add_sub_task">Add Sub Task</button>
                              </div>
                          </div>
                      </div>
                      
                      
                  </div>
                  <!-- /.box-body -->

                  <div class="box-footer">
                    <button type="button" class="btn btn-primary form_new_supplier_btn" value="0"  data-toggle="modal" data-target="#confirm-delete" data-vl="Submit">Submit</button>&nbsp;<button type="button" class="btn btn-warning form_new_supplier_btn" value="1" data-toggle="modal" data-target="#confirm-delete" data-vl="Save as draft">Save as draft</button>
                  </div>
                      
                      
                <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3>Supplier Application From</h3>
                            </div>
                            <div class="modal-body">
                                Are you sure you want to submit this form?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                <button type="submit" id="form_supplier_submit_btn" name="supplier_status" value="" class="btn btn-primary">Yes</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
              
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
         
        </div>
      </div>
</div>
</form>