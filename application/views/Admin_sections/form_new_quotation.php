
<form action="<?php echo base_url('Admin/form_quotation_validation') ?>" method="post" enctype="multipart/form-data">

<div class="content">
    <div class="form_error">
      <?php echo validation_errors(); ?>
    </div>
    <section class="content-header">
        <h1>&nbsp;</h1>  
        <ol class="breadcrumb">
            <li><a href="#">New Quotation Form</a></li>
        </ol>
    </section>
    
    <div class="box box-primary">
        <div class="box-header with-border">
          <h1 class="box-title">Supplier Quote Form</h1>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
              <div class="col-sm-12">
                  <form role="form">
                    <h3 class="text-center">Information</h3><hr/>
                  <div class="box-body">
                      <div class="col-md-6 col-xs-12">
                          <div class="box box-solid">
                            <!-- /.box-header -->
                            <div class="box-body">
                              <label class="">Company&nbsp;Name:</label>
                                <p><?php echo $suppliers_info->company_name; ?></p>
                              <label class="">Phone Number:</label>
                                <p><?php echo $suppliers_info->phone_number_sales; ?>&nbsp;<small>sales</small>&nbsp;/&nbsp;<?php echo $suppliers_info->phone_number_accounts; ?>&nbsp;<small>accounts</small></p>
                              <label class="">Email:</label>
                                <p><?php echo $suppliers_info->email_address_sales; ?>&nbsp;<small>sales</small>&nbsp;/&nbsp;<?php echo $suppliers_info->email_address_accounts; ?>&nbsp;<small>accounts</small></p>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /.box -->
                        </div>
                      
                      <div class="col-md-6 col-xs-12">
                          <div class="box box-solid">
                            <!-- /.box-header -->
                            <div class="box-body">
                              <label for="" class="">Street&nbsp;Address:</label>
                                <p><?php echo $suppliers_info->street_address; ?></p>
                              <label for="" class="">Postal&nbsp;Address:</label>
                                <p><?php echo $suppliers_info->postal_address; ?></p>
                              <label class="">Years in operation:</label>
                                <p><?php echo $suppliers_info->years_in_operation; ?> years</p>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /.box -->
                        </div>
                        
                      <hr/>
                      <div class="row">
                     <div class="col-sm-6 col-xs-12">
                         <label>Project Name:</label>
                                <select class="form-control" name="project_id">
                                    <option value="" disabled selected>Select Project</option>
                                    <?php 
                                    foreach($project_lists as $row)
                                        {
                                            ?>
                                    <option value="<?php echo $row->autonum ?>"><?php echo $row->project_name; ?></option>
                                            <?php
                                        } 
                                    ?>
                                </select>
                                <span class="text-danger"><?php echo form_error('project_id'); ?></span>
                     </div>
                     <div class="col-sm-6 col-xs-12">
                      <label>Contact Name:</label>
                                <input type="text" class="form-control" name="contact_name" />
                                <span class="text-danger"><?php echo form_error('contact_name'); ?></span>
                     </div>
                     <div class="col-sm-6 col-xs-12">
                         <label>Contact Number:</label>
                                <input type="text" class="form-control" name="contact_number" />
                                <span class="text-danger"><?php echo form_error('contact_number'); ?></span>
                     </div>
                     <div class="col-sm-6 col-xs-12">
                         <label>Contact Email:</label>
                                <input type="text" class="form-control" name="contact_email" />
                                <span class="text-danger"><?php echo form_error('contact_email'); ?></span>
                     </div>
                      </div>
                      
                      <table id="tbl_product_quotation" class="table table-bordered" style="margin-top:15px;">
                        <thead>
                            <tr>
                                <th>Product Name or Code</th>
                                <th style="width:30%;">Description</th>
                                <th>Unit Price per Qty (AUD)</th>
                                <th>Qty</th>
                                <th>Unit</th>
                                <th>Sub Total</th>
                                <th>GST (AUD)</th>
                                <th>Total Incl. GST (AUD)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="tr_quotation_products">
                                <td><input type="text" name="product_name[]" class="form-control"></td>
                                <td class="product_description"><textarea name="description[]" class="form-control"></textarea></td>
                                <td><input type="text" step="any" name="unit_price[]" class="form-control text-right inpt_unit_price" value="0" data-e-dec="5" data-a-stor=true></td>
                                <td><input type="number" step="any" name="qty[]" class="form-control text-center inpt_qty" value="0"></td>
                                <td><input type="text" name="unit[]" class="form-control text-center inpt_unit" placeholder="eg. box, pcs , set , hours" ></td>
                                <td class="product_amt text-right"></td>
                                <td><input type="text" step="any" name="gst[]" class="form-control text-right inpt_gst" value="" placeholder="0.00"></td>
                                <td class="total_incl_gst text-right"></td>
                            </tr>
                        </tbody>
                          <tfoot>
                            <tr>
                                <td colspan="8">&nbsp;</td>
                            </tr>
                            <tr>
                                <th colspan="2" class="text-right">Total:</th>  
                                <td colspan="3"></td>  
                                <td class="text-right" id="total_sub_total"></td>  
                                <td class="text-right" id="total_gst"></td>  
                                <td class="text-right" id="total_gst_incld"></td>  
                            </tr>
                          </tfoot>
                      </table>
                      <br/>
                      <div class="col-sm-1 pull-right">
                        <button type="button" class="btn btn-block btn-success btn-sm" id="btn_add_products">Add Row</button>
                      </div>
                      <div class="col-sm-12 no-gutter">
                     <hr/>
                        <label>Freight:</label>
                            <div class="col-sm-12">
                                <input type="radio" class="flat-red quality_assurance_radio_btn" name="freight_stat" value="1">&nbsp;&nbsp;Yes&nbsp;&nbsp;
                                <input type="radio" class="flat-red quality_assurance_radio_btn" name="freight_stat" value="0">&nbsp;&nbsp;No
                            </div>
                          <span class="text-danger"><?php echo form_error('freight_stat'); ?></span>
                          <div class="col-sm-12">
                          <small>Please add freight to the above quote. If there is not freight, please explain why. </small>
                              <textarea class="form-control" name="freight_explanation"></textarea>
                              <span class="text-danger"><?php echo form_error('freight_explanation'); ?></span>
                          </div>
                      </div>
                      <div class="col-sm-12 no-gutter" style="margin-top:15px;">
                        <hr/>
                          <h4 class="text-center">
                            <label>Payment</label><br/><small>Should you quote be processed as an order, please advise your payment methods, policy and legal information. </small>
                          </h4>
                          <br/>  
                          <label>Payment Method:</label>
                                <select class="form-control" name="payment_method">
                                    <option value="" disabled selected><strong>Select Payment:</strong></option>
                                    <option value="Cash">Cash</option>
                                    <option value="Credit Card">Credit Card</option>
                                    <option value="On Account">On Account</option>
                                    <option value="Any Method">Any Method</option>
                                </select>
                          <span class="text-danger"><?php echo form_error('payment_method'); ?></span>
                            <label>Payment Terms:</label>
                                <textarea class="form-control" name="payment_terms"></textarea>
                                <span class="text-danger"><?php echo form_error('payment_terms'); ?></span>
                            <label>Payment Details:</label>
                                <textarea class="form-control" name="payment_details"></textarea>
                                <span class="text-danger"><?php echo form_error('payment_details'); ?></span>
                            <label>Terms and Condition of Sales:&nbsp;</label><small>Attach file (pdf only)
</small>
                                <input type="file" class="form-control" name="payment_attachment">
                                <span class="text-danger"><?php echo form_error('payment_attachment'); ?></span>
                        
                      </div>
                      
                      
                  </div>
                  <!-- /.box-body -->

                  <div class="box-footer">
                    <button type="button" class="btn btn-primary form_add_quotation_btn" value="0"  data-toggle="modal" data-target="#confirm-delete" data-vl="Send">Send Quotation</button>&nbsp;<button type="button" class="btn btn-warning form_add_quotation_btn" value="1" data-toggle="modal" data-target="#confirm-delete" data-vl="Save as draft">Save as draft</button>
                  </div>
                      
                      
                <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3>Quotation Application From</h3>
                            </div>
                            <div class="modal-body">
                                Are you sure you want to submit this form?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                <button type="submit" id="form_quotation_submit_btn" name="quotation_status" value="" class="btn btn-primary">Yes</button>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
              
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
         
        </div>
      </div>
</div>
</form>