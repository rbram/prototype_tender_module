    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Supplier Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('Admin/forms_supplier_application') ?>"><i class="fa fa-drivers-license"></i> Supplier Lists</a></li>
        <li class="active">Supplier profile</li>
      </ol>
    </section>

    <!-- Main content -->
<div class="row">
    <section class="content">
        <div class="col-sm-2 no-gutter-right">
            <div class="col-md-12 no-gutter">

              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">
                  
                  <h3 class="profile-username text-center">Supplier Lists</h3>

                  <table class="table table-bordered table-striped" id="example1">
                    <thead>
                        <tr>
                            <th class="text-center">Click to view</th>
                        </tr>
                    </thead>  
                    <tbody>
                        <?php foreach($supplier_lists as $row){ ?>
                            <tr>
                                <td><a href="<?php echo base_url('Admin/view_supplier/'.$row->autonum) ?>"><?php echo $row->company_name; ?></a></td>
                            </tr>
                        <?php } ?>  
                    </tbody>
                  </table>

<!--                  <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>-->
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->

            </div>
        </div>
        <div class="col-sm-10 col-xs-12">
          <div class="row">
              
            <div class="col-sm-12">

              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">
                  
                  <h3 class="profile-username text-center"><?php echo $supplier_company_details->company_name; ?> 
                      <span class="pull-right"><a href="<?php echo base_url('Admin/form_edit_supplier/'.$supplier_company_details->autonum); ?> " class="btn btn-small btn-info"><i class="fa fa-edit"></i> Edit</a></span>
                      <span class="pull-right" style="margin-right:5px"><a href="#" class="btn btn-small btn-primary" data-toggle="modal" data-target="#modal-default" ><i class="fa fa-send"></i> Invite to tender</a></span>
                  </h3>
                  <p class="text-muted text-center"><b>Status:&nbsp;</b><?php echo ($supplier_company_details->status == 1) ? '<span class="text-orange">Draft</span>':'<span class="text-green">Submited</span>' ?></p>

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                              <b>ABN and/or ACN:</b><br/>&nbsp;<?php echo $supplier_company_details->abn_acn; ?>
                            </li>
                            <li class="list-group-item">
                              <b>Street Address:</b><br/>&nbsp;<?php echo $supplier_company_details->street_address; ?>
                            </li>
                            <li class="list-group-item">
                              <b>Postal Address:</b><br/>&nbsp;<?php echo $supplier_company_details->postal_address; ?>
                            </li>
                          </ul>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                              <b>Phone Numbers:</b><br/>&nbsp;<?php echo '<small>Sales:</small>&nbsp;'.$supplier_company_details->phone_number_sales.' / <small>Accounts:</small>&nbsp;'.$supplier_company_details->phone_number_accounts; ?> 
                            </li>
                            <li class="list-group-item">
                              <b>Email Addresses:</b><br/>&nbsp;<?php echo '<small>Sales:</small>&nbsp;'.$supplier_company_details->email_address_sales.' / <small>Accounts:</small>&nbsp;'.$supplier_company_details->email_address_accounts; ?>
                            </li>
                            <li class="list-group-item">
                                <b>Website:</b><br/>&nbsp;<a href="https:<?php echo $supplier_company_details->website; ?>" target="_blank"><?php echo $supplier_company_details->website; ?></a>
                            </li>
                          </ul>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                              <b>Account Manager:</b><br/>&nbsp;<?php echo $supplier_company_details->account_manager_name; ?>
                            </li>
                            <li class="list-group-item">
                              <b>Years in Operation:</b><br/>&nbsp;<?php echo $supplier_company_details->years_in_operation; ?>
                            </li>
                            <li class="list-group-item">
                              <b>Number of Employees</b><br/>&nbsp;<?php echo $supplier_company_details->number_of_employees; ?>
                            </li>
                          </ul>
                    </div>

                </div>
                  
              </div>
              <!-- /.box -->

            </div>
            <!-- /.col -->
            <div class="col-md-12">
                <input type="radio" name="view_type" checked="checked" class="view_type" value="0"/>&nbsp;Tablular&nbsp;<input type="radio" name="view_type" class="view_type" value="1"/>&nbsp;Show All
                <br/>
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs" id="tab_supplier_navigation">
                  <li class="active"><a href="#supplier_services_tab" data-toggle="tab">Supplier Services</a></li>
                  <li><a href="#quality_assurance_tab" data-toggle="tab">Quality Assurance / Warranty</a></li>
                  <li><a href="#insurance_tab" data-toggle="tab">Insurance</a></li>
                  <li><a href="#trading_terms_tab" data-toggle="tab">Trading Terms</a></li>
                  <li><a href="#trade_references_tab" data-toggle="tab">Trade References</a></li>
                  <li><a href="#tender_invitation_tab" data-toggle="tab">Tender Invitations</a></li>
                </ul>
                <div class="tab-content">
                  <div class="active tab-pane" id="supplier_services_tab">
                      <div class="row">
                      <div class="col-sm-12 text-center hidden-text">
                          <h2 class="page-header">Supplier Services</h2>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="box box-solid">
                            <div class="box-header with-border">
                              <i class="fa fa-align-center"></i>

                              <h3 class="box-title">Category Services</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-sm-6 col-xs-12">  
                                    <ul>
                                          <?php 
                                          $row_count = 1;
                                          foreach($supplier_services_category as $row)
                                            {
                                                echo ($row !== '') ? '<li>'.$row.'</li>':'';
                                              if($row_count == 11){
                                                    echo '</div></ul><div class="col-sm-6"><ul>';
                                                }
                                              $row_count++;
                                            }
                                            echo ($supplier_services->category_of_services_others !== '') ? '<li>'.$supplier_services->category_of_services_others.'</li>':'';
                                          ?>
                                      </ul>
                                </div>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /.box -->
                        </div>
                        
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                  <b>Details of services:</b><br/>&nbsp;<?php echo $supplier_services->details_of_service; ?> 
                                </li>
                                <li class="list-group-item">
                                  <b>Expertise / Certification:</b><br/>&nbsp;<?php echo $supplier_services->expertise_certification; ?>
                                </li>
                                <li class="list-group-item">
                                  <b>Standard Quote turnaround time:</b><br/>&nbsp;<?php echo $supplier_services->standard_quote_turnaround_time; ?>
                                </li>
                                <li class="list-group-item">
                                  <b>Standard Manufacturing supply lead time:</b><br/>&nbsp;<?php echo $supplier_services->standard_manufacturing_supply_lead_time; ?>
                                </li>
                              </ul>
                        </div>
                        </div>
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="quality_assurance_tab">
                      <div class="row">
                        <div class="col-sm-12 text-center hidden-text">
                          <h2 class="page-header-2">Quality Assurance / Warranty</h2>
                          </div>
                        <div class="col-sm-6 col-xs-12">
                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                  <b>Specifics of warranty offered on goods / services:</b><br/>&nbsp;<?php echo $supplier_quality_assurance->specific_of_warranty_offered_on_goods_services; ?> 
                                </li>
                                <li class="list-group-item">
                                  <b>Quality Assurance Procedure:</b><br/>&nbsp;<?php echo $supplier_quality_assurance->quality_assurance_procedure; ?>
                                </li>
                              </ul>
                          </div>
                          <div class="col-sm-6 col-xs-12">
                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>Details of QA procedure / policy:</b><br/>&nbsp;<?php echo $supplier_quality_assurance->details_of_qa_procedure_policy; ?>
                                </li>
                                <li class="list-group-item">
                                    <b>Attachments:</b><br/>
                                        <?php if($supplier_quality_assurance->details_of_qa_procedure_policy_attachments !=NULL){
                                        ?>
                                           <a href="<?php echo base_url('assets/attachments/quality assurance/'.$supplier_quality_assurance->details_of_qa_procedure_policy_attachments); ?>" <?php echo (substr(strrchr($supplier_quality_assurance->details_of_qa_procedure_policy_attachments, "."), 1) == 'pdf') ? 'target="_blank"' : 'class="fancybox"' ?> >&nbsp;<i class="fa fa-expand"></i>&nbsp;View</a>
                                        <?php
                                        }else{ echo "NA"; } ?>
                                </li>
                            </ul>
                        </div>
                      </div>
                    
                  </div>
                  <!-- /.tab-pane -->

                  <div class="tab-pane" id="insurance_tab">
                    <div class="row">
                    <div class="col-sm-12 text-center hidden-text">
                      <h2 class="page-header-2">Insurance</h2>
                      </div>
                    <div class="col-xs-12">
                      <div class="table-responsive">
                        <table class="table table-bordered table-striped dataTable">
                            <thead>
                                <th>Category</th>
                                <th>Name of Insurer</th>
                                <th>Limit of Indemnity</th>
                                <th>Period of Cover</th>
                                <th>Policy Number</th>
                                <th>Excess</th>
                                <th>Attachment</th>
                            </thead> 
                        <tbody>
                        <?php foreach($supplier_insurance as $row)
                            { $file_location = base_url('assets/attachments/quality assurance/'.$row->attachments); ?>
                            <tr>
                                <td><?php echo $row->insurance_type; ?></td>
                                <td><?php echo $row->insurer; ?></td>
                                <td><?php echo $row->indemnity; ?></td>
                                <td><?php echo $row->period_cover; ?></td>
                                <td><?php echo $row->attachments; ?></td>
                                <td><?php echo $row->excess; ?></td>
                                <td>
                                    <?php if($row->attachments !==NULL){
                                ?>
                                    <a href="<?php echo $file_location; ?>" <?php echo (substr(strrchr($row->attachments, "."), 1) == 'pdf') ? 'target="_blank"' : 'class="fancybox"' ?> >&nbsp;<i class="fa fa-expand"></i>&nbsp;View</a>
                                <?php
                                }else{ echo "NA"; } ?>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody></table>
                      </div>
                    </div>
                    
                    
                    </div>
                  </div>
                
                <div class="tab-pane" id="trading_terms_tab">
                    <div class="row">
                    <div class="col-sm-12 text-center hidden-text">
                    <h2 class="page-header-2">Trading Terms</h2>
                    </div>
                      <div class="col-sm-6 col-xs-12">
                          <div class="box box-solid">
                            <div class="box-header with-border">
                              <i class="fa fa-calendar-check-o"></i>

                            <h3 class="box-title">Payment Terms</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-sm-6 col-xs-12">  
                                    <ul>
                                          <?php 
                                          foreach($trading_terms_payment_terms as $row)
                                            {
                                                echo ($row !== '') ? '<li>'.$row.'&nbsp(End of Month)</li>':'';
                                            }
                                            echo ($trading_terms->payment_terms_others !=='') ? '<li>'.$trading_terms->payment_terms_others.'</li>':'';
                                          ?>
                                      </ul>
                                </div>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /.box -->
                        </div>
                        <div class="col-sm-6 col-xs-12">
                          <div class="box box-solid">
                            <div class="box-header with-border">
                              <i class="fa fa-cc-mastercard"></i>

                            <h3 class="box-title">Credit Card And Charges</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="col-sm-6 col-xs-12">  
                                    <ul>
                                          <?php 
                                          foreach($trading_terms_credit_card_charges as $row)
                                            {
                                                echo ($supplier_services->category_of_services_others !== '') ? '<li>'.$row.'</li>':'';
                                            }
                                                echo ($trading_terms->credit_cards_charges_others !== '') ? '<li>'.$trading_terms->credit_cards_charges_others.'</li>':'';
                                          ?>
                                      </ul>
                                </div>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /.box -->
                        </div>
                        <div class="col-sm-6 col-xs-12">
                          <div class="box box-solid">
                            <div class="box-header with-border">
                              <i class="fa fa-newspaper-o"></i>

                            <h3 class="box-title">Bank&nbsp;Account&nbsp;Details</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>BSB</th>
                                            <th>Acct</th>
                                            <th>Financial Institution</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $trading_terms->bank_account_details_bsb; ?></td>
                                            <td><?php echo $trading_terms->bank_account_details_acct; ?></td>
                                            <td><?php echo $trading_terms->bank_account_details_financial_institution; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /.box -->
                        </div>
                        <div class="col-sm-6 col-xs-12">
                          <div class="box box-solid">
                            <div class="box-header with-border">
                              <i class="fa fa-window-restore"></i>

                            <h3 class="box-title">Accounting&nbsp;System /&nbsp;Software</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <ul>
                                    <?php echo ($trading_terms->accounting_system_software !== '') ? "<li>".$trading_terms->accounting_system_software."</li>":'' ?>
                                </ul>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /.box -->
                        </div>
                    </div>
                  </div>
                  <div class="tab-pane" id="trade_references_tab">
                        <div class="row">
                        <div class="col-sm-12 text-center hidden-text">
                        <h2 class="page-header-2">Trade References</h2>
                        </div>
                            <div class="col-sm-12 col-xs-12">
                            <table class="table table-bordered table-striped dataTable">
                                <thead>
                                    <tr>
                                        <th>Company</th>
                                        <th>Contact Name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach($trading_references as $row){
                                    ?>
                                        <tr>
                                            <td><?php echo $row->company; ?></td>
                                            <td><?php echo $row->contact_name; ?></td>
                                            <td><?php echo $row->phone; ?></td>
                                            <td><?php echo $row->email; ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>    
                            </table>
                            </div>
                        </div>
                  </div>
                    
                    <div class="tab-pane" id="tender_invitation_tab">
                      <div class="row">
                      <div class="col-sm-12 text-center hidden-text">
                          <h2 class="page-header">Tender Invitation</h2>
                      </div>
                      <div class="row">
                          <div class="col-sm-12 ">
                          <div class="col-sm-12 table-responsive">
                            <table class="table table-bordered table-striped dataTable" id="example2">
                                <thead>
                                    <tr>
                                        <th>Project Name</th>
                                        <th>Sub Tasks</th>
                                        <th>Invitation Date</th>
                                        <th>Expiration Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
    //                                var_dump($tender_invitations);
                                    foreach($tender_invitations as $row){ ?>
                                    <tr>
                                        <td><?php echo $row->project_name[0]; ?></td>
                                        <td>
                                            <ul>
                                                <?php foreach($row->sub_task_name as $row2){ ?>
                                                    <li><?php echo $row2; ?></li>
                                                <?php } ?>
                                            </ul>
                                        </td>
                                        <td><span class="hidden"><?php echo date('Ymd',strtotime($row->date_submitted)) ?></span><?php echo date('M d, Y',strtotime($row->date_submitted)) ?></td>
                                        <td><span class="hidden"><?php echo date('Ymd',strtotime($row->expiration_date)) ?></span><?php echo date('M d, Y',strtotime($row->expiration_date)) ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>  
                          </div>
                      </div>
                      </div>
                        
                      </div>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div>
              <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->
          </div>
    </div>
      <!-- /.row -->

</section>
</div>

<div class="modal fade" id="modal-default" style="display: none;">
    <form action="<?php echo base_url('Admin/send_invitation') ?>" method="post" id="frm_tender_invitation" enctype="multipart/form-data">
          <div class="modal-dialog modal-lg" style="width:90vw;">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Send Tender Invitation</h4>
              </div>
              <div class="modal-body" id="tender_invitation_modal">
                <label>Project Name:</label><br/>
                  <span class="text-danger hidden-text" id="notification_select_project">Please Select Project <?php echo form_error('project_id'); ?></span>
                  <select class="form-control" id="select_project" name="project_id">
                    <option value="" disabled selected>Select Project</option>
                      <?php 
                        foreach($project_lists as $row)
                        {
                            ?>
                            <option value="<?php echo $row->autonum ?>"><?php echo $row->project_name; ?></option>
                            <?php
                        } 
                      ?>
                  </select>
                  <div id="task_lists">
                  
                  </div>
                  <div>
                    <label>Expiration Date:</label><br/>
                      <span class="text-danger hidden-text" id="notification_expiration_date">Select invitation expiration date. <?php echo form_error('expiration_date'); ?></span>
                      <input class="form-control datepicker " name="expiration_date" id="expiration_date"/>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" id="btn_send_tender_invitation" class="btn btn-primary">Send Invitation</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
        <input type="hidden" name="supplier_id[]" value="'<?php echo md5(md5(md5($supplier_company_details->autonum))) ?>'" />
        <input type="hidden" name="after_submit" value="Admin/view_supplier/<?php echo $supplier_company_details->autonum ?>" />
        
    </form>
          <!-- /.modal-dialog -->
</div>
