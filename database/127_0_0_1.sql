-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 28, 2017 at 07:49 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prototype_tender_module_db`
--
CREATE DATABASE IF NOT EXISTS `prototype_tender_module_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `prototype_tender_module_db`;

-- --------------------------------------------------------

--
-- Table structure for table `prototype_access_type`
--

CREATE TABLE `prototype_access_type` (
  `autonum` int(11) NOT NULL,
  `access_name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prototype_projects`
--

CREATE TABLE `prototype_projects` (
  `autonum` int(11) NOT NULL,
  `project_name` varchar(250) DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prototype_projects`
--

INSERT INTO `prototype_projects` (`autonum`, `project_name`, `status`) VALUES
(1, 'Prototype 1st project', 0),
(2, 'Prortotype 2nd project', 0),
(3, 'Prortotype 3rd project', 0),
(4, 'Prortotype 4th project', 0);

-- --------------------------------------------------------

--
-- Table structure for table `prototype_projects_sub`
--

CREATE TABLE `prototype_projects_sub` (
  `autonum` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `project_name` varchar(250) NOT NULL,
  `project_description` varchar(500) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prototype_quotations`
--

CREATE TABLE `prototype_quotations` (
  `autonum` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `contact_name` varchar(150) NOT NULL,
  `contact_number` varchar(150) NOT NULL,
  `contact_email` varchar(150) NOT NULL,
  `freight_stat` int(11) DEFAULT NULL,
  `freight_explanation` varchar(150) DEFAULT NULL,
  `payment_method` varchar(150) DEFAULT NULL,
  `payment_terms` varchar(150) DEFAULT NULL,
  `payment_details` varchar(150) DEFAULT NULL,
  `payment_attachment` varchar(150) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creted_by_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prototype_quotations`
--

INSERT INTO `prototype_quotations` (`autonum`, `supplier_id`, `project_id`, `contact_name`, `contact_number`, `contact_email`, `freight_stat`, `freight_explanation`, `payment_method`, `payment_terms`, `payment_details`, `payment_attachment`, `status`, `date_created`, `creted_by_id`) VALUES
(1, 2, 1, 'jano', '0876556546', 'Jano@gmail.com', 1, 'test', 'On Account', 'test', 'test', 'payment_attachment_24.pdf', 0, '2017-12-28 04:57:13', 1),
(2, 2, 2, 'Chot Vallejos', '092216516', 'chot@gmail.com', 0, 'freight', 'Credit Card', 'payment terms', 'Payment details', 'payment_attachment_21.pdf', 0, '2017-12-28 01:10:18', 1),
(3, 2, 1, 'Ian Marcelo', '091324654', 'ian@gmail.com', 1, 'freight remarks', 'Cash', 'payment terms', 'payment details', 'payment_attachment_25.pdf', 0, '2017-12-28 05:38:45', 1);

-- --------------------------------------------------------

--
-- Table structure for table `prototype_quotations_product_lists`
--

CREATE TABLE `prototype_quotations_product_lists` (
  `autonum` int(11) NOT NULL,
  `quotation_id` int(11) NOT NULL,
  `product_name` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `unit_price` float NOT NULL,
  `qty` int(11) NOT NULL,
  `unit` varchar(50) NOT NULL,
  `gst` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prototype_quotations_product_lists`
--

INSERT INTO `prototype_quotations_product_lists` (`autonum`, `quotation_id`, `product_name`, `description`, `unit_price`, `qty`, `unit`, `gst`) VALUES
(4, 2, 'product name 1', 'description 1', 17, 7, 'box', 11.9),
(5, 2, 'product name 2', 'description 2', 16, 3, 'pcs', 0),
(6, 2, 'product name 3', 'description 3', 4, 4, 'hours', 1.6),
(12, 1, 'product name 1', 'description 1', 7, 3, 'box', 2.1),
(13, 1, 'product name 2', 'description 2', 4, 2, 'pcs', 0.8),
(14, 3, 'product name 1', 'description 1', 117, 13, 'box', 152.1),
(15, 3, 'product name 2', 'description 2', 217, 9, 'pcs', 195.3),
(16, 3, 'product name 3', 'description 3', 317, 13, 'set', 412.1);

-- --------------------------------------------------------

--
-- Table structure for table `prototype_quotations_status`
--

CREATE TABLE `prototype_quotations_status` (
  `autonum` int(11) NOT NULL,
  `status_name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prototype_quotations_status`
--

INSERT INTO `prototype_quotations_status` (`autonum`, `status_name`) VALUES
(0, '<span class=\"text-blue\">Submitted</span>'),
(1, '<span class=\"text-orange\">Draft</span>'),
(2, '&nbsp;'),
(3, '<span class=\"text-aqua\">In Review</span>'),
(4, '<span class=\"text-green\">Successful</span>'),
(5, '<span class=\"text-red\">Unsuccessful</span>');

-- --------------------------------------------------------

--
-- Table structure for table `prototype_quotations_submitted`
--

CREATE TABLE `prototype_quotations_submitted` (
  `autonum` int(11) NOT NULL,
  `quotation_id` int(11) NOT NULL,
  `date_submitted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expiration_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prototype_quotations_submitted`
--

INSERT INTO `prototype_quotations_submitted` (`autonum`, `quotation_id`, `date_submitted`, `expiration_date`) VALUES
(1, 1, '2017-12-28 00:42:31', '2018-01-07 08:42:31'),
(2, 2, '2017-12-28 01:09:07', '2018-01-07 09:09:07'),
(4, 3, '2017-12-28 05:38:45', '2018-01-07 13:38:45');

-- --------------------------------------------------------

--
-- Table structure for table `prototype_suppliers_info`
--

CREATE TABLE `prototype_suppliers_info` (
  `autonum` int(11) NOT NULL,
  `company_name` varchar(150) DEFAULT NULL,
  `abn_acn` varchar(150) DEFAULT NULL,
  `street_address` varchar(500) DEFAULT NULL,
  `postal_address` varchar(150) DEFAULT NULL,
  `phone_number_sales` varchar(50) DEFAULT NULL,
  `phone_number_accounts` varchar(50) DEFAULT NULL,
  `email_address_sales` varchar(50) DEFAULT NULL,
  `email_address_accounts` varchar(50) DEFAULT NULL,
  `website` varchar(50) DEFAULT NULL,
  `account_manager_name` varchar(50) DEFAULT NULL,
  `years_in_operation` varchar(50) DEFAULT NULL,
  `number_of_employees` varchar(50) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `creator_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prototype_suppliers_info`
--

INSERT INTO `prototype_suppliers_info` (`autonum`, `company_name`, `abn_acn`, `street_address`, `postal_address`, `phone_number_sales`, `phone_number_accounts`, `email_address_sales`, `email_address_accounts`, `website`, `account_manager_name`, `years_in_operation`, `number_of_employees`, `status`, `date_created`, `creator_id`) VALUES
(1, 'RBR Group', 'ABN', 'street address', 'postal address', 'phone sale', 'phone account', 'email sales', 'email account', 'website', 'account manager', 'years operation', 'number of employee', 0, '2017-12-26 05:45:30', 0),
(2, 'RBR Corporation', 'a', 'address', 'postal address', '471323456', '47165465', 'asdf@gmail.com', 'fsdf@test.com', 'website.come', 'Richard Ramirez', '10', '150', 0, '2017-12-28 05:48:10', 0),
(3, 'Chot Vallejos', 'ABN ', 'Molino Bacoor ', 'postal address ', 'phone sale ', 'phone account 2', 'email sales ', 'email account 2', 'website ', 'Chot Vallejos ', '5 ', '150 ', 1, '2017-12-26 01:46:47', 0),
(4, 'test company', '', '', '', '', '', '', '', '', '', '', '', 3, '2017-12-22 07:46:16', 0),
(5, 'Sama', '', '', '', '', '', '', '', '', '', '', '', 3, '2017-12-22 07:46:16', 0),
(6, 'Samasdf', '', '', '', '', '', '', '', '', '', '', '', 3, '2017-12-22 07:46:16', 0),
(7, 'Samasdfsdf', '', '', '', '', '', '', '', '', '', '', '', 3, '2017-12-22 07:46:16', 0),
(8, 'Samasdfsdf', '', '', '', '', '', '', '', '', '', '', '', 3, '2017-12-22 07:46:16', 0),
(9, 'Samasdfsdf', '', '', '', '', '', '', '', '', '', '', '', 3, '2017-12-22 07:46:16', 0),
(10, 'Samasdfsdf', '', '', '', '', '', '', '', '', '', '', '', 3, '2017-12-22 07:46:16', 0),
(11, 'Samasdfsdf', '', '', '', '', '', '', '', '', '', '', '', 3, '2017-12-22 07:47:05', 0),
(12, 'fill company name', '', '', '', '', '', '', '', '', '', '', '', 3, '2017-12-22 07:54:34', 0),
(13, 'xcvz', '', '', '', '', '', '', '', '', '', '', '', 3, '2017-12-22 07:47:24', 0),
(14, 'mmmm', '', '', '', '', '', '', '', '', '', '', '', 3, '2017-12-22 07:54:18', 0),
(15, 'Shore Solutions', 'ABN', 'street address2', 'postal address', 'phone sale2', 'phone account2', 'email sales', 'email account2', 'website', 'account manager', 'years operation', 'number of employee', 0, '2017-12-27 07:27:53', 0),
(16, 'dfxghkj', '', '', '', '', '', '', '', '', '', '', '', 3, '2017-12-22 07:54:34', 0),
(17, 'test validation', '', '', '', '', '', '', '', '', '', '', '', 3, '2017-12-22 07:54:34', 0),
(18, 'Prototype', '123456789101', 'Autralia', 'postal address', '123', '645', '123', '546', 'prototype.net.au', 'account manager', '50', '150', 3, '2017-12-26 08:10:39', 0),
(19, 'te', '', '', '', '', '', '', '', '', '', '', '', 3, '2017-12-26 04:01:43', 0),
(20, 'test', '', '', '', '', '', '', '', '', '', '', '', 3, '2017-12-26 04:13:40', 0),
(21, 'RBR Group', 'ABN', 'street address', 'postal address', 'phone sale', 'phone account', '', '', '', '', '', '', 3, '2017-12-26 05:45:49', 0),
(22, 'RBR Group', 'ABN', 'street address', 'postal address', 'phone sale', 'phone account', '', '', '', '', '', '', 3, '2017-12-26 05:45:49', 0);

-- --------------------------------------------------------

--
-- Table structure for table `prototype_suppliers_insurance`
--

CREATE TABLE `prototype_suppliers_insurance` (
  `autonum` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `insurance_type` varchar(500) DEFAULT NULL,
  `insurer` varchar(500) DEFAULT NULL,
  `number` varchar(500) DEFAULT NULL,
  `indemnity` varchar(500) DEFAULT NULL,
  `excess` varchar(500) DEFAULT NULL,
  `period_cover` varchar(500) DEFAULT NULL,
  `attachments` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prototype_suppliers_insurance`
--

INSERT INTO `prototype_suppliers_insurance` (`autonum`, `supplier_id`, `insurance_type`, `insurer`, `number`, `indemnity`, `excess`, `period_cover`, `attachments`) VALUES
(1, 1, 'Public Liability', 'insurer', 'pilicy', 'limit', 'excess', 'period', ''),
(2, 1, 'Professional Indemnity', 'insurer', 'policy', 'limit', 'excess', 'period', ''),
(3, 1, 'Workers Compensation', 'inusrer', 'policy', 'limit', 'excess', 'period', ''),
(4, 1, 'Marine Transit', 'insurer', 'policy', 'limit', 'excess', 'period', ''),
(5, 2, 'Public Liability', 'awerawe', 'awer', 'aweraw', 'erawer', 'awer', '2_attachment_1.jpg'),
(6, 2, 'Professional Indemnity', 'asdfasdf', 'asdf', 'asdfas', 'dfasdf', 'fasdf', ''),
(7, 2, 'Workers Compensation', 'asdfsadf', 'sadfasdf', 'asdfasdf', 'asdfasdf', 'asdfasdf', ''),
(8, 2, 'Marine Transit', 'fasdfa', 'asdfasd', 'sdfasdf', 'asdf', 'asdfasdf', ''),
(9, 3, 'Public Liability', 'test', 'test', 'test', 'test', 'test', '3_attachment_.jpg'),
(10, 3, 'Professional Indemnity', 'test', 'test', 'test', 'test', 'test', '3_attachment_1.jpg'),
(11, 3, 'Workers Compensation', 'test', 'tets', 'test', 'test', 'ste', '3_attachment_2.jpg'),
(12, 3, 'Marine Transit', 'test', 'test', 'test', 'test', 'test', '3_attachment_3.jpg'),
(13, 4, 'Public Liability', '', '', '', '', '', NULL),
(14, 4, 'Professional Indemnity', '', '', '', '', '', NULL),
(15, 4, 'Workers Compensation', '', '', '', '', '', NULL),
(16, 4, 'Marine Transit', '', '', '', '', '', NULL),
(17, 5, 'Public Liability', '', '', '', '', '', NULL),
(18, 5, 'Professional Indemnity', '', '', '', '', '', NULL),
(19, 5, 'Workers Compensation', '', '', '', '', '', NULL),
(20, 5, 'Marine Transit', '', '', '', '', '', NULL),
(21, 6, 'Public Liability', '', '', '', '', '', NULL),
(22, 6, 'Professional Indemnity', '', '', '', '', '', NULL),
(23, 6, 'Workers Compensation', '', '', '', '', '', NULL),
(24, 6, 'Marine Transit', '', '', '', '', '', NULL),
(25, 7, 'Public Liability', '', '', '', '', '', NULL),
(26, 7, 'Professional Indemnity', '', '', '', '', '', NULL),
(27, 7, 'Workers Compensation', '', '', '', '', '', NULL),
(28, 7, 'Marine Transit', '', '', '', '', '', NULL),
(29, 8, 'Public Liability', '', '', '', '', '', NULL),
(30, 8, 'Professional Indemnity', '', '', '', '', '', NULL),
(31, 8, 'Workers Compensation', '', '', '', '', '', NULL),
(32, 8, 'Marine Transit', '', '', '', '', '', NULL),
(33, 9, 'Public Liability', '', '', '', '', '', NULL),
(34, 9, 'Professional Indemnity', '', '', '', '', '', NULL),
(35, 9, 'Workers Compensation', '', '', '', '', '', NULL),
(36, 9, 'Marine Transit', '', '', '', '', '', NULL),
(37, 10, 'Public Liability', '', '', '', '', '', '10_quality_procedure1.jpg'),
(38, 10, 'Professional Indemnity', '', '', '', '', '', NULL),
(39, 10, 'Workers Compensation', '', '', '', '', '', NULL),
(40, 10, 'Marine Transit', '', '', '', '', '', NULL),
(41, 11, 'Public Liability', '', '', '', '', '', '11_quality_procedure1.jpg'),
(42, 11, 'Professional Indemnity', '', '', '', '', '', ''),
(43, 11, 'Workers Compensation', '', '', '', '', '', ''),
(44, 11, 'Marine Transit', '', '', '', '', '', ''),
(45, 12, 'Public Liability', '', '', '', '', '', ''),
(46, 12, 'Professional Indemnity', '', '', '', '', '', ''),
(47, 12, 'Workers Compensation', '', '', '', '', '', ''),
(48, 12, 'Marine Transit', '', '', '', '', '', ''),
(49, 13, 'Public Liability', '', '', '', '', '', ''),
(50, 13, 'Professional Indemnity', '', '', '', '', '', ''),
(51, 13, 'Workers Compensation', '', '', '', '', '', ''),
(52, 13, 'Marine Transit', '', '', '', '', '', ''),
(53, 14, 'Public Liability', '', '', '', '', '', ''),
(54, 14, 'Professional Indemnity', '', '', '', '', '', ''),
(55, 14, 'Workers Compensation', '', '', '', '', '', ''),
(56, 14, 'Marine Transit', '', '', '', '', '', ''),
(57, 15, 'Public Liability', 'insurer', 'pilicy', '122', 'excess', 'period', '15_quality_procedure1.jpg'),
(58, 15, 'Professional Indemnity', 'insurer', 'policy', '333', 'excess', 'period', '15_quality_procedure2.jpg'),
(59, 15, 'Workers Compensation', 'inusrer', 'policy', '444', 'excess', 'period', '15_quality_procedure3.jpg'),
(60, 15, 'Marine Transit', 'insurer', 'policy', '55', 'excess', 'periods', '15_quality_procedure4.jpg'),
(61, 16, 'Public Liability', '', '', '', '', '', ''),
(62, 16, 'Professional Indemnity', '', '', '', '', '', ''),
(63, 16, 'Workers Compensation', '', '', '', '', '', ''),
(64, 16, 'Marine Transit', '', '', '', '', '', ''),
(65, 17, 'Public Liability', '', '', '', '', '', ''),
(66, 17, 'Professional Indemnity', '', '', '', '', '', ''),
(67, 17, 'Workers Compensation', '', '', '', '', '', ''),
(68, 17, 'Marine Transit', '', '', '', '', '', ''),
(69, 18, 'Public Liability', 'insurer', 'pilicy', 'limit', 'excess', 'period', ''),
(70, 18, 'Professional Indemnity', '', '', '', '', '', ''),
(71, 18, 'Workers Compensation', '', '', '', '', '', ''),
(72, 18, 'Marine Transit', '', '', '', '', '', ''),
(73, 19, 'Public Liability', '', '', '', '', '', ''),
(74, 19, 'Professional Indemnity', '', '', '', '', '', ''),
(75, 19, 'Workers Compensation', '', '', '', '', '', ''),
(76, 19, 'Marine Transit', '', '', '', '', '', ''),
(77, 20, 'Public Liability', '', '', '', '', '', ''),
(78, 20, 'Professional Indemnity', '', '', '', '', '', ''),
(79, 20, 'Workers Compensation', '', '', '', '', '', ''),
(80, 20, 'Marine Transit', '', '', '', '', '', ''),
(81, 21, 'Public Liability', '', '', '', '', '', ''),
(82, 21, 'Professional Indemnity', '', '', '', '', '', ''),
(83, 21, 'Workers Compensation', '', '', '', '', '', ''),
(84, 21, 'Marine Transit', '', '', '', '', '', ''),
(85, 22, 'Public Liability', '', '', '', '', '', ''),
(86, 22, 'Professional Indemnity', '', '', '', '', '', ''),
(87, 22, 'Workers Compensation', '', '', '', '', '', ''),
(88, 22, 'Marine Transit', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `prototype_suppliers_quality_assurance_warranty`
--

CREATE TABLE `prototype_suppliers_quality_assurance_warranty` (
  `autonum` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `specific_of_warranty_offered_on_goods_services` varchar(250) DEFAULT NULL,
  `quality_assurance_procedure` varchar(250) DEFAULT NULL,
  `details_of_qa_procedure_policy` varchar(250) DEFAULT NULL,
  `details_of_qa_procedure_policy_attachments` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prototype_suppliers_quality_assurance_warranty`
--

INSERT INTO `prototype_suppliers_quality_assurance_warranty` (`autonum`, `supplier_id`, `specific_of_warranty_offered_on_goods_services`, `quality_assurance_procedure`, `details_of_qa_procedure_policy`, `details_of_qa_procedure_policy_attachments`) VALUES
(1, 1, 'her particulars. Attach copy of any warranty document.', 'yes', 'Details of QA procedu', 'Specifics of warranty offered on '),
(2, 2, 'sfasdfsdf', 'yes', 'awerawerawer', 'awer'),
(3, 3, 'test', 'yes', 'test', 'test'),
(4, 4, NULL, 'yes', '', NULL),
(5, 5, NULL, 'yes', 's', NULL),
(6, 6, NULL, 'yes', 's', NULL),
(7, 7, NULL, 'yes', 's', NULL),
(8, 8, NULL, 'yes', 's', NULL),
(9, 9, NULL, 'yes', 's', NULL),
(10, 10, NULL, 'yes', 's', '10_quality_procedure.jpg'),
(11, 11, NULL, 'yes', 's', '11_quality_procedure.jpg'),
(12, 12, NULL, 'yes', 'Outline details of QA procedure. Annex a page', '12_quality_procedure.jpg'),
(13, 13, NULL, 'yes', 'asdf', '13_quality_procedure.jpg'),
(14, 14, 'her particulars. Attach copy of any warranty document.', 'yes', 'tea', '14_quality_procedure.jpg'),
(15, 15, 'Specifics', 'yes', 'Details', 'Standard Manufacturing/ supply lead time:'),
(16, 16, '', 'yes', 'dfhgj', '16_attachment_.pdf'),
(17, 17, '', NULL, '', ''),
(18, 18, '1 year', 'yes', 'test', '18_attachment_.pdf'),
(19, 19, '', NULL, '', ''),
(20, 20, '', NULL, '', ''),
(21, 21, '', NULL, '', ''),
(22, 22, '', NULL, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `prototype_suppliers_services`
--

CREATE TABLE `prototype_suppliers_services` (
  `autonum` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `category_of_services` varchar(500) DEFAULT NULL,
  `category_of_services_others` varchar(150) DEFAULT NULL,
  `details_of_service` varchar(500) DEFAULT NULL,
  `expertise_certification` varchar(500) DEFAULT NULL,
  `delivery_stat` varchar(5) DEFAULT NULL,
  `delivery_stat_known` varchar(250) DEFAULT NULL,
  `standard_quote_turnaround_time` varchar(250) DEFAULT NULL,
  `standard_manufacturing_supply_lead_time` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prototype_suppliers_services`
--

INSERT INTO `prototype_suppliers_services` (`autonum`, `supplier_id`, `category_of_services`, `category_of_services_others`, `details_of_service`, `expertise_certification`, `delivery_stat`, `delivery_stat_known`, `standard_quote_turnaround_time`, `standard_manufacturing_supply_lead_time`) VALUES
(1, 1, 'Timber / Joinert,Powder / Coating,Upholstery and Framing,Wholesaler,Shop fitting,Tiler,Electroplating,CAD,Compact Laminate,Labour,Freight,Metal Fabrication,Upholstery,Furniture,Painting Staining and 2pac,Stone Mason,Fabric Supplier,Electrical,Water proofing,Installation and Rigging,Warehousing Storage', 'Other Category', 'detail', 'expertice', '1', 'please', 'standard', 'Specifics of warranty offered on '),
(2, 2, 'Timber / Joinert,Powder / Coating,Upholstery and Framing,Wholesaler,Shop fitting,Tiler,Electroplating,CAD,Compact Laminate,Labour,Freight,Metal Fabrication,Upholstery,Furniture,Painting Staining and 2pac,Stone Mason,Fabric Supplier,Electrical,Water proofing,Installation and Rigging,Warehousing Storage', '', 'asawer', 'asdfawer', '1', 'werawer', 'awerawer', 'awer'),
(3, 3, 'Timber / Joinert,Powder / Coating,Upholstery and Framing,Wholesaler,Shop fitting,Tiler,Electroplating,CAD,Compact Laminate,Labour,Freight,Metal Fabrication,Upholstery,Furniture,Painting Staining and 2pac,Stone Mason,Fabric Supplier,Electrical,Water proofing,Installation and Rigging,Warehousing Storage', 'test', 'test', 'test', '1', 'test', 'test', 'test'),
(4, 4, '', '', '', '', NULL, '', '', ''),
(5, 5, '', '', '', '', NULL, '', '', ''),
(6, 6, '', '', '', '', NULL, '', '', ''),
(7, 7, '', '', '', '', NULL, '', '', ''),
(8, 8, '', '', '', '', NULL, '', '', ''),
(9, 9, '', '', '', '', NULL, '', '', ''),
(10, 10, '', '', '', '', NULL, '', '', ''),
(11, 11, '', '', '', '', NULL, '', '', ''),
(12, 12, 'Timber / Joinert,Powder / Coating,Upholstery and Framing,Wholesaler,Shop fitting,Tiler,Electroplating,CAD,Compact Laminate,Labour,Freight,Metal Fabrication,Upholstery,Furniture,Painting Staining and 2pac,Stone Mason,Fabric Supplier,Electrical,Water proofing,Installation and Rigging,Warehousing Storage', '', '', '', '1', '', '', 'pecifics of warranty offered on good/services:'),
(13, 13, '', '', '', '', NULL, '', '', 'ngth of time and other particulars. Attach copy of any warranty d'),
(14, 14, '', '', '', '', NULL, '', '', ''),
(15, 15, 'Timber / Joinert,Powder / Coating,Upholstery and Framing,Wholesaler,Shop fitting,Tiler,Electroplating,CAD,Compact Laminate,Labour,Freight,Metal Fabrication,Upholstery,Furniture,Painting Staining and 2pac,Stone Mason,Fabric Supplier,Electrical,Water proofing,Installation and Rigging,Warehousing Storage', 'Other Category', 'Detail', 'Expertise', '1', 'specify', 'Standard Quote turnaround/ time:', 'Standard Manufacturing/ supply lead time:'),
(16, 16, '', '', '', '', NULL, '', '', ''),
(17, 17, '', '', '', '', NULL, '', '', ''),
(18, 18, 'Timber / Joinert,Powder / Coating,Upholstery and Framing,Wholesaler,Shop fitting,Tiler,Electroplating,CAD,Compact Laminate,Labour,Freight,Metal Fabrication,Upholstery,Furniture,Painting Staining and 2pac,Stone Mason,Fabric Supplier,Electrical,Water proofing,Installation and Rigging,Warehousing Storage', 'Other Category', 'detail', 'expertise', '1', 'delivert', '1 week', '8 weeks'),
(19, 19, '', '', '', '', NULL, '', '', ''),
(20, 20, '', '', '', '', NULL, '', '', ''),
(21, 21, 'Timber / Joinert,Powder / Coating,Upholstery and Framing,Wholesaler,Shop fitting,Tiler,Electroplating,CAD,Compact Laminate,Labour,Freight,Metal Fabrication,Upholstery,Furniture,Painting Staining and 2pac,Stone Mason,Fabric Supplier,Electrical,Water proofing,Installation and Rigging,Warehousing Storage', 'test', '', '', NULL, '', '', ''),
(22, 22, '', '', '', '', NULL, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `prototype_suppliers_trade_references`
--

CREATE TABLE `prototype_suppliers_trade_references` (
  `autonum` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `reference_no` varchar(500) DEFAULT NULL,
  `company` varchar(500) DEFAULT NULL,
  `contact_name` varchar(500) DEFAULT NULL,
  `phone` varchar(500) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prototype_suppliers_trade_references`
--

INSERT INTO `prototype_suppliers_trade_references` (`autonum`, `supplier_id`, `reference_no`, `company`, `contact_name`, `phone`, `email`) VALUES
(1, 1, '1', 'company', 'contant name', 'phone', 'email'),
(2, 1, '2', 'company', 'contact name', 'phone', 'email'),
(3, 1, '3', 'company', 'contact name', 'phone ', 'email'),
(4, 2, '1', 'aweraw', 'erawraw', 'erawrawer', 'awerawer'),
(5, 2, '2', 'aweraw', 'erawer', 'aewrawer', 'aweraweraw'),
(6, 2, '3', 'erawerawer', 'awer', 'awerawer', 'awerawe'),
(7, 3, '1', 'test', 'test', 'test', 'test'),
(8, 3, '2', 'test', 'test', 'stest', 'tset'),
(9, 3, '3', 'set', 'ste', 'ste', 'st'),
(10, 4, '1', '', '', '', ''),
(11, 4, '2', '', '', '', ''),
(12, 4, '3', '', '', '', ''),
(13, 5, '1', '', '', '', ''),
(14, 5, '2', '', '', '', ''),
(15, 5, '3', '', '', '', ''),
(16, 6, '1', '', '', '', ''),
(17, 6, '2', '', '', '', ''),
(18, 6, '3', '', '', '', ''),
(19, 7, '1', '', '', '', ''),
(20, 7, '2', '', '', '', ''),
(21, 7, '3', '', '', '', ''),
(22, 8, '1', '', '', '', ''),
(23, 8, '2', '', '', '', ''),
(24, 8, '3', '', '', '', ''),
(25, 9, '1', '', '', '', ''),
(26, 9, '2', '', '', '', ''),
(27, 9, '3', '', '', '', ''),
(28, 10, '1', '', '', '', ''),
(29, 10, '2', '', '', '', ''),
(30, 10, '3', '', '', '', ''),
(31, 11, '1', '', '', '', ''),
(32, 11, '2', '', '', '', ''),
(33, 11, '3', '', '', '', ''),
(34, 12, '1', '', '', '', ''),
(35, 12, '2', '', '', '', ''),
(36, 12, '3', '', '', '', ''),
(37, 13, '1', '', '', '', ''),
(38, 13, '2', '', '', '', ''),
(39, 13, '3', '', '', '', ''),
(40, 14, '1', '', '', '', ''),
(41, 14, '2', '', '', '', ''),
(42, 14, '3', '', '', '', ''),
(43, 15, '1', 'company', 'contant name', 'phone', 'email'),
(44, 15, '2', 'company', 'contact name', 'phone', 'email'),
(45, 15, '3', 'company', 'contact name', 'phone', 'email'),
(46, 16, '1', '', '', '', ''),
(47, 16, '2', '', '', '', ''),
(48, 16, '3', '', '', '', ''),
(49, 17, '1', '', '', '', ''),
(50, 17, '2', '', '', '', ''),
(51, 17, '3', '', '', '', ''),
(52, 18, '1', '', '', '', ''),
(53, 18, '2', '', '', '', ''),
(54, 18, '3', '', '', '', ''),
(55, 19, '1', '', '', '', ''),
(56, 19, '2', '', '', '', ''),
(57, 19, '3', '', '', '', ''),
(58, 20, '1', '', '', '', ''),
(59, 20, '2', '', '', '', ''),
(60, 20, '3', '', '', '', ''),
(61, 21, '1', '', '', '', ''),
(62, 21, '2', '', '', '', ''),
(63, 21, '3', '', '', '', ''),
(64, 22, '1', '', '', '', ''),
(65, 22, '2', '', '', '', ''),
(66, 22, '3', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `prototype_suppliers_trading_terms`
--

CREATE TABLE `prototype_suppliers_trading_terms` (
  `autonum` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `payment_terms` varchar(150) DEFAULT NULL,
  `payment_terms_others` varchar(150) DEFAULT NULL,
  `credit_cards_charges` varchar(150) DEFAULT NULL,
  `credit_cards_charges_others` varchar(150) DEFAULT NULL,
  `bank_account_details_bsb` varchar(150) DEFAULT NULL,
  `bank_account_details_acct` varchar(150) DEFAULT NULL,
  `bank_account_details_financial_institution` varchar(150) DEFAULT NULL,
  `accounting_system_software` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prototype_suppliers_trading_terms`
--

INSERT INTO `prototype_suppliers_trading_terms` (`autonum`, `supplier_id`, `payment_terms`, `payment_terms_others`, `credit_cards_charges`, `credit_cards_charges_others`, `bank_account_details_bsb`, `bank_account_details_acct`, `bank_account_details_financial_institution`, `accounting_system_software`) VALUES
(1, 1, '30 days,60 days,90 days', 'other payment terms', 'VISA,Amex,Mastercard', 'other credit card', 'bsb', 'acct', 'financiall', 'accounting system'),
(2, 2, '30 days,60 days,90 days', 'weraw', 'VISA,Amex,Mastercard', 'erawera', 'awerae', 'awerawer', 'wareaewr', 'awerawersdfsdf'),
(3, 3, '30 days,60 days,90 days', 'test', 'VISA,Amex,Mastercard', 'test', 'test', 'test', 'test', 'test'),
(4, 4, NULL, '', NULL, '', '', '', '', ''),
(5, 5, NULL, '', NULL, '', '', '', '', ''),
(6, 6, NULL, '', NULL, '', '', '', '', ''),
(7, 7, NULL, '', NULL, '', '', '', '', ''),
(8, 8, NULL, '', NULL, '', '', '', '', ''),
(9, 9, NULL, '', NULL, '', '', '', '', ''),
(10, 10, NULL, '', NULL, '', '', '', '', ''),
(11, 11, NULL, '', NULL, '', '', '', '', ''),
(12, 12, '30 days,60 days,90 days', '', 'VISA,Amex,Mastercard', '', '', '', '', ''),
(13, 13, NULL, '', NULL, '', '', '', '', ''),
(14, 14, NULL, '', NULL, '', '', '', '', ''),
(15, 15, '30 days,60 days,90 days', 'other payment terms', 'VISA,Amex,Mastercard', 'other credit card', 'bsb', 'acct', 'financiall', 'accounting system'),
(16, 16, NULL, '', NULL, '', '', '', '', ''),
(17, 17, NULL, '', NULL, '', '', '', '', ''),
(18, 18, NULL, '', NULL, '', '', '', '', ''),
(19, 19, NULL, '', NULL, '', '', '', '', ''),
(20, 20, NULL, '', NULL, '', '', '', '', ''),
(21, 21, NULL, '', NULL, '', '', '', '', ''),
(22, 22, NULL, '', NULL, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `prototype_users`
--

CREATE TABLE `prototype_users` (
  `autonum` int(11) NOT NULL,
  `user_name` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `middle_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `suffix_name` varchar(150) NOT NULL,
  `title_name` varchar(150) NOT NULL,
  `position` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `supplier_company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prototype_users`
--

INSERT INTO `prototype_users` (`autonum`, `user_name`, `password`, `first_name`, `middle_name`, `last_name`, `suffix_name`, `title_name`, `position`, `gender`, `supplier_company_id`) VALUES
(1, 'rbramirez', 'wew', 'Richard', 'Bautista', 'Ramirez', '', 'Mr.', 'Web & System Developer ', 'Male', 2);

-- --------------------------------------------------------

--
-- Table structure for table `prototype_user_access`
--

CREATE TABLE `prototype_user_access` (
  `autonum` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `access_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `prototype_access_type`
--
ALTER TABLE `prototype_access_type`
  ADD PRIMARY KEY (`autonum`);

--
-- Indexes for table `prototype_projects`
--
ALTER TABLE `prototype_projects`
  ADD PRIMARY KEY (`autonum`);

--
-- Indexes for table `prototype_projects_sub`
--
ALTER TABLE `prototype_projects_sub`
  ADD PRIMARY KEY (`autonum`);

--
-- Indexes for table `prototype_quotations`
--
ALTER TABLE `prototype_quotations`
  ADD PRIMARY KEY (`autonum`);

--
-- Indexes for table `prototype_quotations_product_lists`
--
ALTER TABLE `prototype_quotations_product_lists`
  ADD PRIMARY KEY (`autonum`),
  ADD KEY `quotation_id` (`quotation_id`);

--
-- Indexes for table `prototype_quotations_status`
--
ALTER TABLE `prototype_quotations_status`
  ADD PRIMARY KEY (`autonum`);

--
-- Indexes for table `prototype_quotations_submitted`
--
ALTER TABLE `prototype_quotations_submitted`
  ADD PRIMARY KEY (`autonum`),
  ADD KEY `quotation_id` (`quotation_id`);

--
-- Indexes for table `prototype_suppliers_info`
--
ALTER TABLE `prototype_suppliers_info`
  ADD PRIMARY KEY (`autonum`);

--
-- Indexes for table `prototype_suppliers_insurance`
--
ALTER TABLE `prototype_suppliers_insurance`
  ADD PRIMARY KEY (`autonum`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `prototype_suppliers_quality_assurance_warranty`
--
ALTER TABLE `prototype_suppliers_quality_assurance_warranty`
  ADD PRIMARY KEY (`autonum`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `prototype_suppliers_services`
--
ALTER TABLE `prototype_suppliers_services`
  ADD PRIMARY KEY (`autonum`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `prototype_suppliers_trade_references`
--
ALTER TABLE `prototype_suppliers_trade_references`
  ADD PRIMARY KEY (`autonum`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `prototype_suppliers_trading_terms`
--
ALTER TABLE `prototype_suppliers_trading_terms`
  ADD PRIMARY KEY (`autonum`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `prototype_users`
--
ALTER TABLE `prototype_users`
  ADD PRIMARY KEY (`autonum`),
  ADD KEY `supplier_company_id` (`supplier_company_id`);

--
-- Indexes for table `prototype_user_access`
--
ALTER TABLE `prototype_user_access`
  ADD PRIMARY KEY (`autonum`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `access_id` (`access_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `prototype_access_type`
--
ALTER TABLE `prototype_access_type`
  MODIFY `autonum` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `prototype_projects`
--
ALTER TABLE `prototype_projects`
  MODIFY `autonum` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `prototype_projects_sub`
--
ALTER TABLE `prototype_projects_sub`
  MODIFY `autonum` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `prototype_quotations`
--
ALTER TABLE `prototype_quotations`
  MODIFY `autonum` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `prototype_quotations_product_lists`
--
ALTER TABLE `prototype_quotations_product_lists`
  MODIFY `autonum` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `prototype_quotations_status`
--
ALTER TABLE `prototype_quotations_status`
  MODIFY `autonum` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `prototype_quotations_submitted`
--
ALTER TABLE `prototype_quotations_submitted`
  MODIFY `autonum` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `prototype_suppliers_info`
--
ALTER TABLE `prototype_suppliers_info`
  MODIFY `autonum` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `prototype_suppliers_insurance`
--
ALTER TABLE `prototype_suppliers_insurance`
  MODIFY `autonum` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `prototype_suppliers_quality_assurance_warranty`
--
ALTER TABLE `prototype_suppliers_quality_assurance_warranty`
  MODIFY `autonum` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `prototype_suppliers_services`
--
ALTER TABLE `prototype_suppliers_services`
  MODIFY `autonum` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `prototype_suppliers_trade_references`
--
ALTER TABLE `prototype_suppliers_trade_references`
  MODIFY `autonum` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `prototype_suppliers_trading_terms`
--
ALTER TABLE `prototype_suppliers_trading_terms`
  MODIFY `autonum` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `prototype_users`
--
ALTER TABLE `prototype_users`
  MODIFY `autonum` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `prototype_user_access`
--
ALTER TABLE `prototype_user_access`
  MODIFY `autonum` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `prototype_quotations_product_lists`
--
ALTER TABLE `prototype_quotations_product_lists`
  ADD CONSTRAINT `prototype_quotations_product_lists_ibfk_1` FOREIGN KEY (`quotation_id`) REFERENCES `prototype_quotations` (`autonum`);

--
-- Constraints for table `prototype_quotations_submitted`
--
ALTER TABLE `prototype_quotations_submitted`
  ADD CONSTRAINT `prototype_quotations_submitted_ibfk_1` FOREIGN KEY (`quotation_id`) REFERENCES `prototype_quotations` (`autonum`);

--
-- Constraints for table `prototype_suppliers_insurance`
--
ALTER TABLE `prototype_suppliers_insurance`
  ADD CONSTRAINT `prototype_suppliers_insurance_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `prototype_suppliers_info` (`autonum`);

--
-- Constraints for table `prototype_suppliers_quality_assurance_warranty`
--
ALTER TABLE `prototype_suppliers_quality_assurance_warranty`
  ADD CONSTRAINT `prototype_suppliers_quality_assurance_warranty_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `prototype_suppliers_info` (`autonum`);

--
-- Constraints for table `prototype_suppliers_services`
--
ALTER TABLE `prototype_suppliers_services`
  ADD CONSTRAINT `prototype_suppliers_services_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `prototype_suppliers_info` (`autonum`);

--
-- Constraints for table `prototype_suppliers_trade_references`
--
ALTER TABLE `prototype_suppliers_trade_references`
  ADD CONSTRAINT `prototype_suppliers_trade_references_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `prototype_suppliers_info` (`autonum`);

--
-- Constraints for table `prototype_suppliers_trading_terms`
--
ALTER TABLE `prototype_suppliers_trading_terms`
  ADD CONSTRAINT `prototype_suppliers_trading_terms_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `prototype_suppliers_info` (`autonum`);

--
-- Constraints for table `prototype_users`
--
ALTER TABLE `prototype_users`
  ADD CONSTRAINT `prototype_users_ibfk_1` FOREIGN KEY (`supplier_company_id`) REFERENCES `prototype_suppliers_info` (`autonum`);

--
-- Constraints for table `prototype_user_access`
--
ALTER TABLE `prototype_user_access`
  ADD CONSTRAINT `prototype_user_access_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `prototype_users` (`autonum`),
  ADD CONSTRAINT `prototype_user_access_ibfk_2` FOREIGN KEY (`access_id`) REFERENCES `prototype_access_type` (`autonum`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
