<?php
    $url  = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
    $url .= $_SERVER['SERVER_NAME'];
    $url .= htmlspecialchars($_SERVER['REQUEST_URI']);
    $themeurl = dirname(dirname($url)) . "";
    header('location:'.$themeurl); 
?>