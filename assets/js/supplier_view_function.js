//VIEW TABULAR OR ALL
$(document).on('change','.view_type',function()
               {
    var ths = $(this),
        ths_val = ths.val();
    if(ths_val == 1){
        $('.tab-pane').attr('class','tab-pane active');
        $('.tab-pane .hidden-text').show();
        $('#tab_supplier_navigation').hide()
    }else{
        $('.tab-pane').attr('class','tab-pane');
        $('.tab-pane .hidden-text').hide();
        $('#supplier_services_tab').attr('class','tab-pane active');
        $('#tab_supplier_navigation li').attr('class','');
        $('#tab_supplier_navigation li').first().attr('class','active');
        $('#tab_supplier_navigation').show();
    }
})

//INVITE TENDER LOAD
$(document).on('change','#select_project',function(){
    var ths = $(this),
        project_id = ths.val();
    $.post('../tender_project_list',{
        project_id : project_id
    }).done(function(data)
            {
                $('#task_lists').html(data);
            })
});

$(document).on('click','.tender_sub_task_checkbox',function(){
    $(this).prop("checked", !checkBoxes.prop("checked"));
})

$(document).on('click','#btn_send_tender_invitation',function(){
    var select_project = $('#select_project').val(),
        tender_sub_task_checkbox = $('.tender_sub_task_checkbox'),
        cnt = 0,
        expiration_date = $('#expiration_date').val();
    $(tender_sub_task_checkbox).each(function(){
        if($(this).prop('checked')){
            cnt++;
        }
    })
    
    if(select_project !== null && expiration_date !== '' && cnt !=0 ){
        bootbox.confirm({
            title: "Confirm",
            message: "Are you sure you want to send invitation?.",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm',
                }
            },
            callback: function (result) {
                if(result){
                    $('#frm_tender_invitation').submit();
                    bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Sending Invitation. please wait...</div>',
                                     closeButton: false
                                   })
                }
            }
        });
    }else{
        if(select_project === null){
            $('#notification_select_project').show();
        }else{ $('#notification_select_project').hide(); }
        if(expiration_date === ''){
            $('#notification_expiration_date').show();
        }else{ $('#notification_expiration_date').hide(); }
        if(cnt == 0){
            $('#notification_task_lists').show();
        }else{ $('#notification_task_lists').hide(); }
    }
})
