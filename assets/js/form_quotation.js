
$('.form_add_quotation_btn').click(function(){
    var ths = $(this),
        vl = ths.data('vl'),
        form_stat = ths.val();
    
    $('#form_quotation_submit_btn').val(form_stat);
    $('#confirm-delete').find('.modal-body').text("Are you sure you want to "+vl+" this quotation?");
});

$('#btn_add_products').click(function(){
    var tbl_id = $('#tbl_product_quotation'),
        parent_tr = tbl_id.find('tbody tr:first').clone();
        parent_tr.find('input').val('');
        parent_tr.find('.total_incl_gst').html('');
        parent_tr.find('.inpt_unit_price').val(0);
        parent_tr.find('.inpt_unit').text('');
        parent_tr.find('.product_amt').text('');
        parent_tr.find('.inpt_qty').val('');
        parent_tr.find('.inpt_gst').val('');
        parent_tr.find('.product_description').html('<textarea name="description[]" class="form-control"></textarea>');
        parent_tr.find('.inpt_gst').attr('placeholder','0.00');
    $('#tbl_product_quotation tbody').append(parent_tr);
});

$(document).on('change','.inpt_unit_price, .inpt_qty, .inpt_gst',function(){
    var ths_row = $(this).parent().parent(),
        unit_price = ths_row.find('.inpt_unit_price').val().replace(/[^0-9\.]+/g, ''),
        unit_qty = Number(ths_row.find('.inpt_qty').val()),
        inpt_gst = ths_row.find('.inpt_gst'),
        product_amt = ths_row.find('.product_amt'),
        product_amt_computation = Number(unit_price)*unit_qty,
        gst_total_place_holder = product_amt_computation*Number(.10),
        gst_total_incld = product_amt_computation+Number(inpt_gst.val().replace(/[^0-9\.]+/g, ''));
    inpt_gst.attr('placeholder',gst_total_place_holder).formatCurrency();
    product_amt.text(product_amt_computation).formatCurrency();
    ths_row.find('.total_incl_gst').text(gst_total_incld).formatCurrency();
    compute_products();
    
        
})

function compute_products(){
    var tr_quotation_products = $('.tr_quotation_products'),
        total_sub_total = Number(0),
        total_gst = Number(0),
        total_gst_incld = Number(0);
        $(tr_quotation_products).each(function(){
            var ths = $(this);
                total_gst += Number(ths.find('.inpt_gst').val().replace(/[^0-9\.]+/g, ''));
                total_sub_total += Number(ths.find('.product_amt').text().replace(/[^0-9\.]+/g, ''));
                total_gst_incld += Number(ths.find('.total_incl_gst').text().replace(/[^0-9\.]+/g, ''));
        })
        $('#total_gst').text(total_gst).formatCurrency();
        $('#total_sub_total').text(total_sub_total).formatCurrency();
        $('#total_gst_incld').text(total_gst_incld).formatCurrency();
}

