$(document).on('click','.suppliers_check_all',function(){
   var ths = $(this),
        ths_val = ths.val();
        if(ths_val == 0){
            ths.html('<i class="fa fa-check-square-o"></i>');
            ths.val('1');
            $('.supplier_check_box').prop("checked",true);
        }else{
            ths.html('<i class="fa fa-square-o"></i>');
            ths.val('0');
            $('.supplier_check_box').prop("checked",false);
        }
});

$(document).on('click','.confirm_delete_btn',function(){
    var checked_suppliers   = [],
        suppliers_deleted   = Number(0),
        total_suppliers     = Number($('.total_supplier_count').text());
        $(".supplier_check_box").each(function ()
        {
            var ths = $(this);
            if(ths.prop('checked') == true){    
                checked_suppliers.push(ths.val());
                ths.parent().parent().parent().fadeOut();
                suppliers_deleted++;
            }
        });
        
        // Variable to hold request
        var $inputs = $('#form_supplier_lists').find("input, select, button, textarea");
            $inputs.prop("disabled", true);
        $.post('remove_from_lists',
           {
            arr_id:checked_suppliers,
            table:'prototype_suppliers_info'
            },
           function(data)
           {
            }
          ).done(function(){
            $inputs.prop("disabled", false);
            $('.total_supplier_count').text(total_suppliers - suppliers_deleted);
            $('.close_modal').click();
        });
});




