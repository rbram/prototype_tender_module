$(document).on('click','.btn_add_sub_task',function(){
    var tab_li = $('#sub_task_nav_tabs li'),
        new_li = tab_li.first().clone(),
        old_task = $('#sub_task_tab_content .tab-pane'),
        new_task = old_task.first().clone(),
        count_tasks = Number($('#sub_task_nav_tabs li').length)+1;
    tab_li.removeClass('active');
    tab_li.find('a').removeAttr('aria-expanded');
    old_task.removeClass('active');
    
    new_li.addClass('active');
    new_li.find('a').attr('href','#tab_'+count_tasks);
    new_li.find('a').attr('aria-expanded','true');
    new_task.addClass('active');
    new_task.attr('aria-expanded','true');
    new_task.attr('id','tab_'+count_tasks);
    new_task.find('.tmp_text_area').html('');
    new_task.find('#text_area_details_').html('<textarea class="form-control new_text_area" style="max-height: 300px" placeholder="..."></textarea>');
    new_task.find('#text_area_instructions_').html('<textarea class="form-control new_text_area" style="max-height: 300px" placeholder="..."></textarea>');
    new_task.find('#text_area_other_').html('<textarea class="form-control new_text_area" style="max-height: 300px" placeholder="..."></textarea>');
    $('#sub_task_nav_tabs').append(new_li);
    $('#sub_task_tab_content').append(new_task);
    
    $('#tab_'+count_tasks+' .new_text_area').wysihtml5();
    $('.wysihtml5-toolbar .btn').addClass('btn-xs');
});