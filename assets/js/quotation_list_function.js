//CHECK ALL QUOTATION
$(document).on('click','.quotation_check_all',function()
               {
   var ths = $(this),
        ths_val = ths.val();
        if(ths_val == 0){
            ths.html('<i class="fa fa-check-square-o"></i>');
            ths.val('1');
            $('.quotation_check_box').prop("checked",true);
        }else{
            ths.html('<i class="fa fa-square-o"></i>');
            ths.val('0');
            $('.quotation_check_box').prop("checked",false);
        }
});
//CONFIRM DELETE QUOTATION
$(document).on('click','.confirm_delete_btn',function()
               {
    var checked_quotation   = [],
        quotation_deleted   = Number(0),
        total_suppliers     = Number($('.total_supplier_count').text());
        $(".quotation_check_box").each(function ()
        {
            var ths = $(this);
            if(ths.prop('checked') == true){    
                checked_quotation.push(ths.val());
                ths.parent().parent().parent().fadeOut();
                quotation_deleted++;
            }
        });
        
        // Variable to hold request
        var $inputs = $('#form_quotation_lists').find("input, select, button, textarea");
            $inputs.prop("disabled", true);
        $.post('remove_from_lists',
           {
            arr_id:checked_quotation,
            table:'prototype_quotations'
            },
           function(data)
           {
                
            }
          ).done(function(){
            $inputs.prop("disabled", false);
//            $('.total_supplier_count').text(total_suppliers - suppliers_deleted);
            $('.close_modal').click();
        });
});
//VIEW QUOTATION
$(document).on('click','.view_quotation',function()
               {
    var quotation_id = $(this).data('quotationid');
    $.post('view_quote',{
        quotation_id    : quotation_id
    }).done(function(data){
        $('#view_quote').append('<div class="col-sm-6 col-xs-12 no-gutter-left">'+data+'</div>');
        
    })
});
//MINIMIZE QUOTATION
$(document).on('click','.btn-box-tool',function()
               {
    var ths = $(this),
        btn_action = ths.data('widget');
        $("i", ths).toggleClass("fa fa-minus fa fa-plus");
    if(btn_action == 'collapse'){
        ths.parent().parent().parent().find('.box-body').slideToggle();
    }else if(btn_action == 'remove'){
        ths.parent().parent().parent().parent().remove();
    }
})
//Update quotation
$(document).on('click','.btn_quote_update',function(){
    var ths = $(this),
        quote_stat = ths.val();
    
    bootbox.confirm({
            title: "Confirm",
            message: "Are you sure you want to award projects to?.",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm',
                }
            },
            callback: function (result) {
                if(result){
                    
                    bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Sending Invitation. please wait...</div>',
                                     closeButton: false
                                   })
                }
            }
        });
        
})