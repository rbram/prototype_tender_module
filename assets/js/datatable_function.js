$( document ).ready(function() {
    $('#example1').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
      paging      : true,
      lengthChange: true,
      searching   : true,
      ordering    : true,
      info        : true,
      responsive  : true,
      autoWidth   : true
    });
    
    $('#example2').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
      paging      : true,
      lengthChange: true,
      searching   : true,
      ordering    : true,
      info        : true,
      responsive  : true,
      autoWidth   : true
    });
});
